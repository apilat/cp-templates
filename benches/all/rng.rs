use cp_templates::rng::*;
use criterion::{criterion_group, Criterion};

fn gen(c: &mut Criterion) {
    let mut rng = Rng::new(1);
    c.bench_function("rng gen", |b| b.iter(|| rng.next()));
}

criterion_group!(rng, gen);
