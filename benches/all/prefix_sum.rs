use crate::{BENCH_ELEMENTS, BENCH_SIZES};
use cp_templates::prefix_sum::*;
use criterion::{criterion_group, AxisScale, BenchmarkId, Criterion, PlotConfiguration};

fn query(c: &mut Criterion) {
    let mut group = c.benchmark_group("prefix_sum query");
    group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));
    for size in BENCH_SIZES {
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            let sums = PrefixSum::build(BENCH_ELEMENTS[..size].to_vec());
            b.iter(|| sums.query(..));
        });
    }
}

criterion_group!(prefix_sum, query);
