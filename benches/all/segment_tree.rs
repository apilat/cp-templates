use crate::{BENCH_ELEMENTS, BENCH_SIZES};
use cp_templates::segment_tree::{formatter::Formatter, operation::Operation, update::Update, *};
use criterion::{criterion_group, AxisScale, BatchSize, BenchmarkId, Criterion, PlotConfiguration};

const MOD: i64 = 1_000_000_007;
#[derive(Clone)]
struct ModMul;

impl Spec for ModMul {
    type Operation = Self;
    type Formatter = Self;
    type Update = Self;
}

impl Operation for ModMul {
    type Spec = Self;
    type Node = (i64, usize);
    #[inline(always)]
    fn op(_: &Self::Spec, a: &Self::Node, b: &Self::Node) -> Self::Node {
        ((a.0 * b.0) % MOD, a.1 + b.1)
    }
}

impl Formatter for ModMul {
    type Spec = Self;
    type Input = i64;
    type Output = i64;

    #[inline(always)]
    fn input(
        _: &Self::Spec,
        a: Self::Input,
        _i: usize,
    ) -> <<Self::Spec as Spec>::Operation as Operation>::Node {
        (a, 1)
    }
    #[inline(always)]
    fn output(
        _: &Self::Spec,
        a: &<<Self::Spec as Spec>::Operation as Operation>::Node,
    ) -> Self::Output {
        a.0
    }
}

impl Update for ModMul {
    type Spec = Self;
    type Update = i64;

    fn apply(
        _: &Self::Spec,
        f: &Self::Update,
        a: &<<Self::Spec as Spec>::Operation as Operation>::Node,
    ) -> <<Self::Spec as Spec>::Operation as Operation>::Node {
        fn modexp(a: i64, b: i64, m: i64) -> i64 {
            if b == 0 {
                1
            } else if b % 2 == 0 {
                modexp((a * a) % m, b / 2, m)
            } else {
                (modexp((a * a) % m, b / 2, m) * a) % m
            }
        }

        ((a.0 * modexp(*f, a.1 as i64, MOD)) % MOD, a.1)
    }
    #[inline(always)]
    fn compose(_: &Self::Spec, f: &Self::Update, g: &Self::Update) -> Self::Update {
        (*f * *g) % MOD
    }
}

fn query(c: &mut Criterion) {
    let mut group = c.benchmark_group("segment_tree query");
    group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));
    for size in BENCH_SIZES {
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            let mut tree = SegmentTree::<ModMul>::new(BENCH_ELEMENTS[..size].to_vec(), ModMul);
            b.iter(|| tree.query(..));
        });
    }
    group.finish();
}

fn set(c: &mut Criterion) {
    let mut group = c.benchmark_group("segment_tree set");
    group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));
    for size in BENCH_SIZES {
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            let mut tree = SegmentTree::<ModMul>::new(BENCH_ELEMENTS[..size].to_vec(), ModMul);
            b.iter(|| tree.set(size / 2, BENCH_ELEMENTS[size / 2]));
        });
    }
    group.finish();
}

fn update(c: &mut Criterion) {
    let mut group = c.benchmark_group("segment_tree update");
    group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));
    for size in BENCH_SIZES {
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            let tree = SegmentTree::<ModMul>::new(BENCH_ELEMENTS[..size].to_vec(), ModMul);
            b.iter_batched_ref(
                || tree.clone(),
                |tree| tree.update(.., BENCH_ELEMENTS[size / 2]),
                BatchSize::SmallInput,
            );
        });
    }
    group.finish();
}

criterion_group!(segment_tree, query, set, update);
