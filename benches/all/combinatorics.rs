use crate::BENCH_SIZES_BIG;
use cp_templates::combinatorics::*;
use criterion::{
    criterion_group, AxisScale, BenchmarkId, Criterion, PlotConfiguration, Throughput,
};

fn factorials_new(c: &mut Criterion) {
    let mut group = c.benchmark_group("factorials new");
    group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));
    for size in BENCH_SIZES_BIG {
        group.throughput(Throughput::Elements(size as u64));
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            b.iter(|| Factorials::new(size as i64, 1_000_000_007));
        });
    }
}

criterion_group!(factorials, factorials_new);
