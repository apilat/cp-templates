use crate::BENCH_SIZES_BIG;
use cp_templates::graph::*;
use criterion::{
    criterion_group, AxisScale, BenchmarkId, Criterion, PlotConfiguration, Throughput,
};

fn dfs_(c: &mut Criterion) {
    for shape in StandardGraph::iter() {
        let mut group = c.benchmark_group(format!("dfs {:?}", shape));
        group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));
        for size in BENCH_SIZES_BIG {
            group.throughput(Throughput::Elements(size as u64));
            group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
                let graph = shape.init(size, false, false);
                b.iter(|| graph.dfs());
            });
        }
    }
}

criterion_group!(dfs, dfs_);
