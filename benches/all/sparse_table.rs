use crate::{BENCH_ELEMENTS, BENCH_SIZES};
use cp_templates::sparse_table::{ops::Min, *};
use criterion::{
    criterion_group, AxisScale, BenchmarkId, Criterion, PlotConfiguration, Throughput,
};

const MOD: i64 = 1_000_000_007;
#[derive(Clone)]
struct ModSum;
impl Spec for ModSum {
    type Data = i64;
    const IDEMPOTENT: bool = false;
    #[inline(always)]
    fn op(&self, a: &Self::Data, b: &Self::Data) -> Self::Data {
        (*a + *b) % MOD
    }
}

fn new(c: &mut Criterion) {
    let mut group = c.benchmark_group("sparse_table new");
    group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));
    for size in BENCH_SIZES {
        group.throughput(Throughput::Elements(size as u64));
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            b.iter(|| SparseTable::new(BENCH_ELEMENTS[..size].to_vec(), Min::<i64>::default()));
        });
    }
    group.finish();
}

fn query_idempotent(c: &mut Criterion) {
    let mut group = c.benchmark_group("sparse_table query_idempotent");
    group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));
    for mut size in BENCH_SIZES {
        size -= 1;
        assert!(!size.is_power_of_two());
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            let table = SparseTable::new(BENCH_ELEMENTS[..size].to_vec(), Min::<i64>::default());
            b.iter(|| table.query(..));
        });
    }
    group.finish();
}

fn query_non_idempotent(c: &mut Criterion) {
    let mut group = c.benchmark_group("sparse_table query_non_idempotent");
    group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));
    for mut size in BENCH_SIZES {
        size -= 1;
        assert!(!size.is_power_of_two());
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            let table = SparseTable::new(BENCH_ELEMENTS[..size].to_vec(), ModSum);
            b.iter(|| table.query(..));
        });
    }
    group.finish();
}

fn query_power_of_two(c: &mut Criterion) {
    let mut group = c.benchmark_group("sparse_table query_power_of_two");
    group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));
    for size in BENCH_SIZES {
        assert!(size.is_power_of_two());
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            let table = SparseTable::new(BENCH_ELEMENTS[..size].to_vec(), ModSum);
            b.iter(|| table.query(..));
        });
    }
    group.finish();
}

criterion_group!(
    sparse_table,
    new,
    query_idempotent,
    query_non_idempotent,
    query_power_of_two
);
