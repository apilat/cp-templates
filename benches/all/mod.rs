use criterion::criterion_main;
mod avl_tree;
mod combinatorics;
mod graph;
mod number_theory;
mod prefix_sum;
mod rng;
mod segment_tree;
mod sparse_table;
criterion_main!(
    avl_tree::avl_tree,
    combinatorics::factorials,
    number_theory::prime_sieve,
    prefix_sum::prefix_sum,
    rng::rng,
    segment_tree::segment_tree,
    sparse_table::sparse_table,
    graph::dfs,
);

pub const BENCH_SIZES: [usize; 10] = [8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096];
pub const BENCH_ELEMENTS: [i64; 65536] = {
    let mut elems = [0; 65536];
    let mut seed = 1i64;
    let mut i = 0;
    while i < elems.len() {
        seed = seed
            .wrapping_mul(6364136223846793005)
            .wrapping_add(1442695040888963407);
        elems[i] = seed;
        i += 1;
    }
    elems
};

pub const BENCH_SIZES_BIG: [usize; 10] = [
    100, 256, 1_000, 1_024, 10_000, 16_384, 100_000, 131_072, 1_000_000, 1_048_576,
];
