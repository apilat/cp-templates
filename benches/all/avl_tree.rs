use crate::{BENCH_ELEMENTS, BENCH_SIZES};
use cp_templates::avl_tree::*;
use criterion::{criterion_group, AxisScale, BatchSize, BenchmarkId, Criterion, PlotConfiguration};

fn insert(c: &mut Criterion) {
    let mut group = c.benchmark_group("avl_tree insert");
    group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));
    for size in BENCH_SIZES {
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            let mut tree = AvlTree::<i64>::new();
            for &x in &BENCH_ELEMENTS[..size - 1] {
                tree.insert(x);
            }
            b.iter_batched_ref(
                || tree.clone(),
                |tree| tree.insert(BENCH_ELEMENTS[size - 1]),
                BatchSize::SmallInput,
            );
        });
    }
    group.finish();
}

fn find(c: &mut Criterion) {
    let mut tree = AvlTree::<i64>::new();
    for x in BENCH_ELEMENTS {
        tree.insert(x);
    }

    let mut group = c.benchmark_group("avl_tree find");
    group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));
    for size in BENCH_SIZES {
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            let mut tree = AvlTree::<i64>::new();
            for &x in &BENCH_ELEMENTS[..size] {
                tree.insert(x);
            }
            b.iter(|| tree.find(&BENCH_ELEMENTS[size / 2]).copied());
        });
    }
    group.finish();
}

fn count_lt(c: &mut Criterion) {
    let mut group = c.benchmark_group("avl_tree count_lt");
    group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));
    for size in BENCH_SIZES {
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            let mut tree = AvlTree::<i64>::new();
            for &x in &BENCH_ELEMENTS[..size] {
                tree.insert(x);
            }
            b.iter(|| tree.count_lt(&BENCH_ELEMENTS[size / 2]));
        });
    }
    group.finish();
}

fn select(c: &mut Criterion) {
    let mut group = c.benchmark_group("avl_tree select");
    group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));
    for size in BENCH_SIZES {
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            let mut tree = AvlTree::<i64>::new();
            for &x in &BENCH_ELEMENTS[..size] {
                tree.insert(x);
            }
            b.iter(|| tree.select(size / 2).unwrap());
        });
    }
    group.finish();
}

criterion_group!(avl_tree, insert, find, count_lt, select);
