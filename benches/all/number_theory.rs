use crate::BENCH_SIZES_BIG;
use cp_templates::number_theory::*;
use criterion::{
    criterion_group, AxisScale, BenchmarkId, Criterion, PlotConfiguration, Throughput,
};

fn prime_sieve_new(c: &mut Criterion) {
    let mut group = c.benchmark_group("prime_sieve new");
    group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));
    for size in BENCH_SIZES_BIG {
        group.throughput(Throughput::Elements(size as u64));
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            b.iter(|| PrimeSieve::<i64>::new(size as i64))
        });
    }
    group.finish();
}

fn prime_sieve_factorize(c: &mut Criterion) {
    let mut group = c.benchmark_group("prime_sieve factorize");
    group.plot_config(PlotConfiguration::default().summary_scale(AxisScale::Logarithmic));
    for size in BENCH_SIZES_BIG {
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            let sieve = PrimeSieve::<i64>::new(size as i64);
            b.iter(|| sieve.factorize(size as i64));
        });
    }
    group.finish();
}

criterion_group!(prime_sieve, prime_sieve_new, prime_sieve_factorize);
