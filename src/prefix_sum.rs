// NAME prefix_sum
// START code
use std::{
    fmt::{self, Debug, Formatter},
    marker::PhantomData,
    ops::{Add, Bound, Range, RangeBounds, Sub},
};

/// An operation that can be used to create prefix arrays. Optionally supports an inverse, which
/// allows arbitrary range queries.
#[derive(Clone)]
pub struct PrefixOp<T, F, I> {
    fwd: F,
    inv: Option<I>,
    marker: PhantomData<fn(&T, &T) -> T>,
}

impl<T, F> PrefixOp<T, F, ()>
where
    F: Fn(&T, &T) -> T,
{
    /// Defines a non-invertible operation `op`.
    pub fn new(op: F) -> Self {
        PrefixOp {
            fwd: op,
            inv: None,
            marker: PhantomData,
        }
    }
}

impl<T, F, I> PrefixOp<T, F, I>
where
    F: Fn(&T, &T) -> T,
    I: Fn(&T, &T) -> T,
{
    /// Defines an invertible operation `fwd` whose inverse is `inv`.
    ///
    /// The following must hold for query operations to return the correct result:
    /// `inv(op(x, y), x) = y [for any x,y]`
    pub fn new_invertible(op: F, inverse: I) -> Self {
        PrefixOp {
            fwd: op,
            inv: Some(inverse),
            marker: PhantomData,
        }
    }
}

impl<T, F, I> PrefixOp<T, F, I>
where
    F: Fn(&T, &T) -> T,
    I: Clone,
{
    /// Builds a prefix array of `arr` using this operation.
    pub fn build(&self, mut arr: Vec<T>) -> PrefixArray<T, F, I> {
        for i in 1..arr.len() {
            arr[i] = (self.fwd)(&arr[i - 1], &arr[i]);
        }
        PrefixArray {
            pref: arr,
            fwd: PhantomData,
            inv: self.inv.clone(),
        }
    }
}

/// Cumulative operations applied on prefixes of a given array, supporting range queries for
/// invertible operations.
///
/// Time complexity of operations:
/// - `build: O(n)`
/// - `query, query_prefix: O(1)`
#[derive(Clone)]
pub struct PrefixArray<T, F, I> {
    pref: Vec<T>,
    // Forward operation is not actually needed for anything anymore but still store in in the type
    // to prevent accidental type mismatches.
    fwd: PhantomData<F>,
    inv: Option<I>,
}

impl<T, F, I> PrefixArray<T, F, I> {
    /// Returns the length of the prefix array.
    pub fn len(&self) -> usize {
        self.pref.len()
    }

    /// Returns `true` if the prefix array is empty.
    pub fn is_empty(&self) -> bool {
        self.pref.is_empty()
    }

    /// Verifiers the range bounds are valid and returns the range `[l, r)` the represent. Panics on error.
    #[track_caller]
    fn verify_range_bounds(&self, range: impl RangeBounds<usize>) -> Range<usize> {
        let start = match range.start_bound() {
            Bound::Unbounded => 0,
            Bound::Included(&x) => x,
            Bound::Excluded(&x) => x + 1,
        };
        let end = match range.end_bound() {
            Bound::Unbounded => self.len(),
            Bound::Included(&x) => x + 1,
            Bound::Excluded(&x) => x,
        };

        assert!(
            start < end,
            "range inverted: start >= end ({} >= {})",
            start,
            end
        );
        assert!(
            end <= self.len(),
            "range out of bounds: end > len ({} > {})",
            end,
            self.len()
        );

        Range { start, end }
    }

    /// Returns the result of the operation applied on the prefix `range`.
    ///
    /// Panics if the given range does not start at the `0`th element or is otherwise invalid.
    #[track_caller]
    pub fn query_prefix(&self, range: impl RangeBounds<usize>) -> T
    where
        T: Clone,
    {
        let Range { start, end } = self.verify_range_bounds(range);
        assert!(start == 0, "query start bound is non-zero: {} > 0", start);
        self.pref[end - 1].clone()
    }
}

impl<T, F, I> PrefixArray<T, F, I>
where
    I: Fn(&T, &T) -> T,
{
    /// Returns the result of the operation applied on the given `range`.
    #[track_caller]
    pub fn query(&self, range: impl RangeBounds<usize>) -> T
    where
        T: Clone,
    {
        let Range { start, end } = self.verify_range_bounds(range);
        if start == 0 {
            self.pref[end - 1].clone()
        } else {
            (self
                .inv
                .as_ref()
                .expect("range query is not supported on non-invertible operation"))(
                &self.pref[end - 1],
                &self.pref[start - 1],
            )
        }
    }
}

impl<T, F, I> Debug for PrefixOp<T, F, I>
where
    T: Debug,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("PrefixOp").finish_non_exhaustive()
    }
}

impl<T, F, I> Debug for PrefixArray<T, F, I>
where
    T: Debug,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("PrefixArray")
            .field("pref", &self.pref)
            .finish_non_exhaustive()
    }
}

type NextFn<T> = fn(&T, &T) -> T;

pub struct PrefixSum<T>(PhantomData<T>);
impl<T> PrefixSum<T>
where
    T: Add<Output = T> + Sub<Output = T> + Clone,
{
    /// Creates an invertible `PrefixOp` using `Add` and `Sub`.
    pub fn op() -> PrefixOp<T, NextFn<T>, NextFn<T>> {
        PrefixOp::new_invertible(|a, b| a.clone() + b.clone(), |b, a| b.clone() - a.clone())
    }

    /// Builds a prefix sum array from `arr`.
    pub fn build(arr: Vec<T>) -> PrefixArray<T, NextFn<T>, NextFn<T>> {
        Self::op().build(arr)
    }
}
// END code

/* Templates
// START imports
{PrefixOp, PrefixSum}
// END imports
*/

#[cfg(test)]
mod tests {
    use super::*;
    use std::panic::catch_unwind;

    #[test]
    fn prefix_op() {
        assert_eq!(
            PrefixOp::new(|a, b| format!("{}{}", a, b))
                .build(
                    ["x", "y", "z", "v"]
                        .iter()
                        .copied()
                        .map(str::to_string)
                        .collect()
                )
                .pref,
            ["x", "xy", "xyz", "xyzv"]
        );

        assert_eq!(
            PrefixOp::new(|a, b| a - b)
                .build(vec![7i64, 13, 80, -15, 11])
                .pref,
            [7, -6, -86, -71, -82]
        );

        assert_eq!(
            PrefixOp::new(|a: &i64, b| *a.min(b))
                .build(vec![5, 2, 3, 2, 1, 6, 3, 0, 9])
                .pref,
            [5, 2, 2, 2, 1, 1, 1, 0, 0]
        );
    }

    #[test]
    fn prefix_sums() {
        assert_eq!(PrefixSum::build(vec![1, 3, -2, 5]).pref, [1, 4, 2, 7]);
        assert_eq!(
            PrefixSum::build(vec![10, 20, -4, 5, 12, 0, 1]).pref,
            [10, 30, 26, 31, 43, 43, 44]
        );
        assert_eq!(
            PrefixSum::op().build(vec![0, -4, 5, -1, 8]).pref,
            [0, -4, 1, 0, 8]
        );
    }

    #[test]
    fn query_prefix() {
        let pref = PrefixOp::new(|a: &i64, b| *a.min(b)).build(vec![10, 8, 10, 11, 7, 4, 5, 9]);
        assert_eq!(pref.query_prefix(0..=3), 8);
        assert_eq!(pref.query_prefix(0..=4), 7);
        assert_eq!(pref.query_prefix(0..8), 4);
        assert_eq!(pref.query_prefix(..), 4);
        assert!(catch_unwind(|| pref.query_prefix(0..9)).is_err());
        assert!(catch_unwind(|| pref.query_prefix(1..3)).is_err());
        assert!(
            catch_unwind(|| pref.query_prefix((Bound::Excluded(0), Bound::Included(1)))).is_err()
        );
    }

    #[test]
    fn query() {
        let pref = PrefixOp::new_invertible(|a, b| a + b, |b, a| b - a)
            .build(vec![3, 4, 2, 3, 7, 5, 7, 2, 8, 9]);
        assert_eq!(pref.query(0..4), 12);
        assert_eq!(pref.query(0..=4), 19);
        assert_eq!(pref.query(..), 50);
        assert_eq!(pref.query(3..), 41);
        assert_eq!(pref.query((Bound::Excluded(3), Bound::Included(5))), 12);
        assert!(catch_unwind(|| pref.query(3..2)).is_err());
        assert!(catch_unwind(|| pref.query(1..1)).is_err());
        assert!(catch_unwind(|| pref.query(0..11)).is_err());
        assert!(catch_unwind(|| pref.query(2..=10)).is_err());
    }

    #[test]
    fn query_random() {
        let pref = PrefixSum::build(
            (0u64..5000)
                .map(|x| x.wrapping_mul(7115656737144105383) >> 32)
                .collect(),
        );
        let mut res = 0;
        for i in 0u64..5000 {
            let a = (i.wrapping_mul(7908553268304490033) % 5000) as usize;
            let b = (i.wrapping_mul(8769096718339921231) % 5000) as usize;
            res += pref.query(a.min(b)..=a.max(b));
        }
        assert_eq!(res, 17882850931751612);
    }
}
