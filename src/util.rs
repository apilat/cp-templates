// NAME util
// START code
use std::cmp::Reverse;

// Rust's type analysis is not advanced enough to see that these implementations are disjoint
// (https://stackoverflow.com/a/66248448/5304124).
// We use a hack from that answer to overcome this.
pub trait IntoVec<T, _Hack> {
    fn into_vec(self) -> Vec<T>;
}

#[non_exhaustive]
pub struct Owned;
impl<I, T> IntoVec<T, Owned> for I
where
    I: Iterator<Item = T>,
{
    fn into_vec(self) -> Vec<T> {
        self.collect()
    }
}

#[non_exhaustive]
pub struct Reference;
impl<'a, I, T> IntoVec<T, Reference> for I
where
    I: Iterator<Item = &'a T>,
    T: Clone + 'a,
{
    fn into_vec(self) -> Vec<T> {
        self.cloned().collect()
    }
}

pub trait Argmax {
    fn argmax(self) -> Option<usize>;
    fn argmin(self) -> Option<usize>;
}

impl<I> Argmax for I
where
    I: Iterator,
    I::Item: Ord,
{
    fn argmax(self) -> Option<usize> {
        self.fold((0, None), |(i, mx), next| {
            if let Some((mx_i, mx_v)) = mx {
                if next.gt(&mx_v) {
                    (i + 1, Some((i, next)))
                } else {
                    (i + 1, Some((mx_i, mx_v)))
                }
            } else {
                (i + 1, Some((i, next)))
            }
        })
        .1
        .map(|x| x.0)
    }

    fn argmin(self) -> Option<usize> {
        self.map(Reverse).argmax()
    }
}

pub trait Sorted<T> {
    fn sorted(self) -> Self;
}

impl<T> Sorted<T> for Vec<T>
where
    T: Ord,
{
    fn sorted(mut self) -> Self {
        self.sort();
        self
    }
}

pub trait SortedBy<T> {
    fn sorted_by<F, K>(self, f: F) -> Self
    where
        F: FnMut(&T) -> K,
        K: Ord;
}

impl<T> SortedBy<T> for Vec<T> {
    fn sorted_by<F, K>(mut self, f: F) -> Self
    where
        F: FnMut(&T) -> K,
        K: Ord,
    {
        self.sort_by_key(f);
        self
    }
}

pub trait Choose {
    fn choose<T>(self, a: T, b: T) -> T;
    fn choose_by<T>(self, a: impl FnOnce() -> T, b: impl FnOnce() -> T) -> T;
}

impl Choose for bool {
    fn choose<T>(self, a: T, b: T) -> T {
        if self {
            a
        } else {
            b
        }
    }

    fn choose_by<T>(self, a: impl FnOnce() -> T, b: impl FnOnce() -> T) -> T {
        if self {
            a()
        } else {
            b()
        }
    }
}

#[macro_export]
macro_rules! p {
    ( $( $x:expr ),* ) => {
        #[cfg(any(debug_assertions, debug_print))]
        {
            dbg!( $( & $x, )* );
        }
    };
}

pub fn fix<A, R>(f: &dyn Fn(&dyn Fn(A) -> R, A) -> R) -> impl Fn(A) -> R + '_ {
    move |arg| f(&fix(&f), arg)
}

pub trait Assign<T> {
    fn assign(&mut self, val: T);
}

impl<T> Assign<T> for Option<T> {
    // Assign `val` to `self`, asserting that it was `None` before.
    fn assign(&mut self, val: T) {
        assert!(self.is_none(), ".assign() called on Some variant");
        *self = Some(val);
    }
}

pub fn compress(arr: &mut [i64]) {
    let mut pairs: Vec<_> = arr.iter().copied().enumerate().collect();
    pairs.sort_unstable_by_key(|(_idx, val)| *val);
    let (mut cur_idx, mut last_val) = (0, None);
    for &(idx, val) in pairs.iter() {
        if last_val.is_some() && last_val != Some(val) {
            cur_idx += 1;
        }
        arr[idx] = cur_idx;
        last_val = Some(val);
    }
}
// END code

/* Templates
// START imports
{IntoVec, Argmax, Sorted, SortedBy, Choose, fix, Assign, compress}
// END imports
*/

#[cfg(test)]
mod tests {
    use super::*;
    use std::{cell::Cell, panic::catch_unwind};

    #[test]
    fn into_vec() {
        assert_eq!([1, 2, 3, 4].iter().copied().into_vec(), vec![1, 2, 3, 4]);
        assert_eq!("xyz".chars().into_vec(), vec!['x', 'y', 'z']);

        let mut x = [5, 6, 7, 0, -2].iter().into_vec();
        x[0] = 8; // Forces Clone implementation
        assert_eq!(x, vec![8, 6, 7, 0, -2]);
    }

    #[test]
    fn argmax() {
        assert_eq!([100; 0].iter().argmax(), None);
        assert_eq!([i64::MIN].iter().argmax(), Some(0));
        assert_eq!([1, 2, 3, 4, 3].iter().argmax(), Some(3));
        assert_eq!(
            [u64::MAX, u64::MIN, u64::MAX, 0, 0, u64::MAX]
                .iter()
                .argmax(),
            Some(0)
        );
        assert_eq!([-12, 10, 5, 10, -7, 99].iter().argmax(), Some(5));
        assert_eq!([1, 1, 1, 1].iter().argmax(), Some(0));
    }

    #[test]
    fn argmin() {
        assert_eq!([-1, -5, 10, 100].iter().argmin(), Some(1));
        assert_eq!([15, 1000, -100].iter().argmin(), Some(2));
        assert_eq!(
            [i32::MAX, i32::MIN, i32::MIN, 0, i32::MAX].iter().argmin(),
            Some(1)
        );
    }

    #[test]
    fn sorted() {
        assert_eq!(vec![3, 4, 2, 6, 8, 1].sorted(), vec![1, 2, 3, 4, 6, 8]);
        assert_eq!(
            vec![890, 781, 190, 209, 2373, 4890, 129].sorted(),
            vec![129, 190, 209, 781, 890, 2373, 4890]
        );
    }

    #[test]
    fn sorted_by() {
        #[derive(Debug, Clone, PartialEq, Eq)]
        struct NoSort<T>(T);
        assert_eq!(
            vec![
                NoSort(3),
                NoSort(4),
                NoSort(7),
                NoSort(-1),
                NoSort(2),
                NoSort(8)
            ]
            .sorted_by(|x| Reverse(x.0)),
            [8, 7, 4, 3, 2, -1].iter().copied().map(NoSort).into_vec()
        );
        assert_eq!(
            ["aa", "b", "ccc", "dd", "f", "eeee"]
                .iter()
                .map(NoSort)
                .into_vec()
                .sorted_by(|x| x.0.len()),
            ["b", "f", "aa", "dd", "ccc", "eeee"]
                .iter()
                .map(NoSort)
                .into_vec()
        );
    }

    #[test]
    fn choose() {
        assert_eq!(true.choose(3, 1), 3);
        assert_eq!(false.choose("YES", "NO"), "NO");

        let counter = Cell::new(0);
        let plus1 = || {
            counter.set(counter.get() + 1);
            counter.get()
        };
        let plus2 = || {
            counter.set(counter.get() + 2);
            counter.get()
        };
        assert_eq!(true.choose_by(plus1, plus2), 1);
        assert_eq!(true.choose_by(plus1, plus2), 2);
        assert_eq!(false.choose_by(plus1, plus2), 4);
    }

    #[test]
    fn p_macro() {
        let _copy_type = 123;
        let needs_reference = vec![2, 3, 4];
        p!(_copy_type, needs_reference);
        let _ = needs_reference;
    }

    #[test]
    fn fix_() {
        let fact = fix(&|fact, n: i64| if n == 0 { 1 } else { n * fact(n - 1) });
        assert_eq!(fact(0), 1);
        assert_eq!(fact(1), 1);
        assert_eq!(fact(6), 720);
        assert_eq!(fact(19), 121645100408832000i64);

        let arr = vec![2, 3, 4, 5, 6, 1, 8];
        let sum = fix(&|sum, i| {
            if let Some(&v) = arr.get(i) {
                v + sum(i + 1)
            } else {
                0
            }
        })(0);
        assert_eq!(sum, 29);
    }

    #[test]
    fn assign() {
        let mut opt = None;
        opt.assign(5);
        assert_eq!(opt, Some(5));
        assert!(catch_unwind(move || opt.assign(3)).is_err());

        let mut opt = Some("yes");
        assert!(catch_unwind(move || opt.assign("no")).is_err());
    }

    #[test]
    fn compress_() {
        fn compressed(mut arr: Vec<i64>) -> Vec<i64> {
            compress(&mut arr);
            arr
        }
        assert_eq!(compressed(vec![]), vec![]);
        assert_eq!(compressed(vec![0]), vec![0]);
        assert_eq!(compressed(vec![2]), vec![0]);
        assert_eq!(compressed(vec![10, 20, -10, 30]), vec![1, 2, 0, 3]);
        assert_eq!(
            compressed(vec![1, 1, 1, -1, 2, -1, 3, 3, 0]),
            vec![2, 2, 2, 0, 3, 0, 4, 4, 1]
        );
    }
}
