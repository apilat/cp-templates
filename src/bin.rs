use std::collections::HashMap;
use std::env::args;
use std::error::Error;
use std::ffi::OsStr;
use std::fmt::Write;
use std::fs::{read, read_dir, write};
use std::mem;
use std::path::PathBuf;

#[derive(Debug, Clone)]
struct Module {
    name: String,
    sections: HashMap<String, String>,
    requirements: Vec<String>,
}

fn load_modules() -> Result<Vec<Module>, Box<dyn Error>> {
    let mut search_paths = vec![PathBuf::from(".")];
    let mut files: Vec<String> = Vec::new();

    while let Some(path) = search_paths.pop() {
        for file in read_dir(path)? {
            let file = file?;
            let file_type = file.file_type()?;
            let path = file.path();

            let extension = path.extension().and_then(OsStr::to_str);
            if file_type.is_file() && (extension == Some("rs") || extension == Some("cfg")) {
                files.push(String::from_utf8(read(&path)?)?);
            } else if file_type.is_dir() {
                search_paths.push(path);
            }
        }
    }

    let mut modules = Vec::new();
    for file in files {
        if let Some(module) = parse_module(&file)? {
            if modules.iter().any(|m2: &Module| module.name == m2.name) {
                return Err(format!("duplicate module {}", module.name).into());
            }
            modules.push(module);
        }
    }

    Ok(modules)
}

fn parse_module(text: &str) -> Result<Option<Module>, Box<dyn Error>> {
    let mut sections = HashMap::new();
    let mut section_content = String::new();
    let mut section_name = None;
    let mut adding = false;
    let mut name = None;
    let mut requirements = Vec::new();

    for line in text.split('\n') {
        if let Some(rest) = line.strip_prefix("// NAME ") {
            name = Some(rest.to_string());
            continue;
        } else if let Some(rest) = line.strip_prefix("// REQUIREMENT ") {
            requirements.push(rest.to_string());
            continue;
        } else if let Some(rest) = line.strip_prefix("// START ") {
            adding = true;
            section_name = Some(rest.to_string());
        } else if let Some(rest) = line.strip_prefix("// END ") {
            adding = false;
            if let Some(name) = section_name.take() {
                if name == rest {
                    sections.insert(name, mem::take(&mut section_content));
                } else {
                    return Err(format!("section {} end found but expecting {}", rest, name).into());
                }
            } else {
                return Err(format!("section {} end found before start", rest).into());
            }
        } else if adding {
            section_content.push_str(line);
            section_content.push('\n');
        }
    }

    if let Some(name) = section_name {
        return Err(format!("section {} started but not ended", name).into());
    }

    // If module not named, skipping since we're assuming it is not a correct template.
    if let Some(name) = name {
        Ok(Some(Module {
            name,
            sections,
            requirements,
        }))
    } else {
        Ok(None)
    }
}

fn resolve_modules(
    modules: Vec<Module>,
    desired_names: Vec<String>,
) -> Result<Vec<Module>, Box<dyn Error>> {
    // TODO Figure out if this can be done without cloning the names.
    let mut unused_modules: HashMap<String, Module> =
        modules.into_iter().map(|x| (x.name.clone(), x)).collect();
    let mut required = desired_names;
    let mut modules = HashMap::new();

    while let Some(name) = required.pop() {
        if let Some(mut module) = unused_modules.remove(&name) {
            // Requirements will no longer be needed so we can take ownership of them.
            for req in std::mem::take(&mut module.requirements) {
                if !modules.contains_key(&req) {
                    required.push(req);
                }
            }
            modules.insert(module.name.clone(), module);
        } else if !modules.contains_key(&name) {
            return Err(format!("module not found: {}", name).into());
        }
    }
    Ok(modules.into_iter().map(|(_, m)| m).collect())
}

fn create_template(modules: Vec<Module>) -> Result<String, Box<dyn Error>> {
    let mut s = String::new();
    s.push_str("#![allow(dead_code,unused_imports)]\n");

    for m in modules.iter() {
        if let Some(code) = m.sections.get("code") {
            write!(s, "#[rustfmt::skip] mod {} {{", m.name).unwrap();
            for line in code.lines() {
                // Strip leading whitespace and comments
                s.push_str(line.trim().split("//").next().unwrap());
                if s.as_bytes().last() != Some(&b';') {
                    // In case whitespace might be syntactic, re-append it.
                    // This is likely too conservative, but it still leads to good compression.
                    s.push(' ');
                }
            }
            s.push_str("}\n");
        }
    }
    s.push('\n');

    for m in modules.iter() {
        if let Some(import) = m.sections.get("imports") {
            writeln!(s, "use {}::{};", m.name, import.trim()).unwrap();
        }
    }
    s.push('\n');

    s.push_str("fn main() {\n");
    for m in modules.iter() {
        if let Some(boilerplate) = m.sections.get("include_main") {
            for line in boilerplate.lines() {
                writeln!(s, "    {}", line).unwrap();
            }
        }
    }
    s.push_str("}\n");

    Ok(s)
}

fn main() -> Result<(), Box<dyn Error>> {
    let all_modules = load_modules()?;
    let desired_modules: Vec<String> = args()
        .nth(1)
        .expect("Add desired modules as second argument")
        .split(',')
        .map(str::to_string)
        .collect();
    let modules = resolve_modules(all_modules, desired_modules)?;
    let content = create_template(modules)?;
    write("template.rs", content)?;
    Ok(())
}
