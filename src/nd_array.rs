// NAME nd_array
// START code
use std::cell::RefCell;
use std::fmt::{self, Debug, Formatter};
use std::ops::{Index, IndexMut};
use std::rc::Rc;

#[derive(Clone, PartialEq, Eq)]
pub struct NdArray<T, const N: usize> {
    dim: [usize; N],
    data: Box<[T]>,
}

impl<T, const N: usize> NdArray<T, N> {
    #[track_caller]
    fn calculate_capacity(dim: &[usize; N]) -> usize {
        assert!(!dim.is_empty(), "array dimensions cannot be empty");
        let mut cap = 1;
        for &x in dim {
            assert!(x > 0, "array dimension must be non-zero");
            cap = usize::checked_mul(cap, x).expect("vector capacity overflowed usize");
        }
        cap
    }

    #[track_caller]
    pub fn new(dim: [usize; N], default: T) -> Self
    where
        T: Clone,
    {
        let cap = Self::calculate_capacity(&dim);
        NdArray {
            dim,
            data: vec![default; cap].into_boxed_slice(),
        }
    }

    #[track_caller]
    pub fn new_with(dim: [usize; N], generator: impl FnMut() -> T) -> Self {
        let cap = Self::calculate_capacity(&dim);
        NdArray {
            dim,
            data: {
                let mut v = Vec::with_capacity(cap);
                v.resize_with(cap, generator);
                v.into_boxed_slice()
            },
        }
    }

    #[track_caller]
    fn get_flat_idx(&self, idx: &[usize; N]) -> usize {
        let mut i = 0;
        #[allow(clippy::needless_range_loop)]
        for d in 0..self.dim.len() {
            assert!(
                idx[d] < self.dim[d],
                "index {} is out of bounds for dimension {} with size {}",
                idx[d],
                d,
                self.dim[d],
            );
            // This cannot overflow since we already checked product of all dimensions fits usize
            // and idx < self.dim.
            i = i * self.dim[d] + idx[d];
        }
        i
    }

    pub fn dim(&self) -> &[usize; N] {
        &self.dim
    }

    pub fn len(&self) -> usize {
        self.data.len()
    }

    pub fn is_empty(&self) -> bool {
        // Cannot be empty since we ensure in calculate_capacity that all dimensions are above 0.
        false
    }

    pub fn iter(&self) -> View<'_, T, N> {
        let mut dim = [ViewOption::Const(0); N];
        for (i, &x) in self.dim.iter().enumerate() {
            dim[i] = ViewOption::Iterating { cur: 0, size: x };
        }
        View {
            array: self,
            dim,
            finished: false,
        }
    }

    pub fn iter_slice(&self, idx: &[Option<usize>; N]) -> View<'_, T, N> {
        let mut dim = [ViewOption::Const(0); N];
        for i in 0..N {
            dim[i] = match idx[i] {
                None => ViewOption::Iterating {
                    cur: 0,
                    size: self.dim[i],
                },
                Some(x) => ViewOption::Const(x),
            };
        }
        View {
            array: self,
            dim,
            finished: false,
        }
    }
}

impl<T, const N: usize> Index<&[usize; N]> for NdArray<T, N> {
    type Output = T;

    #[track_caller]
    fn index(&self, idx: &[usize; N]) -> &T {
        &self.data[self.get_flat_idx(idx)]
    }
}

impl<T, const N: usize> IndexMut<&[usize; N]> for NdArray<T, N> {
    #[track_caller]
    fn index_mut(&mut self, idx: &[usize; N]) -> &mut T {
        let i = self.get_flat_idx(idx);
        &mut self.data[i]
    }
}

impl<T: Debug, const N: usize> Debug for NdArray<T, N> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        struct DataView<'a, T, const N: usize> {
            array: &'a NdArray<T, N>,
            idx: Rc<RefCell<[usize; N]>>,
            filled: usize,
        }

        impl<T: Debug, const N: usize> Debug for DataView<'_, T, N> {
            fn fmt(&self, f: &mut Formatter) -> fmt::Result {
                if self.filled == N {
                    self.array[&self.idx.borrow()].fmt(f)
                } else {
                    let mut builder = f.debug_list();
                    for i in 0..self.array.dim()[self.filled] {
                        self.idx.borrow_mut()[self.filled] = i;
                        let view = DataView {
                            array: self.array,
                            idx: self.idx.clone(),
                            filled: self.filled + 1,
                        };
                        builder.entry(&view);
                    }
                    builder.finish()
                }
            }
        }

        f.debug_struct("NdArray")
            .field("dim", &self.dim)
            .field(
                "data",
                &DataView {
                    array: self,
                    idx: Rc::new(RefCell::new([0; N])),
                    filled: 0,
                },
            )
            .finish()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum ViewOption {
    Const(usize),
    Iterating { cur: usize, size: usize },
}

#[derive(Debug, PartialEq, Eq)]
pub struct View<'a, T, const N: usize> {
    array: &'a NdArray<T, N>,
    dim: [ViewOption; N],
    finished: bool,
}

impl<'a, T, const N: usize> Iterator for View<'a, T, N> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.finished {
            return None;
        }

        let mut idx = [0; N];
        let mut carry = true;

        for (i, d) in self.dim.iter_mut().enumerate().rev() {
            match d {
                ViewOption::Const(x) => idx[i] = *x,
                ViewOption::Iterating { cur, size } => {
                    idx[i] = *cur;
                    if carry {
                        *cur += 1;
                        if cur == size {
                            *cur = 0;
                        } else {
                            carry = false;
                        }
                    }
                }
            };
        }

        if carry {
            self.finished = true;
        }

        Some(&self.array[&idx])
    }
}

// We don't want a clone bound on T, so have to derive manually.
impl<'a, T, const N: usize> Clone for View<'a, T, N> {
    fn clone(&self) -> Self {
        View {
            array: self.array,
            dim: self.dim,
            finished: false,
        }
    }
}
// END code

/* Templates
// START imports
NdArray
// END imports
*/

#[cfg(test)]
mod tests {
    use super::*;
    use std::panic::catch_unwind;

    #[test]
    fn nd_array_1d() {
        let mut array = NdArray::new_with([10], || 1);
        assert_eq!(array[&[0]], 1);
        assert_eq!(array[&[9]], 1);
        assert!(catch_unwind(|| array[&[10]]).is_err());

        array[&[5]] = 99;
        assert_eq!(array[&[4]], 1);
        assert_eq!(array[&[5]], 99);
        assert_eq!(array[&[6]], 1);
    }

    #[test]
    fn nd_array() {
        let mut array = NdArray::new([2, 3, 4], 0);
        array[&[1, 2, 3]] = 10;
        assert_eq!(array[&[1, 2, 3]], 10);
        assert_eq!(array[&[1, 0, 0]], 0);
        assert!(catch_unwind(|| array[&[2, 0, 0]]).is_err());
        assert_eq!(array[&[0, 2, 0]], 0);
        assert!(catch_unwind(|| array[&[0, 3, 0]]).is_err());
        assert_eq!(array[&[0, 0, 3]], 0);
        assert!(catch_unwind(|| array[&[0, 0, 4]]).is_err());
    }

    #[test]
    fn nd_array_0d() {
        assert!(catch_unwind(|| NdArray::new([], 0)).is_err());
        assert!(catch_unwind(|| NdArray::new([1, 2, 0], 0)).is_err());
        assert!(catch_unwind(|| NdArray::new([0, usize::MAX], 0)).is_err());
    }

    #[test]
    fn nd_array_overflow() {
        // Panics at the allocator code since sizeof(usize) > 2
        // NdArray::new([usize::MAX / 2], 0);
        assert!(catch_unwind(|| NdArray::new([usize::MAX / 2 + 1, 2], 0)).is_err());
        assert!(catch_unwind(|| NdArray::new(
            [usize::MAX / 3, usize::MAX / 3, usize::MAX / 3 + 1],
            0
        ))
        .is_err());
    }

    #[test]
    fn nd_array_view() {
        let mut array = NdArray::new([2, 5, 3], 0);
        for i in 0..30 {
            array[&[i / 15, (i / 3) % 5, i % 3]] = i + 1;
        }
        assert_eq!(array.iter().count(), array.len());

        let mut iter = array.iter();
        for i in 1..=30 {
            assert_eq!(iter.next(), Some(&i));
        }
        assert_eq!(iter.next(), None);
        assert_eq!(iter.next(), None);

        let mut iter = array.iter_slice(&[None, Some(2), None]);
        for x in &[7, 8, 9, 22, 23, 24] {
            assert_eq!(iter.next(), Some(x));
        }
        assert_eq!(iter.next(), None);
    }
}
