// NAME rng
// START code
use std::ops::{Bound, RangeBounds};

/// Random number generator using [`xorshift*`].
/// It has a maximal period of `2^64 - 1`.
///
/// [`xorshift*`]: https://en.wikipedia.org/wiki/Xorshift#xorshift*
#[derive(Debug, Clone)]
pub struct Rng {
    state: u64,
}

impl Rng {
    /// Creates a new `Rng` seeded with the given value.
    ///
    /// Panics if `seed == 0` since this is not a valid seed.
    pub fn new(seed: u64) -> Rng {
        assert!(seed > 0, "xorshift generator cannot be seeded with 0");
        Rng { state: seed }
    }

    /// Creates a new `Rng` from a hardware-generated seed.
    ///
    /// Panics if the current CPU does not support `rdseed` or the iteration limit is exceeded.
    const RDSEED_ITER_LIMIT: i64 = 50_000;
    #[cfg(target_arch = "x86_64")]
    pub fn hardware() -> Rng {
        use std::arch::x86_64::_rdseed64_step;

        unsafe fn rdseed64(limit: i64) -> u64 {
            let mut seed = 0;
            let mut iters = 0;
            while _rdseed64_step(&mut seed) != 1 {
                iters += 1;
                assert!(
                    iters <= limit,
                    "rdseed iteration limit ({}) exceeded, something has gone very wrong",
                    limit
                );
            }
            seed
        }

        assert!(is_x86_feature_detected!("rdseed"), "rdseed not supported");
        // SAFETY: This block is only compiled with target_arch = x86_64 and we have checked
        // rdseed is supported using is_x86_feature_detected.
        unsafe { Rng::new(rdseed64(Self::RDSEED_ITER_LIMIT)) }
    }

    /// Creates a new `Rng` from a hardware-generated seed.
    ///
    /// Panics if the current CPU does not support `rdseed` or the iteration limit is exceeded.
    #[cfg(target_arch = "x86")]
    pub fn hardware() -> Rng {
        use std::arch::x86::_rdseed32_step;

        unsafe fn rdseed32(limit: i64) -> u32 {
            let mut seed = 0;
            let mut iters = 0;
            while _rdseed32_step(&mut seed) != 1 {
                iters += 1;
                assert!(
                    iters <= limit,
                    "rdseed iteration limit ({}) exceeded, something has gone very wrong",
                    limit
                );
            }
            seed
        }

        assert!(is_x86_feature_detected!("rdseed"), "rdseed not supported");
        // SAFETY: This block is only compiled with target_arch = x86 and we have checked
        // rdseed is supported using is_x86_feature_detected.
        unsafe {
            Rng::new(
                (rdseed32(Self::RDSEED_ITER_LIMIT) as u64) << 32
                    | rdseed32(Self::RDSEED_ITER_LIMIT) as u64,
            )
        }
    }

    /// Generates 64 random bits.
    #[allow(clippy::should_implement_trait)]
    pub fn next(&mut self) -> u64 {
        self.state ^= self.state >> 12;
        self.state ^= self.state << 25;
        self.state ^= self.state >> 27;
        self.state.wrapping_mul(0x2545f4914f6cdd1d)
    }

    /// Generates a random value of type `T` using the standard distrbution for the type.
    pub fn gen<T>(&mut self) -> T
    where
        T: Generatable<Standard>,
    {
        self.gen_dist(&Standard::default())
    }

    /// Generates a random value of type `T` from the distribution `dist`.
    pub fn gen_dist<T, D>(&mut self, dist: &D) -> T
    where
        T: Generatable<D>,
    {
        T::generate(self, dist)
    }
}

/// A type that can be generated using a distribution `D`.
pub trait Generatable<D> {
    fn generate(rng: &mut Rng, dist: &D) -> Self;
}

/// Standard distribution for generating numbers.
///
/// Corresponds to the following distributions:
/// - integers, bool: uniform over all possible values
/// - float: uniform over `[0, 1)`
#[derive(Debug, Clone, Default)]
pub struct Standard(());

impl Generatable<Standard> for u64 {
    fn generate(rng: &mut Rng, _dist: &Standard) -> u64 {
        rng.next()
    }
}

impl Generatable<Standard> for f64 {
    fn generate(rng: &mut Rng, _dist: &Standard) -> f64 {
        let bits = rng.next();
        let float = (1023 << 52) + (bits & ((1 << 52) - 1));
        f64::from_bits(float) - 1.0
    }
}

impl Generatable<Standard> for bool {
    fn generate(rng: &mut Rng, _dist: &Standard) -> bool {
        rng.next() & 1 > 0
    }
}

macro_rules! generate_standard_impl_by_cast {
    ($( $source:ty => $target:ty ),+) => {
        $(
            impl Generatable<Standard> for $target {
                fn generate(rng: &mut Rng, _dist: &Standard) -> $target {
                    rng.gen::<$source>() as $target
                }
            }
        )+
    };
}

generate_standard_impl_by_cast!(f64 => f32, u64 => u32, u64 => u16, u64 => u8, u64 => i64, i64 => i32, i64 => i16, i64 => i8);

/// Uniform integer distribution representing `[min, max]`.
#[derive(Debug, Clone)]
pub struct UniformInt {
    min: u64,
    max: u64,
    mask: u64,
}

impl UniformInt {
    /// Creates a new uniform distribution covering `range`.
    ///
    /// Panics if the range is empty.
    pub fn new(range: impl RangeBounds<u64>) -> UniformInt {
        let min = match range.start_bound() {
            Bound::Included(&x) => x,
            Bound::Excluded(&x) => x.checked_add(1).expect("minimum overflowed u64"),
            Bound::Unbounded => 0,
        };
        let max = match range.end_bound() {
            Bound::Included(&x) => x,
            Bound::Excluded(&x) => x.checked_sub(1).expect("minimum underflowed u64"),
            Bound::Unbounded => u64::MAX,
        };
        assert!(min <= max, "range empty: min > max ({} > {})", min, max);

        if max - min == u64::MAX {
            UniformInt { min, max, mask: !0 }
        } else {
            let len = max - min + 1;
            let bit_len = 0u64.leading_zeros() - len.leading_zeros();
            UniformInt {
                min,
                max,
                mask: if bit_len == 0u64.leading_zeros() {
                    !0
                } else {
                    (1 << bit_len) - 1
                },
            }
        }
    }
}

impl Generatable<UniformInt> for u64 {
    fn generate(rng: &mut Rng, dist: &UniformInt) -> Self {
        loop {
            let x = rng.next() & dist.mask;
            if x <= dist.max - dist.min {
                return dist.min + x;
            }
        }
    }
}

/// Helper trait for the `.shuffle()` method.
pub trait Shuffle {
    /// Shuffles `self` into a random permutation using `rng`.
    fn shuffle(&mut self, rng: &mut Rng);
}

impl<T> Shuffle for [T] {
    /// Shuffles `self` into a random permutation using `rng`.
    ///
    /// This implementation uses the Fisher-Yates algorithm.
    fn shuffle(&mut self, rng: &mut Rng) {
        for i in (1..self.len()).rev() {
            let j: u64 = rng.gen_dist(&UniformInt::new(0..=i as u64));
            self.swap(i, j as usize);
        }
    }
}
// END code

/* Templates
// START imports
{Rng, UniformInt, Shuffle}
// END imports
// START include_main
let mut rng = Rng::hardware();
// END include_main
*/

#[cfg(test)]
mod tests {
    use super::*;
    use std::{ops::Range, panic::catch_unwind};

    #[test]
    fn standard() {
        let mut rng = Rng::new(1);
        rng.gen::<u64>();
        rng.gen::<u32>();
        rng.gen::<u16>();
        rng.gen::<u8>();
        rng.gen::<i64>();
        rng.gen::<i32>();
        rng.gen::<i16>();
        rng.gen::<i8>();
        rng.gen::<bool>();
        rng.gen::<f64>();
        rng.gen::<f32>();
    }

    #[test]
    fn uniform_int() {
        #[track_caller]
        fn assert_in_range(rng: &mut Rng, range: Range<u64>) {
            let x: u64 = rng.gen_dist(&UniformInt::new(range.clone()));
            assert!(range.start <= x);
            assert!(x < range.end);
        }

        let mut rng = Rng::new(1);
        assert_in_range(&mut rng, 0..1);
        assert_in_range(&mut rng, 10..18);
        assert_in_range(&mut rng, 10..19);
        assert_in_range(&mut rng, 10..17);
        assert_in_range(&mut rng, u64::MAX - 1..u64::MAX);
        assert_in_range(&mut rng, 0..u64::MAX);
        assert_in_range(&mut rng, 8420861363088252596..9819222528813816460);
        assert!(catch_unwind(|| rng.clone().gen_dist::<u64, _>(&UniformInt::new(1..1))).is_err());
        assert!(catch_unwind(|| rng.clone().gen_dist::<u64, _>(&UniformInt::new(..0))).is_err());
        assert!(
            catch_unwind(|| rng.clone().gen_dist::<u64, _>(&UniformInt::new((
                Bound::Excluded(u64::MAX),
                Bound::Unbounded
            ))))
            .is_err()
        );
        assert!(catch_unwind(|| rng.clone().gen_dist::<u64, _>(&UniformInt::new(5..=1))).is_err());
    }

    #[test]
    fn shuffle() {
        let mut rng = Rng::new(1);
        let mut arr = (0..10).collect::<Vec<i64>>();
        arr.shuffle(&mut rng);
        // This also tests the generator is deterministic but this will be covered more
        // comprehensively in a future test.
        assert_eq!(arr, [3, 2, 9, 6, 4, 8, 5, 1, 0, 7]);

        let mut arr = ["hello", "world", "!"];
        arr.shuffle(&mut rng);
        assert_eq!(arr, ["world", "hello", "!"]);
    }
}
