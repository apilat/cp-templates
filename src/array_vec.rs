// NAME array_vec
// START code
use core::slice;
use std::fmt::{self, Debug, Formatter};
use std::mem::{size_of, MaybeUninit};
use std::ops::{Deref, DerefMut};
use std::ptr;

pub enum ArrayVec<T, const N: usize> {
    Inline {
        arr: [MaybeUninit<T>; N],
        len: usize,
    },
    Heap(Vec<T>),
}

impl<T, const N: usize> Debug for ArrayVec<T, N>
where
    T: Debug,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_list().entries(self.iter()).finish()
    }
}

impl<T, const N: usize> Clone for ArrayVec<T, N>
where
    T: Clone,
{
    fn clone(&self) -> Self {
        match self {
            ArrayVec::Inline { arr, len } => ArrayVec::Inline {
                arr: unsafe {
                    let mut arr_new: [MaybeUninit<T>; N] = MaybeUninit::uninit().assume_init();
                    for i in 0..*len {
                        // TODO Prevent memory leak if `T::clone()` panics.
                        let elem = T::clone(arr.get_unchecked(i).assume_init_ref());
                        arr_new.get_unchecked_mut(i).as_mut_ptr().write(elem);
                    }
                    arr_new
                },
                len: *len,
            },
            ArrayVec::Heap(vec) => ArrayVec::Heap(vec.clone()),
        }
    }
}

impl<T, const N: usize> Drop for ArrayVec<T, N> {
    fn drop(&mut self) {
        match self {
            ArrayVec::Inline { arr, len } => unsafe {
                ptr::drop_in_place(slice::from_raw_parts_mut(arr.as_mut_ptr() as *mut T, *len))
            },
            ArrayVec::Heap(_) => (), // Vec is automatically dropped
        }
    }
}

impl<T, const N: usize> Deref for ArrayVec<T, N> {
    type Target = [T];
    fn deref(&self) -> &Self::Target {
        self.as_slice()
    }
}

impl<T, const N: usize> DerefMut for ArrayVec<T, N> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.as_mut_slice()
    }
}

impl<T, const N: usize> Default for ArrayVec<T, N> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T, const N: usize> ArrayVec<T, N> {
    pub fn new() -> Self {
        assert!(size_of::<T>() > 0, "zero-sized types are not supported");
        ArrayVec::Inline {
            arr: unsafe { MaybeUninit::uninit().assume_init() },
            len: 0,
        }
    }

    pub fn as_slice(&self) -> &[T] {
        match self {
            ArrayVec::Inline { arr, len } => unsafe {
                slice::from_raw_parts(arr.as_ptr() as *const T, *len)
            },
            ArrayVec::Heap(vec) => vec.as_slice(),
        }
    }

    pub fn as_mut_slice(&mut self) -> &mut [T] {
        match self {
            ArrayVec::Inline { arr, len } => unsafe {
                slice::from_raw_parts_mut(arr.as_mut_ptr() as *mut T, *len)
            },
            ArrayVec::Heap(vec) => vec.as_mut_slice(),
        }
    }

    pub fn len(&self) -> usize {
        match self {
            ArrayVec::Inline { len, .. } => *len,
            ArrayVec::Heap(vec) => vec.len(),
        }
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn push(&mut self, val: T) {
        match self {
            ArrayVec::Inline { arr, len } => unsafe {
                if *len < N {
                    arr.get_unchecked_mut(*len).write(val);
                    *len += 1;
                } else {
                    debug_assert!(*len == N);
                    let mut vec = Vec::with_capacity(N + 1);
                    for i in 0..*len {
                        vec.push(arr.get_unchecked(i).assume_init_read());
                    }
                    vec.push(val);
                    // Make sure we don't accidentally drop the data from under us.
                    (self as *mut Self).write(ArrayVec::Heap(vec));
                }
            },
            ArrayVec::Heap(vec) => vec.push(val),
        }
    }

    pub fn pop(&mut self) -> Option<T> {
        match self {
            ArrayVec::Inline { arr, len } => unsafe {
                if *len > 0 {
                    *len -= 1;
                    Some(arr.get_unchecked(*len).assume_init_read())
                } else {
                    None
                }
            },
            ArrayVec::Heap(vec) => vec.pop(),
        }
    }
}
// END code

/*
// START imports
{ArrayVec}
// END imports
*/

#[cfg(test)]
mod tests {
    use super::*;
    use std::{cell::Cell, panic::catch_unwind};

    #[derive(Debug)]
    struct Counted<'a, T> {
        count: &'a Cell<usize>,
        val: T,
    }
    impl<'a, T> Counted<'a, T> {
        fn new(count: &'a Cell<usize>, x: T) -> Self {
            count.set(count.get() + 1);
            Counted { count, val: x }
        }
    }
    impl<T: Clone> Clone for Counted<'_, T> {
        fn clone(&self) -> Self {
            self.count.set(self.count.get() + 1);
            Self {
                count: self.count,
                val: self.val.clone(),
            }
        }
    }
    impl<T> Drop for Counted<'_, T> {
        fn drop(&mut self) {
            self.count.set(self.count.get() - 1);
        }
    }

    #[test]
    fn no_inline() {
        let drop_counter = Cell::new(0);
        let cnt = |x: i32| Counted::new(&drop_counter, x);
        let mut arr: ArrayVec<Counted<i32>, 0> = ArrayVec::new();
        arr.push(cnt(1));
        assert_eq!(arr.len(), 1);
        arr.push(cnt(2));
        assert_eq!(arr.len(), 2);
        arr.push(cnt(3));
        assert_eq!(arr.len(), 3);
        assert_eq!(arr.pop().map(|x| x.val), Some(3));
        assert_eq!(arr.len(), 2);
        assert_eq!(arr.pop().map(|x| x.val), Some(2));
        assert_eq!(arr.len(), 1);
        assert_eq!(arr.pop().map(|x| x.val), Some(1));
        assert_eq!(arr.len(), 0);
        assert_eq!(arr.pop().map(|x| x.val), None);
        assert_eq!(arr.len(), 0);
        assert!(drop_counter.get() == 0);
    }

    #[test]
    fn all_inline() {
        let drop_counter = Cell::new(0);
        let cnt = |x: i32| Counted::new(&drop_counter, x);
        let mut arr: ArrayVec<Counted<i32>, 8> = ArrayVec::new();
        arr.push(cnt(1));
        assert_eq!(arr.len(), 1);
        arr.push(cnt(2));
        assert_eq!(arr.len(), 2);
        arr.push(cnt(3));
        assert_eq!(arr.len(), 3);
        assert_eq!(arr.pop().map(|x| x.val), Some(3));
        assert_eq!(arr.len(), 2);
        assert_eq!(arr.pop().map(|x| x.val), Some(2));
        assert_eq!(arr.len(), 1);
        assert_eq!(arr.pop().map(|x| x.val), Some(1));
        assert_eq!(arr.len(), 0);
        assert_eq!(arr.pop().map(|x| x.val), None);
        assert_eq!(arr.len(), 0);
        assert_eq!(drop_counter.get(), 0);
    }

    #[test]
    fn partial_inline() {
        let drop_counter = Cell::new(0);
        let cnt = |x: usize| Counted::new(&drop_counter, x);
        let mut arr: ArrayVec<Counted<usize>, 8> = ArrayVec::new();
        for i in 0..16 {
            assert_eq!(arr.len(), i);
            arr.push(cnt(i));
            assert_eq!(arr.len(), i + 1);
        }
        for i in (0..16).rev() {
            assert_eq!(arr.len(), i + 1);
            assert_eq!(arr.pop().map(|x| x.val), Some(i));
            assert_eq!(arr.len(), i);
        }
        assert_eq!(arr.pop().map(|x| x.val), None);
        assert_eq!(arr.len(), 0);
        assert_eq!(drop_counter.get(), 0);
    }

    #[test]
    fn clone() {
        let drop_counter = Cell::new(0);
        let cnt = |x: i32| Counted::new(&drop_counter, x);
        let mut arr: ArrayVec<Counted<i32>, 8> = ArrayVec::new();
        for i in 0..8 {
            arr.push(cnt(i));
        }
        assert!(arr.clone().iter().map(|x| x.val).eq(0..8));
        assert!(arr.iter().map(|x| x.val).eq(0..8));
        for i in 8..16 {
            arr.push(cnt(i));
        }
        assert!(arr.clone().iter().map(|x| x.val).eq(0..16));
        assert!(arr.iter().map(|x| x.val).eq(0..16));
        drop(arr);
        assert_eq!(drop_counter.get(), 0);
    }

    #[test]
    fn zst() {
        assert!(catch_unwind(|| ArrayVec::<(), 16>::new()).is_err());
        assert!(catch_unwind(|| ArrayVec::<(), 16>::default()).is_err());
    }

    /* Turns out this produces a compilation-time error instead so cannot test here.
    #[test]
    fn extreme_size() {
        assert!(catch_unwind(|| ArrayVec::<u8, { isize::MAX as usize }>::new()).is_err());
        assert!(catch_unwind(|| ArrayVec::<u8, { usize::MAX }>::new()).is_err());
    }
    */
}
