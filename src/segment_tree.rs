// NAME segment_tree
// START code
use self::formatter::Formatter;
use self::operation::Operation;
use self::spec::SpecExt;
use self::update::Update;
use std::{
    fmt::{self, Debug},
    ops::{Bound, Range, RangeBounds},
};

/// The specification of an operation used in a segment tree, along with possible range updates,
/// a formatter for the user-facing interface.
pub trait Spec {
    type Operation: Operation<Spec = Self>;
    type Formatter: Formatter<Spec = Self>;
    type Update: Update<Spec = Self>;
}

/// Segment tree which supports point updates, range queries and range updates (lazily propagated)
/// for arbitrary associative operations.
///
/// Time complexity of operations, assuming all `Spec` functions are `O(1)`:
/// - `new: O(n)`
/// - `query: O(log n)`
/// - `set: O(log n)`
/// - `get:` `O(log n)` if using range updates, `O(1)` otherwise
/// - `update: O(log n)`
/// - `view, view_mut: O(log n + r - l)`
///
/// The implementation is based on
/// [Efficient and easy segment trees](https://codeforces.com/blog/entry/18051)
pub struct SegmentTree<S: Spec> {
    spec: S,
    data: Vec<<S::Operation as Operation>::Node>,
    updates: Vec<Option<<S::Update as Update>::Update>>,
    // Until any range operations are done, this is set to false, which improves performance.
    needs_update_propagation: bool,
}

impl<S> Debug for SegmentTree<S>
where
    S: Spec + Debug,
    <S::Operation as Operation>::Node: Debug,
    <S::Update as Update>::Update: Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("SegmentTree")
            .field("spec", &self.spec)
            .field("data", &self.data)
            .field("updates", &self.updates)
            .field("needs_update_propagation", &self.needs_update_propagation)
            .finish()
    }
}

impl<S> Clone for SegmentTree<S>
where
    S: Spec + Clone,
    <S::Operation as Operation>::Node: Clone,
    <S::Update as Update>::Update: Clone,
{
    fn clone(&self) -> Self {
        SegmentTree {
            spec: self.spec.clone(),
            data: self.data.clone(),
            updates: self.updates.clone(),
            needs_update_propagation: self.needs_update_propagation,
        }
    }
}

impl<S: Spec> SegmentTree<S> {
    /// Creates a new segment tree with the elements from `vals`.
    #[track_caller]
    pub fn new(vals: Vec<<S::Formatter as Formatter>::Input>, spec: S) -> Self {
        let n = vals.len();

        // We don't want to fail on n = 0 so simply saturate 2*n-1 capacity to 0.
        let mut data = Vec::with_capacity(
            n.checked_mul(2)
                .expect("size needed for tree overflowed")
                .saturating_sub(1),
        );

        // We could avoid reversing the vector by allocating it and initializing from the back but
        // that would require unsafe code so this is probably good enough.
        data.extend(
            vals.into_iter()
                .enumerate()
                .map(|(i, x)| spec.input(x, i))
                .rev(),
        );
        for i in 0..n.saturating_sub(1) {
            data.push(spec.op(&data[2 * i + 1], &data[2 * i]));
        }
        data.reverse();

        let mut updates = Vec::with_capacity(n.saturating_sub(1));
        updates.resize_with(n.saturating_sub(1), || None);

        SegmentTree {
            spec,
            data,
            updates,
            needs_update_propagation: false,
        }
    }

    /// Returns the length of the underlying array.
    pub fn len(&self) -> usize {
        (self.data.len() + 1) / 2
    }

    /// Returns `true` if the underyling array is empty.
    pub fn is_empty(&self) -> bool {
        self.data.is_empty()
    }

    /// Returns the height of the tree.
    pub fn height(&self) -> usize {
        let n = self.len();
        (0usize.leading_zeros() - n.leading_zeros()) as usize
    }

    /// Returns the aggregate of `op` applied for all elements in `range`.
    #[track_caller]
    pub fn query(&mut self, range: impl RangeBounds<usize>) -> <S::Formatter as Formatter>::Output {
        let Range {
            start: left,
            end: right,
        } = self.verify_range_bounds(range);

        if self.needs_update_propagation {
            self.push_updates_to(left + self.len());
            self.push_updates_to(right + self.len() - 1);
        }

        let (mut res_l_owned, mut res_r_owned);
        let (mut res_l, mut res_r) = (None, None);
        let (mut l, mut r) = (left + self.len(), right + self.len());
        while l < r {
            if l % 2 == 1 {
                if let Some(cur_l) = res_l.take() {
                    res_l_owned = self.spec.op(cur_l, &self.data[l - 1]);
                    res_l = Some(&res_l_owned);
                } else {
                    res_l = Some(&self.data[l - 1]);
                }
                l += 1;
            }
            if r % 2 == 1 {
                r -= 1;
                if let Some(cur_r) = res_r {
                    res_r_owned = self.spec.op(&self.data[r - 1], cur_r);
                    res_r = Some(&res_r_owned);
                } else {
                    res_r = Some(&self.data[r - 1]);
                }
            }
            l /= 2;
            r /= 2;
        }

        let res_final;
        self.spec.output(match (res_l, res_r) {
            (Some(l), Some(r)) => {
                res_final = self.spec.op(l, r);
                &res_final
            }
            (Some(x), None) | (None, Some(x)) => x,
            (None, None) => unreachable!("empty query"),
        })
    }

    /// Verifies the range bounds are valid and returns the range `[l, r)` they represent.
    /// Panics on error.
    #[track_caller]
    fn verify_range_bounds(&self, range: impl RangeBounds<usize>) -> Range<usize> {
        let left = match range.start_bound() {
            Bound::Included(&x) => x,
            Bound::Excluded(&x) => x + 1,
            Bound::Unbounded => 0,
        };
        let right = match range.end_bound() {
            Bound::Included(&x) => x + 1,
            Bound::Excluded(&x) => x,
            Bound::Unbounded => self.len(),
        };
        assert!(
            left < right,
            "range inverted: left >= right ({} >= {})",
            left,
            right
        );
        assert!(
            right <= self.len(),
            "range out of bounds: right > n ({} > {})",
            right,
            self.len()
        );
        Range {
            start: left,
            end: right,
        }
    }

    /// Pushes updates from all parents to `i`.
    fn push_updates_to(&mut self, i: usize) {
        for h in (1..self.height()).rev() {
            let ip = i >> h;
            if let Some(upd) = self.updates[ip - 1].take() {
                self.apply_update_to(2 * ip, &upd);
                self.apply_update_to(2 * ip + 1, &upd);
            }
        }
    }

    /// Pushes updates from all parents to `range`.
    fn push_updates_to_range(&mut self, range: Range<usize>) {
        let (l, r) = (range.start, range.end - 1);
        for h in (1..self.height()).rev() {
            let (lp, rp) = (l >> h, r >> h);
            for x in lp..=rp {
                if let Some(upd) = self.updates[x - 1].take() {
                    self.apply_update_to(2 * x, &upd);
                    self.apply_update_to(2 * x + 1, &upd);
                }
            }
        }
    }

    /// Updates all parents of node `i`.
    fn update_parents_of(&mut self, i: usize) {
        for h in 1..self.height() {
            let ip = i >> h;
            self.data[ip - 1] = self.spec.op(&self.data[2 * ip - 1], &self.data[2 * ip]);
            if self.needs_update_propagation {
                if let Some(ref upd) = self.updates[ip - 1] {
                    self.data[ip - 1] = self.spec.apply(upd, &self.data[ip - 1]);
                }
            }
        }
    }

    /// Updates all parents of nodes in `range`.
    fn update_parents_of_range(&mut self, range: Range<usize>) {
        let (l, r) = (range.start, range.end - 1);
        for h in 1..self.height() {
            let (lp, rp) = (l >> h, r >> h);
            for x in lp..=rp {
                self.data[x - 1] = self.spec.op(&self.data[2 * x - 1], &self.data[2 * x]);
                if self.needs_update_propagation {
                    if let Some(ref upd) = self.updates[x - 1] {
                        self.data[x - 1] = self.spec.apply(upd, &self.data[x - 1]);
                    }
                }
            }
        }
    }

    /// Applies update `f` to node `i` (non-recursively).
    fn apply_update_to(&mut self, i: usize, f: &<S::Update as Update>::Update) {
        self.data[i - 1] = self.spec.apply(f, &self.data[i - 1]);
        if i < self.len() {
            self.updates[i - 1] = match self.updates[i - 1].take() {
                None => Some(f.clone()),
                Some(g) => Some(self.spec.compose(&g, f)),
            };
        }
    }

    /// Verifies the index is in bounds and returns it. Panics on error.
    #[track_caller]
    fn verify_index(&self, index: usize) -> usize {
        assert!(
            index < self.len(),
            "index out of bounds: index >= n ({} >= {})",
            index,
            self.len()
        );
        index
    }

    /// Sets the element at `index` to `val`.
    #[track_caller]
    pub fn set(&mut self, index: usize, val: <S::Formatter as Formatter>::Input) {
        let i = self.verify_index(index) + self.len();
        if self.needs_update_propagation {
            self.push_updates_to(i);
        }
        self.data[i - 1] = self.spec.input(val, i);
        self.update_parents_of(i)
    }

    /// Gets the element at `index`.
    #[track_caller]
    pub fn get(&mut self, index: usize) -> <S::Formatter as Formatter>::Output {
        let i = self.verify_index(index) + self.len();
        if self.needs_update_propagation {
            self.push_updates_to(i);
        }
        self.spec.output(&self.data[i - 1])
    }

    /// Updates the elements inside `range` lazily using `update`.
    #[track_caller]
    pub fn update(
        &mut self,
        range: impl RangeBounds<usize>,
        update: <S::Update as Update>::Update,
    ) {
        let Range {
            start: left,
            end: right,
        } = self.verify_range_bounds(range);

        // If no range updates had been done before, we don't have to propagate anything.
        if self.needs_update_propagation {
            self.push_updates_to(left + self.len());
            self.push_updates_to(right + self.len() - 1);
        }
        self.needs_update_propagation = true;

        let (mut l, mut r) = (left + self.len(), right + self.len());
        while l < r {
            if l % 2 == 1 {
                self.apply_update_to(l, &update);
                l += 1;
            }
            if r % 2 == 1 {
                r -= 1;
                self.apply_update_to(r, &update);
            }
            l /= 2;
            r /= 2;
        }

        self.update_parents_of(left + self.len());
        self.update_parents_of(right + self.len() - 1);
    }

    /// Pushes updates to nodes in `range` and inspects them using `f`.
    #[track_caller]
    pub fn view(
        &mut self,
        range: impl RangeBounds<usize>,
        f: impl FnOnce(&[<S::Operation as Operation>::Node]),
    ) {
        let Range {
            start: left,
            end: right,
        } = self.verify_range_bounds(range);

        let range = left + self.len()..right + self.len();
        if self.needs_update_propagation {
            self.push_updates_to_range(range.clone());
        }
        f(&self.data[range.start - 1..range.end - 1]);
    }

    /// Pushes updates to nodes in `range` and modifies them using `f` while maintaining the segment
    /// tree structure.
    #[track_caller]
    pub fn view_mut(
        &mut self,
        range: impl RangeBounds<usize>,
        f: impl FnOnce(&mut [<S::Operation as Operation>::Node]),
    ) {
        let Range {
            start: left,
            end: right,
        } = self.verify_range_bounds(range);

        let range = left + self.len()..right + self.len();
        if self.needs_update_propagation {
            self.push_updates_to_range(range.clone());
        }
        f(&mut self.data[range.start - 1..range.end - 1]);
        self.update_parents_of_range(range);
    }
}

pub mod operation {
    use super::Spec;
    use std::{cmp, marker::PhantomData};

    /// An operation and query specification for a segment tree.
    ///
    /// The operation must be associative, i.e. `merge(merge(x, y), z) = merge(x, merge(y, z))` for any nodes `x,y,z`.
    pub trait Operation {
        type Spec: Spec;
        type Node;
        fn op(s: &Self::Spec, a: &Self::Node, b: &Self::Node) -> Self::Node;
    }

    pub struct Add<S, T>(PhantomData<(S, T)>);
    impl<S, T> Operation for Add<S, T>
    where
        S: Spec,
        T: Clone + std::ops::Add<Output = T>,
    {
        type Spec = S;
        type Node = T;

        #[inline(always)]
        fn op(_: &Self::Spec, a: &Self::Node, b: &Self::Node) -> Self::Node {
            a.clone() + b.clone()
        }
    }

    pub struct Min<S, T>(PhantomData<(S, T)>);
    impl<S, T> Operation for Min<S, T>
    where
        S: Spec,
        T: Clone + Ord,
    {
        type Spec = S;
        type Node = T;

        #[inline(always)]
        fn op(_: &Self::Spec, a: &Self::Node, b: &Self::Node) -> Self::Node {
            cmp::min(a.clone(), b.clone())
        }
    }

    pub struct Max<S, T>(PhantomData<(S, T)>);
    impl<S, T> Operation for Max<S, T>
    where
        S: Spec,
        T: Clone + Ord,
    {
        type Spec = S;
        type Node = T;

        #[inline(always)]
        fn op(_: &Self::Spec, a: &Self::Node, b: &Self::Node) -> Self::Node {
            cmp::max(a.clone(), b.clone())
        }
    }

    pub struct Argmin<S, T>(PhantomData<(S, T)>);
    impl<S, T> Operation for Argmin<S, T>
    where
        S: Spec,
        T: Clone + Ord,
    {
        type Spec = S;
        type Node = (T, usize);

        #[inline(always)]
        fn op(_: &Self::Spec, a: &Self::Node, b: &Self::Node) -> Self::Node {
            if a.0 <= b.0 {
                a.clone()
            } else {
                b.clone()
            }
        }
    }

    pub struct Argmax<S, T>(PhantomData<(S, T)>);
    impl<S, T> Operation for Argmax<S, T>
    where
        S: Spec,
        T: Clone + Ord,
    {
        type Spec = S;
        type Node = (T, usize);

        #[inline(always)]
        fn op(_: &Self::Spec, a: &Self::Node, b: &Self::Node) -> Self::Node {
            if a.0 >= b.0 {
                a.clone()
            } else {
                b.clone()
            }
        }
    }
}

pub mod formatter {
    use super::{operation::Operation, Spec};
    use std::marker::PhantomData;

    /// A formatter specification for a segment tree.
    pub trait Formatter {
        type Spec: Spec;
        type Input;
        type Output;

        fn input(
            s: &Self::Spec,
            a: Self::Input,
            i: usize,
        ) -> <<Self::Spec as Spec>::Operation as Operation>::Node;
        fn output(
            s: &Self::Spec,
            a: &<<Self::Spec as Spec>::Operation as Operation>::Node,
        ) -> Self::Output;
    }

    pub struct Noop<S, T>(PhantomData<(S, T)>);
    impl<T, S> Formatter for Noop<S, T>
    where
        S: Spec,
        S::Operation: Operation<Node = T>,
        T: Clone,
    {
        type Spec = S;
        type Input = T;
        type Output = T;

        #[inline(always)]
        fn input(_: &Self::Spec, a: Self::Input, _i: usize) -> <S::Operation as Operation>::Node {
            a
        }
        #[inline(always)]
        fn output(_: &Self::Spec, a: &<S::Operation as Operation>::Node) -> Self::Output {
            a.clone()
        }
    }

    pub struct ValueIndex<S, T>(PhantomData<(S, T)>);
    impl<T, S> Formatter for ValueIndex<S, T>
    where
        S: Spec,
        S::Operation: Operation<Node = (T, usize)>,
    {
        type Spec = S;
        type Input = T;
        type Output = usize;

        #[inline(always)]
        fn input(_: &Self::Spec, a: Self::Input, i: usize) -> <S::Operation as Operation>::Node {
            (a, i)
        }
        #[inline(always)]
        fn output(_: &Self::Spec, a: &<S::Operation as Operation>::Node) -> Self::Output {
            a.1
        }
    }
}

pub mod update {
    use super::{operation::Operation, Spec};
    use std::{cmp, marker::PhantomData};

    /// A renage update operation for a segment tree.
    ///
    /// `compose` and `apply` must satisfy: `apply(compose(f, g), a) =
    /// apply(f, apply(g, x))` for any updates `f,g` and node `x`.
    pub trait Update {
        type Spec: Spec;
        type Update: Clone;
        fn apply(
            s: &Self::Spec,
            f: &Self::Update,
            a: &<<Self::Spec as Spec>::Operation as Operation>::Node,
        ) -> <<Self::Spec as Spec>::Operation as Operation>::Node;
        fn compose(s: &Self::Spec, f: &Self::Update, g: &Self::Update) -> Self::Update;
    }

    pub struct Noop<S>(PhantomData<S>);
    impl<S> Update for Noop<S>
    where
        S: Spec,
    {
        type Spec = S;
        type Update = ();

        #[inline(always)]
        fn apply(
            _: &Self::Spec,
            _f: &Self::Update,
            _a: &<S::Operation as Operation>::Node,
        ) -> <S::Operation as Operation>::Node {
            panic!("update not supported")
        }
        #[inline(always)]
        fn compose(_: &Self::Spec, _f: &Self::Update, _g: &Self::Update) -> Self::Update {
            panic!("update not supported")
        }
    }

    pub struct Add<S, T>(PhantomData<(S, T)>);
    impl<S, T> Update for Add<S, T>
    where
        S: Spec,
        S::Operation: Operation<Node = T>,
        T: Clone + std::ops::Add<Output = T>,
    {
        type Spec = S;
        type Update = T;

        #[inline(always)]
        fn apply(
            _: &Self::Spec,
            f: &Self::Update,
            a: &<S::Operation as Operation>::Node,
        ) -> <S::Operation as Operation>::Node {
            f.clone() + a.clone()
        }
        #[inline(always)]
        fn compose(_: &Self::Spec, f: &Self::Update, g: &Self::Update) -> Self::Update {
            f.clone() + g.clone()
        }
    }

    pub struct Min<S, T>(PhantomData<(S, T)>);
    impl<S, T> Update for Min<S, T>
    where
        S: Spec,
        S::Operation: Operation<Node = T>,
        T: Clone + Ord,
    {
        type Spec = S;
        type Update = T;

        #[inline(always)]
        fn apply(
            _: &Self::Spec,
            f: &Self::Update,
            a: &<S::Operation as Operation>::Node,
        ) -> <S::Operation as Operation>::Node {
            cmp::min(f, a).clone()
        }
        #[inline(always)]
        fn compose(_: &Self::Spec, f: &Self::Update, g: &Self::Update) -> Self::Update {
            cmp::min(f, g).clone()
        }
    }

    pub struct Max<S, T>(PhantomData<(S, T)>);
    impl<S, T> Update for Max<S, T>
    where
        S: Spec,
        S::Operation: Operation<Node = T>,
        T: Clone + Ord,
    {
        type Spec = S;
        type Update = T;

        #[inline(always)]
        fn apply(
            _: &Self::Spec,
            f: &Self::Update,
            a: &<S::Operation as Operation>::Node,
        ) -> <S::Operation as Operation>::Node {
            cmp::max(f, a).clone()
        }
        #[inline(always)]
        fn compose(_: &Self::Spec, f: &Self::Update, g: &Self::Update) -> Self::Update {
            cmp::max(f, g).clone()
        }
    }

    pub struct Argmin<S, T>(PhantomData<(S, T)>);
    impl<S, T> Update for Argmin<S, T>
    where
        S: Spec,
        S::Operation: Operation<Node = (T, usize)>,
        T: Clone + Ord,
    {
        type Spec = S;
        type Update = T;

        #[inline(always)]
        fn apply(
            _: &Self::Spec,
            f: &Self::Update,
            a: &<S::Operation as Operation>::Node,
        ) -> <S::Operation as Operation>::Node {
            (cmp::min(f, &a.0).clone(), a.1)
        }
        #[inline(always)]
        fn compose(_: &Self::Spec, f: &Self::Update, g: &Self::Update) -> Self::Update {
            cmp::min(f, g).clone()
        }
    }

    pub struct Argmax<S, T>(PhantomData<(S, T)>);
    impl<S, T> Update for Argmax<S, T>
    where
        S: Spec,
        S::Operation: Operation<Node = (T, usize)>,
        T: Clone + Ord,
    {
        type Spec = S;
        type Update = T;

        #[inline(always)]
        fn apply(
            _: &Self::Spec,
            f: &Self::Update,
            a: &<S::Operation as Operation>::Node,
        ) -> <S::Operation as Operation>::Node {
            (cmp::max(f, &a.0).clone(), a.1)
        }
        #[inline(always)]
        fn compose(_: &Self::Spec, f: &Self::Update, g: &Self::Update) -> Self::Update {
            cmp::max(f, g).clone()
        }
    }
}

pub mod spec {
    use super::{
        formatter::{self, Formatter},
        operation::{self, Operation},
        update::{self, Update},
        Spec,
    };
    use std::{marker::PhantomData, ops::Add};

    pub(super) trait SpecExt: Spec {
        #[inline(always)]
        fn op(
            &self,
            l: &<Self::Operation as Operation>::Node,
            r: &<Self::Operation as Operation>::Node,
        ) -> <Self::Operation as Operation>::Node {
            Self::Operation::op(self, l, r)
        }

        #[inline(always)]
        fn input(
            &self,
            a: <Self::Formatter as Formatter>::Input,
            i: usize,
        ) -> <Self::Operation as Operation>::Node {
            Self::Formatter::input(self, a, i)
        }
        #[inline(always)]
        fn output(
            &self,
            a: &<Self::Operation as Operation>::Node,
        ) -> <Self::Formatter as Formatter>::Output {
            Self::Formatter::output(self, a)
        }

        #[inline(always)]
        fn apply(
            &self,
            f: &<Self::Update as Update>::Update,
            a: &<Self::Operation as Operation>::Node,
        ) -> <Self::Operation as Operation>::Node {
            Self::Update::apply(self, f, a)
        }
        #[inline(always)]
        fn compose(
            &self,
            f: &<Self::Update as Update>::Update,
            g: &<Self::Update as Update>::Update,
        ) -> <Self::Update as Update>::Update {
            Self::Update::compose(self, f, g)
        }
    }

    impl<T: Spec> SpecExt for T {}

    #[derive(Debug, Clone, Default)]
    pub struct Sum<T>(PhantomData<T>);
    impl<T> Spec for Sum<T>
    where
        T: Clone + Add<Output = T>,
    {
        type Operation = operation::Add<Self, T>;
        type Formatter = formatter::Noop<Self, T>;
        type Update = update::Noop<Self>;
    }

    #[derive(Debug, Clone, Default)]
    pub struct Min<T>(PhantomData<T>);
    impl<T> Spec for Min<T>
    where
        T: Clone + Ord,
    {
        type Operation = operation::Min<Self, T>;
        type Formatter = formatter::Noop<Self, T>;
        type Update = update::Min<Self, T>;
    }

    #[derive(Debug, Clone, Default)]
    pub struct Max<T>(PhantomData<T>);
    impl<T> Spec for Max<T>
    where
        T: Clone + Ord,
    {
        type Operation = operation::Max<Self, T>;
        type Formatter = formatter::Noop<Self, T>;
        type Update = update::Max<Self, T>;
    }

    #[derive(Debug, Clone, Default)]
    pub struct Argmin<T>(PhantomData<T>);
    impl<T> Spec for Argmin<T>
    where
        T: Clone + Ord,
    {
        type Operation = operation::Argmin<Self, T>;
        type Formatter = formatter::ValueIndex<Self, T>;
        type Update = update::Argmin<Self, T>;
    }

    #[derive(Debug, Clone, Default)]
    pub struct Argmax<T>(PhantomData<T>);
    impl<T> Spec for Argmax<T>
    where
        T: Clone + Ord,
    {
        type Operation = operation::Argmax<Self, T>;
        type Formatter = formatter::ValueIndex<Self, T>;
        type Update = update::Argmax<Self, T>;
    }

    #[derive(Debug, Clone, Default)]
    pub struct MinAdd<T>(PhantomData<T>);
    impl<T> Spec for MinAdd<T>
    where
        T: Clone + Ord + Add<Output = T>,
    {
        type Operation = operation::Min<Self, T>;
        type Formatter = formatter::Noop<Self, T>;
        type Update = update::Add<Self, T>;
    }

    #[derive(Debug, Clone, Default)]
    pub struct MaxAdd<T>(PhantomData<T>);
    impl<T> Spec for MaxAdd<T>
    where
        T: Clone + Ord + Add<Output = T>,
    {
        type Operation = operation::Max<Self, T>;
        type Formatter = formatter::Noop<Self, T>;
        type Update = update::Add<Self, T>;
    }

    #[derive(Debug, Clone, Default)]
    pub struct SumSet(());
    impl Spec for SumSet {
        type Operation = Self;
        type Formatter = Self;
        type Update = Self;
    }
    impl operation::Operation for SumSet {
        type Spec = Self;
        type Node = (i64, usize);
        #[inline(always)]
        fn op(_: &Self, a: &Self::Node, b: &Self::Node) -> Self::Node {
            (a.0 + b.0, a.1 + b.1)
        }
    }
    impl formatter::Formatter for SumSet {
        type Spec = Self;
        type Input = i64;
        type Output = i64;

        #[inline(always)]
        fn input(
            _: &Self::Spec,
            a: Self::Input,
            _: usize,
        ) -> <<Self::Spec as Spec>::Operation as Operation>::Node {
            (a, 1)
        }
        #[inline(always)]
        fn output(
            _: &Self::Spec,
            a: &<<Self::Spec as Spec>::Operation as Operation>::Node,
        ) -> Self::Output {
            a.0
        }
    }
    impl update::Update for SumSet {
        type Spec = Self;
        type Update = i64;

        #[inline(always)]
        fn apply(
            _: &Self::Spec,
            f: &Self::Update,
            a: &<<Self::Spec as Spec>::Operation as Operation>::Node,
        ) -> <<Self::Spec as Spec>::Operation as Operation>::Node {
            (a.1 as i64 * *f, a.1)
        }
        #[inline(always)]
        fn compose(_: &Self::Spec, _f: &Self::Update, g: &Self::Update) -> Self::Update {
            *g
        }
    }
}
// END code

/* Templates
// START imports
SegmentTree
// END imports
*/

#[cfg(test)]
mod tests {
    use super::{formatter, operation::Operation, spec::*, update, *};
    use std::cell::Cell;
    use std::panic::catch_unwind;

    struct Empty;
    impl Spec for Empty {
        type Operation = Self;
        type Formatter = formatter::Noop<Self, ()>;
        type Update = update::Noop<Self>;
    }
    impl Operation for Empty {
        type Spec = Self;
        type Node = ();
        fn op(_: &Self, _a: &Self::Node, _b: &Self::Node) -> Self::Node {
            ()
        }
    }

    struct Panic;
    impl Spec for Panic {
        type Operation = Self;
        type Formatter = formatter::Noop<Self, i64>;
        type Update = update::Noop<Self>;
    }
    impl Operation for Panic {
        type Spec = Self;
        type Node = i64;
        fn op(_: &Self, _a: &Self::Node, _b: &Self::Node) -> Self::Node {
            unreachable!()
        }
    }

    struct Concat;
    impl Spec for Concat {
        type Operation = Concat;
        type Formatter = formatter::Noop<Self, String>;
        type Update = update::Noop<Self>;
    }
    impl Operation for Concat {
        type Spec = Self;
        type Node = String;
        fn op(_: &Self, a: &Self::Node, b: &Self::Node) -> Self::Node {
            format!("{}{}", a, b)
        }
    }

    #[test]
    fn new() {
        assert_eq!(
            SegmentTree::<Sum<i32>>::new(vec![], Sum::default()).data,
            vec![]
        );
        assert_eq!(
            SegmentTree::<Empty>::new(vec![(), (), ()], Empty).data,
            vec![(), (), (), (), ()]
        );
        assert_eq!(SegmentTree::<Panic>::new(vec![1], Panic).data, vec![1]);

        assert_eq!(
            SegmentTree::<Sum<i8>>::new(vec![3, 4, 0, -1, 2, 5, 4], Sum::default()).data,
            vec![17, 5, 12, 4, 1, 9, 3, 4, 0, -1, 2, 5, 4]
        );
        assert_eq!(
            SegmentTree::<Sum<u8>>::new(vec![1, 2, 3, 4, 4, 3, 2, 1], Sum::default()).data,
            vec![20, 10, 10, 3, 7, 7, 3, 1, 2, 3, 4, 4, 3, 2, 1]
        );
        assert_eq!(
            SegmentTree::<Sum<i32>>::new(vec![0, 5, -3, -2, 4, 8, 7, 2, 2], Sum::default()).data,
            vec![23, 6, 17, 4, 2, 2, 15, 4, 0, 5, -3, -2, 4, 8, 7, 2, 2]
        );

        assert_eq!(
            SegmentTree::<Min<u8>>::new(vec![0, 5, 3, 2, 2, 1], Min::default()).data,
            vec![0, 1, 0, 2, 1, 0, 5, 3, 2, 2, 1]
        );
        assert_eq!(
            SegmentTree::<Concat>::new(
                ["y", "a", "b", "z", "x"]
                    .iter()
                    .map(|x| x.to_string())
                    .collect(),
                Concat
            )
            .data,
            vec!["zxyab", "zxy", "ab", "zx", "y", "a", "b", "z", "x"]
        );
    }

    #[test]
    fn query() {
        let mut tree = SegmentTree::<Sum<u64>>::new((0..=100000).collect(), Sum::default());
        assert_eq!(tree.query(..=0), 0);
        assert_eq!(tree.query(32000..32001), 32000);
        assert_eq!(tree.query(2..=10), 54);
        assert_eq!(tree.query((Bound::Excluded(1), Bound::Included(2))), 2);
        assert!(catch_unwind(|| tree.clone().query(101..101)).is_err());
        assert_eq!(tree.query(..), 5000050000);
        assert!(catch_unwind(|| tree.clone().query(..100002)).is_err());
        assert_eq!(tree.query(10000..20001), 150015000);
    }

    #[test]
    fn query_set() {
        let mut tree = SegmentTree::<Min<i64>>::new(
            std::iter::repeat(0..=999).take(2).flatten().collect(),
            Min::default(),
        );
        assert_eq!(tree.query(100..1000), 100);
        assert_eq!(tree.query(100..1001), 0);
        assert_eq!(tree.query(100..1002), 0);

        tree.set(1000, 50);
        assert_eq!(tree.query(100..1000), 100);
        assert_eq!(tree.query(100..1001), 50);
        assert_eq!(tree.query(100..1002), 1);

        tree.set(100, 200);
        assert_eq!(tree.query(100..1000), 101);
        assert_eq!(tree.query(100..1001), 50);
        assert_eq!(tree.query(100..1002), 1);
        assert!(catch_unwind(move || tree.set(2000, 0)).is_err());
    }

    #[test]
    fn update_get() {
        let mut tree = SegmentTree::<Max<i16>>::new((1..=1000).collect(), Max::default());
        assert_eq!(tree.get(0), 1);
        assert_eq!(tree.get(99), 100);
        assert_eq!(tree.get(999), 1000);
        assert!(catch_unwind(|| tree.clone().get(1000)).is_err());

        tree.update(200..=300, 290);
        assert_eq!(tree.get(199), 200);
        assert_eq!(tree.get(200), 290);
        assert_eq!(tree.get(289), 290);
        assert_eq!(tree.get(290), 291);
        assert_eq!(tree.get(300), 301);

        tree.update(100..=225, 250);
        assert_eq!(tree.get(99), 100);
        assert_eq!(tree.get(100), 250);
        assert_eq!(tree.get(101), 250);
        assert_eq!(tree.get(199), 250);
        assert_eq!(tree.get(200), 290);
        assert_eq!(tree.get(225), 290);
        assert_eq!(tree.get(226), 290);
    }

    #[test]
    fn view() {
        #[track_caller]
        fn verify(nodes: &[(i64, usize)], res: &[i64]) {
            nodes.iter().enumerate().for_each(|(i, &(val, cnt))| {
                assert!(cnt == 1, "count at index {} is {} != 1", i, cnt);
                assert!(
                    val == res[i],
                    "mismatch at index {}: found {} != expected {}",
                    i,
                    val,
                    res[i]
                );
            })
        }

        let mut tree =
            SegmentTree::<SumSet>::new(vec![5, 4, 2, 0, 4, 7, 5, 1, 3], SumSet::default());
        tree.view(.., |x| verify(x, &[5, 4, 2, 0, 4, 7, 5, 1, 3]));
        tree.view(2.., |x| verify(x, &[2, 0, 4, 7, 5, 1, 3]));
        tree.view(..3, |x| verify(x, &[5, 4, 2]));
        tree.update(2..=4, 9); // [5, 4, 9, 9, 9, 7, 5, 1, 3]
        tree.view(3..6, |x| verify(x, &[9, 9, 7]));
        tree.view(1..=4, |x| verify(x, &[4, 9, 9, 9]));
        tree.update(..=3, 3); // [3, 3, 3, 3, 9, 7, 5, 1, 3]
        tree.view(1..5, |x| verify(x, &[3, 3, 3, 9]));
        tree.view(.., |x| verify(x, &[3, 3, 3, 3, 9, 7, 5, 1, 3]));
    }

    #[test]
    fn view_mut() {
        let mut tree = SegmentTree::<Sum<i8>>::new(vec![4, 2, 3, 4, 5, 8, 3, 3, 6], Sum::default());
        assert_eq!(tree.query(..), 38);
        tree.view_mut(1..=3, |x| x.copy_from_slice(&[8, 8, 2])); // [4, 8, 8, 2, 5, 8, 3, 3, 6]
        assert_eq!(tree.query(..4), 22);
        assert_eq!(tree.query(..), 47);
        tree.view_mut(5.., |x| x.copy_from_slice(&[0, 1, -3, 5])); // [4, 8, 8, 2, 5, 0, 1, -3, 5]
        assert_eq!(tree.query(5..=7), -2);
        assert_eq!(tree.query(4..), 8);
    }

    struct PanicAfter(Cell<usize>);
    impl Spec for PanicAfter {
        type Operation = Self;
        type Formatter = formatter::Noop<Self, i64>;
        type Update = update::Noop<Self>;
    }
    impl Operation for PanicAfter {
        type Spec = Self;
        type Node = i64;
        fn op(s: &Self, a: &Self::Node, b: &Self::Node) -> Self::Node {
            s.0.set(s.0.get() - 1);
            if s.0.get() == 0 {
                panic!()
            } else {
                *a + *b
            }
        }
    }

    #[test]
    // In the normal environment, this test simply verifies that the structure correctly propagates
    // panics from merge. Run it under miri to verify no unsafe invariants are broken.
    fn panic_safety() {
        assert!(catch_unwind(|| SegmentTree::<Panic>::new((0..1000).collect(), Panic)).is_err());
        assert!(catch_unwind(|| {
            SegmentTree::new((0..1000).collect(), PanicAfter(Cell::new(600)));
        })
        .is_err());
        assert!(catch_unwind(|| {
            let mut tree = SegmentTree::new((0..1000).collect(), PanicAfter(Cell::new(1000)));
            tree.query(3000..5000);
        })
        .is_err());
        assert!(catch_unwind(|| {
            let mut tree = SegmentTree::new((0..1000).collect(), PanicAfter(Cell::new(1000)));
            tree.set(3000, -1);
        })
        .is_err());
    }

    #[test]
    fn update() {
        let mut tree =
            SegmentTree::<MinAdd<i16>>::new(vec![4, 7, 2, 9, 5, 1, 10], MinAdd::default());
        assert_eq!(tree.query(..=2), 2);
        assert_eq!(tree.query(..), 1);
        tree.update(0..=4, -3); // [1, 4, -1, 6, 2, 1, 10]
        assert_eq!(tree.query(..=2), -1);
        assert_eq!(tree.query(..), -1);
        tree.update(.., 5); // [6, 9, 4, 10, 11, 7, 6, 15]
        tree.update(1..=4, 10); // [6, 19, 14, 20, 21, 7, 6, 15]
        tree.update(3.., 7); // [6, 19, 14, 20, 28, 14, 13, 15]
        assert_eq!(tree.query(..), 6);
        assert_eq!(tree.query(2..), 13);

        let mut tree = SegmentTree::<Sum<u64>>::new(vec![1, 2, 3], Sum::default());
        assert!(catch_unwind(move || tree.update(0..2, ())).is_err());
    }

    #[test]
    fn update_order() {
        let mut tree =
            SegmentTree::<SumSet>::new(vec![3, 6, 1, 2, 8, 2, 5, 6, 7, 1, 9], SumSet::default());
        tree.update(0..=2, 2);
        tree.update(1.., 3);
        tree.update(1..=1, 6);
        tree.update(8..=8, 4);
        tree.update(3..=6, 2);
        tree.update(4..=4, 8);
        assert_eq!(tree.query(..), 38);
        tree.view(.., |slice| {
            assert!(slice
                .iter()
                .enumerate()
                .all(|(i, &(x, y))| y == 1 && x == [2, 6, 3, 2, 8, 2, 2, 3, 4, 3, 3][i]))
        });
    }

    #[test]
    #[rustfmt::skip]
    fn update_random() {
        let mut tree = SegmentTree::<SumSet>::new(vec![
            634841, -510742, 529707, 655749, 829834, 115227, -299160, 665733, 975208, 766017,
            130364, -754518, 441990, 678604, -672334, -210840, 15544, 877650, -670423, 69622,
            32348, 305726, 288055, -654880, -181729, 49034, 505832, -442931, 310382, 538142,
            -108866, 82743, 366455, -525140, -700452, 679335, -702807, -396751, 616005, 219390,
            416743, 248356, -457678, 373234, 628830, -604548, -503174, 490012, -974554, -935229,
            742993, 483648, 733409, 932985, -671932, -475478, 277470, 882172, 957508, -1016456,
            -510335, -540862, 691884, 312152, 1017245, 1002643, -693102, 648803, 506811, -407739,
            -135418, 759482, -412013, -391657, 69993, -1016327, -586194, -696545, 720208, 922856,
            340913, 39251, -908055, 984171, 351761, 652425, 373505, -500203, -215666, -293732,
            -423690, 24407, -89206, -22656, 363123, -695803, 613543, -1006923, 769551, -946347,
        ], SumSet::default());
        for (l, r, v) in [
            (77, 83, 842419), (0, 31, -511270), (1, 76, 893604), (60, 99, -609226), (8, 24, 661225),
            (52, 76, 480225), (13, 17, 415910), (85, 94, 781667), (24, 52, 347859), (51, 65, -456330),
            (47, 52, 207529), (52, 98, -450758), (76, 84, 949696), (21, 46, 1008442), (66, 80, -856815),
            (91, 91, 496301), (33, 71, 1031907), (45, 92, 270553), (37, 91, -861119), (70, 92, -372484),
            (86, 97, 704677), (57, 73, -683973), (55, 60, -34341), (9, 59, 995725), (60, 79, -383111),
            (67, 76, -970391), (1, 24, 509446), (32, 67, -412718), (25, 82, 80447), (71, 79, -1039571),
            (53, 86, -996625), (60, 83, -490622), (73, 80, -661194), (96, 99, 826854), (71, 93, 280204),
            (30, 50, -650065), (2, 53, -957804), (27, 38, 329214), (57, 90, -1001674), (74, 96, 427065),
            (13, 41, -567166), (80, 80, -327789), (57, 60, -149005), (96, 98, -188833), (87, 93, 836211),
            (92, 98, 195122), (44, 51, 694214), (92, 98, -375661), (21, 55, 827967), (71, 89, -297697),
            (88, 95, -741944), (6, 56, -1043759), (10, 67, -1029901), (61, 96, -825455), (43, 64, 178288),
            (37, 95, -145195), (62, 95, 433739), (63, 94, -44349), (45, 79, -647607), (6, 72, -1008710),
            (84, 92, -529445), (52, 77, 1035456), (96, 98, 208176), (40, 44, -538880), (18, 98, 17728),
            (3, 68, -240936), (63, 97, 328525), (61, 99, 329820), (60, 73, 478998), (42, 81, -782870),
            (14, 79, 852554), (76, 94, 728128), (79, 96, 710421), (88, 91, -917666), (33, 88, -134976),
            (43, 77, 679782), (39, 46, -716666), (6, 38, 787655), (35, 67, -753965), (43, 65, -843957),
            (64, 96, -709175), (0, 44, -141201), (66, 84, -194312), (97, 98, 132953), (73, 74, -836898),
            (89, 98, -219357), (23, 40, -670432), (86, 97, -416502), (44, 81, -58865), (85, 89, 893259),
            (86, 93, -899333), (14, 83, 579441), (90, 90, -937619), (63, 63, -677570), (80, 92, -270711),
            (87, 89, -972320), (26, 84, 775919), (30, 92, -264562), (15, 82, 564419), (60, 93, 919429),
        ] {
            tree.update(l..=r, v);
        }
        for (l, r, v) in [
            (89, 99, 3041600), (72, 80, 8274861), (34, 73, 27546900), (42, 55, 7901866), (31, 65, 21884725),
            (31, 49, 10723961), (85, 98, 6389496), (48, 50, 1693257), (91, 98, 872922), (14, 84, 48964021),
            (86, 93, 7355432), (64, 95, 26749866), (36, 61, 15384914), (8, 89, 52713960), (96, 99, -722541),
            (84, 88, 4597145), (84, 99, 7638745), (88, 89, 1838858), (61, 63, 2758287), (12, 30, 9327743),
            (74, 98, 16503215), (31, 89, 43951021), (72, 89, 16549722), (93, 96, -330077), (2, 52, 20332951),
            (80, 95, 12039002), (20, 78, 40045911), (40, 95, 41715962), (89, 93, 4597145), (59, 85, 24469573),
            (47, 95, 37765029), (58, 67, 8484270), (23, 81, 41110941), (62, 69, 7355432), (45, 83, 30532581),
            (81, 96, 10703071), (64, 78, 13791435), (30, 67, 24288002), (38, 74, 26208653), (40, 81, 31515818),
            (44, 95, 39458286), (90, 96, 2428210), (30, 85, 40837724), (94, 99, -1555545), (29, 80, 36804998),
            (94, 99, -1555545), (8, 54, 22308995), (24, 87, 46063096), (83, 85, 2758287), (96, 99, -722541),
            (6, 80, 44156697), (87, 87, 919429), (73, 81, 8274861), (45, 91, 37888013), (40, 93, 42548966),
            (65, 70, 5516574), (22, 71, 32481070), (95, 95, -416502), (3, 82, 45571952), (69, 98, 21100360),
            (36, 57, 12417218), (50, 83, 27710486), (43, 85, 33500277), (87, 92, 5516574), (77, 92, 14710864),
            (11, 43, 16523989), (9, 48, 19063682), (13, 52, 21886162), (78, 83, 5516574), (36, 91, 42967784),
            (87, 94, 6019501), (12, 74, 39487329), (13, 61, 27675953), (3, 53, 21038571), (84, 85, 1838858),
            (83, 99, 8558174), (78, 97, 13044856), (56, 69, 11451966), (19, 53, 19754665), (20, 33, 7901866),
            (51, 80, 24387780), (90, 91, 1838858), (21, 39, 10723961), (82, 92, 10113719), (82, 89, 7355432),
            (69, 72, 3677716), (36, 78, 31015207), (31, 61, 18207009), (0, 33, 9326588), (10, 22, 4529989),
            (43, 83, 31661419), (30, 34, 2822095), (42, 91, 39581270), (87, 95, 5602999), (72, 85, 12872006),
            (22, 57, 20319084), (11, 25, 6364447), (59, 84, 23550144), (36, 55, 11288380), (77, 91, 13791435)
        ] {
            assert_eq!(tree.query(l..=r), v);
        }
        let res = [
            -141201, -141201, -141201, -141201, -141201, -141201, -141201, -141201, -141201,
            -141201, -141201, -141201, -141201, -141201, 579441, 564419, 564419, 564419, 564419,
            564419, 564419, 564419, 564419, 564419, 564419, 564419, 564419, 564419, 564419, 564419,
            564419, 564419, 564419, 564419, 564419, 564419, 564419, 564419, 564419, 564419, 564419,
            564419, 564419, 564419, 564419, 564419, 564419, 564419, 564419, 564419, 564419, 564419,
            564419, 564419, 564419, 564419, 564419, 564419, 564419, 564419, 919429, 919429, 919429,
            919429, 919429, 919429, 919429, 919429, 919429, 919429, 919429, 919429, 919429, 919429,
            919429, 919429, 919429, 919429, 919429, 919429, 919429, 919429, 919429, 919429, 919429,
            919429, 919429, 919429, 919429, 919429, 919429, 919429, 919429, 919429, -416502,
            -416502, -416502, -416502, -219357, 329820,
        ];
        tree.view(.., |slice| {
            assert!(slice
                .iter()
                .enumerate()
                .all(|(i, &(x, y))| y == 1 && x == res[i]))
        });
    }

    #[test]
    #[rustfmt::skip]
    fn ops() {
        let input = [
            -811383, -664442, -692598, 465808, -339388, 243841, 6640, -158466, -898695, -384271,
            757783, 602016, 511957, 817277, 76507, -897899, -933464, 478211, 901330, 287163,
            545322, 728177, -358662, -304354, -58203, -81359, -948508, -307359, 315172, -320492,
            -475226, 460079, -285870, 820563, 690680, 479222, 435314, 469285, 821114, -372519,
            628573, 886703, -413, 1006691, 122063, 1040461, 435889, 858526, 885049, 422680, 866338,
            992408, -118234, 313177, -352030, 76074, 963709, 249825, 223542, 657123, 259393,
            -176923, 1002046, 489048, -732429, 383606, -1013253, -245775, -603322, -802124,
            -843433, 96887, -98078, -602865, -476054, 66442, -21677, -165764, -795304, 725269,
            -914891, -810355, 471259, 462289, -327655, -2091, -950251, -700855, -565296, -765601,
            -942273, -877136, -959888, 516307, 23865, -512598, -389575, -277920, -1040455, 568604,
        ];
        let updates = [
            (75, -867546), (78, -574138), (57, -856593), (28, -656603), (16, 599586), (33, 49649),
            (85, -810420), (12, 853079), (29, 816620), (79, 460656), (4, 565194), (30, -621130),
            (27, -946067), (27, -167183), (74, -833682), (84, 963862), (37, 512655), (13, 234555),
            (81, 989280), (0, 144113), (28, 1008501), (91, 894327), (15, -532851), (27, -606492),
            (92, 1039975), (34, 982312), (87, 196141), (70, 745134), (9, 444836), (11, -890989),
            (56, -324931), (12, 330694), (21, -387927), (59, 801880), (50, -823878), (90, 933229),
            (97, -104863), (81, -999898), (12, -976697), (27, -627228), (95, -245391), (61, 15481),
            (44, -1035837), (96, 985663), (3, -713835), (86, -482890), (86, 187983), (46, -905898),
            (86, 245075), (9, 33481), (83, 599645), (76, -421367), (58, -75038), (86, 541963),
            (42, 869815), (59, -487130), (41, -381746), (23, 445006), (51, 386938), (95, 626732),
            (39, 608919), (61, -680747), (13, 422701), (1, 295247), (81, -417004), (18, -191489),
            (32, -674690), (43, 515014), (55, 812025), (43, 606385), (62, 280342), (75, 1030582),
            (49, 849959), (15, -314178), (32, -604148), (18, 743670), (6, 943075), (17, -795623),
            (66, 367365), (62, -1042252), (59, -160282), (79, 603296), (19, -166453), (55, -445758),
            (12, -629326), (29, 715855), (3, -201271), (82, -469350), (69, -848124), (4, -226977),
            (97, -661518), (20, 311738), (53, -907226), (2, -149343), (55, -508550), (49, 936347),
            (56, -242723), (9, -333763), (98, -511338), (12, -995621),
        ];
        let range_updates = [
            (4, 4, 395107), (62, 63, 244887), (79, 83, 934404), (13, 16, 829725), (41, 50, 381895),
            (83, 83, -458986), (16, 36, -332564), (65, 67, -13588), (10, 85, -91423), (35, 35, -414752),
            (14, 79, -680227), (23, 52, -607234), (58, 77, 545270), (55, 82, -959143), (74, 79, -655206),
            (66, 73, 488564), (87, 93, -624273), (57, 60, 673917), (75, 94, -739082), (39, 98, -727510),
            (73, 84, 594672), (50, 57, 17457), (42, 83, 591834), (84, 99, -867465), (58, 67, 523921),
            (92, 98, 1015531), (20, 82, 39448), (89, 90, -646144), (18, 71, -768232), (49, 63, 333089),
            (14, 33, 728592), (91, 99, 30452), (51, 73, -441388), (76, 88, -291821), (51, 67, 680087),
            (42, 87, -707104), (51, 93, 555978), (67, 96, -304638), (93, 97, 569526), (98, 98, 102381),
            (1, 79, 773421), (59, 72, -743125), (85, 95, -774082), (58, 75, 882148), (22, 73, 22199),
            (63, 87, 900335), (36, 95, -1040943), (72, 86, -191197), (61, 95, 221074), (42, 83, 260771),
            (65, 78, 591924), (39, 41, -71665), (7, 21, -578957), (40, 62, -723998), (63, 91, 176106),
            (81, 87, 96612), (60, 72, 519205), (29, 63, 536897), (74, 96, 295642), (68, 72, 492555),
            (33, 78, 799999), (48, 56, -931012), (9, 92, 781935), (69, 78, 918122), (25, 88, -649927),
            (18, 71, 463101), (31, 61, -852063), (18, 40, -784077), (16, 57, -560297), (50, 99, 938657),
            (79, 98, -668793), (58, 68, 678595), (57, 94, -510823), (39, 68, -1011476), (28, 50, -943970),
            (43, 95, 903569), (61, 69, -921183), (69, 98, -1022464), (30, 47, 509697), (14, 45, -594604),
            (27, 75, 363818), (64, 74, -606306), (25, 63, 380164), (50, 70, 206760), (36, 98, 125245),
            (89, 97, -612443), (50, 87, -884565), (95, 97, 944349), (18, 94, 262291), (81, 98, -266437),
            (80, 89, -172529), (66, 70, 682357), (84, 87, 93284), (33, 83, -204862), (57, 86, 402023),
            (78, 84, 948355), (73, 83, 249400), (80, 95, -116384), (26, 94, 206243), (40, 43, 289138),
        ];
        let queries = [
            (31, 50), (36, 79), (33, 84), (84, 94), (62, 94), (77, 90), (32, 36), (81, 88), (9, 41), (32, 86),
            (52, 62), (12, 14), (66, 95), (93, 99), (53, 86), (46, 47), (39, 40), (25, 27), (59, 72), (3, 50),
            (24, 38), (17, 61), (10, 88), (3, 82), (37, 59), (67, 97), (50, 64), (89, 99), (22, 91), (16, 53),
            (67, 85), (66, 81), (59, 74), (92, 92), (61, 78), (46, 81), (68, 80), (40, 69), (51, 64), (49, 84),
            (62, 70), (65, 76), (62, 96), (86, 91), (78, 89), (6, 26), (19, 20), (68, 92), (63, 71), (40, 59),
            (66, 71), (64, 69), (66, 83), (46, 59), (46, 56), (91, 98), (12, 42), (17, 34), (35, 99), (96, 98),
            (61, 72), (23, 63), (21, 66), (30, 79), (46, 66), (42, 58), (43, 55), (82, 83), (99, 99), (29, 35),
            (69, 95), (51, 95), (5, 18), (9, 59), (85, 90), (85, 86), (30, 38), (6, 31), (87, 95), (16, 85),
            (6, 84), (91, 99), (22, 70), (54, 62), (60, 77), (89, 91), (27, 51), (80, 80), (94, 95), (13, 30),
            (87, 94), (89, 93), (58, 77), (53, 74), (88, 90), (78, 86), (96, 99), (47, 85), (87, 91), (3, 28),
        ];

        let sum_result = [
            10670428, 7244216, 7008910, -5550874, -10692174, -5060353, 1368995, -3231284, 7252857,
            3926842, 1991284, 1746863, -13419985, -1111772, -6744395, 1294415, 256054, -1197050,
            -1627234, 12569383, 3111581, 14150455, 10714264, 11715677, 10012986, -6569377, 3606649,
            -885344, 6707725, 13808069, -2350915, -4451158, -1475214, 1039975, -4020855, -692858,
            -4181410, 4777904, 1596428, -2964090, -777069, -3862695, -3523345, -257451, -3026288,
            -1264778, 832485, -2173560, -1682228, 3717950, -1822453, -3013297, -5769122, 893192,
            1022943, 2069428, 4249474, 2605306, 1782388, -159655, -762779, 6193502, 4084837,
            364613, -1293624, 1366972, 1753719, 1070904, 568604, 1492062, 271206, -395619,
            -1030437, 5578678, -469984, -268457, 2515067, 21829, 2899679, 3124516, 3256306,
            3510155, 3674491, -3678238, -2856390, 1061955, 7199461, -914891, 650597, 197877,
            2272947, 2618237, -3137710, -6467699, -397668, -477037, -147706, -4899084, 692800,
            -1891515,
        ];
        let min_result = [
            -372519, -1013253, -1013253, -959888, -1013253, -950251, -285870, -950251, -948508,
            -1013253, -856593, 76507, -1013253, -1040455, -1013253, 435889, -372519, -948508,
            -1013253, -948508, -948508, -948508, -1013253, -1013253, -856593, -950251, -856593,
            -1040455, -1013253, -948508, -914891, -1013253, -1013253, 1039975, -1013253, -1013253,
            -914891, -1013253, -856593, -1013253, -1013253, -1013253, -1013253, -950251, -999898,
            -976697, 287163, -999898, -1013253, -1035837, -1013253, -1013253, -1013253, -905898,
            -905898, -1040455, -976697, -948508, -1040455, -1040455, -1013253, -1035837, -1035837,
            -1035837, -1013253, -1035837, -1035837, 471259, 568604, -674690, -914891, -1013253,
            -976697, -1035837, -810420, -810420, -621130, -976697, -765601, -1042252, -1042252,
            -1040455, -1042252, -1042252, -1042252, -765601, -1035837, -914891, 23865, -948508,
            -765601, -765601, -1042252, -1042252, -765601, -914891, -1040455, -1042252, -765601,
            -995621,
        ];
        let max_result = [
            1040461, 1040461, 1040461, 516307, 1002046, 725269, 690680, 471259, 901330, 1040461,
            1002046, 853079, 516307, 568604, 1002046, 858526, 628573, -81359, 1002046, 1040461,
            1008501, 1040461, 1040461, 1040461, 1040461, 1039975, 1002046, 1039975, 1040461,
            1040461, 989280, 989280, 1002046, 1039975, 1002046, 1002046, 745134, 1040461, 1002046,
            1002046, 1002046, 745134, 1039975, 933229, 963862, 901330, 545322, 1039975, 745134,
            1040461, 745134, 383606, 745134, 992408, 992408, 1039975, 1008501, 1008501, 1040461,
            985663, 1002046, 1040461, 1040461, 1040461, 1002046, 1040461, 1040461, 599645, 568604,
            982312, 1039975, 1039975, 757783, 1040461, 933229, 541963, 982312, 1008501, 1039975,
            1040461, 1040461, 1039975, 1040461, 259393, 1030582, 933229, 1040461, -914891, 626732,
            1008501, 1039975, 1039975, 1030582, 745134, 933229, 963862, 985663, 1030582, 933229,
            1008501,
        ];
        let min_result_range = [
            -372519, -1013253, -1013253, -959888, -1013253, -950251, -332564, -950251, -948508, -1013253,
            -680227, -680227, -1013253, -1040455, -1013253, -680227, -680227, -948508, -1013253, -948508,
            -948508, -959143, -1013253, -1013253, -959143, -959888, -959143, -1040455, -1013253, -948508,
            -959143, -1013253, -1013253, -959888, -1013253, -1013253, -959143, -1013253, -959143, -1013253,
            -1013253, -1013253, -1013253, -950251, -959143, -948508, -768232, -1040943, -1040943, -1040943,
            -1040943, -1040943, -1040943, -1040943, -1040943, -1040943, -1040943, -948508, -1040943, -1040455,
            -1040943, -1040943, -1040943, -1040943, -1040943, -1040943, -1040943, -1040943, -867465, -852063,
            -1040943, -1040943, -933464, -1040943, -1040943, -1040943, -1040943, -948508, -1040943, -1040943,
            -1040943, -1040943, -1040943, -1040943, -1040943, -1040943, -1040943, -1040943, -1040943, -948508,
            -1040943, -1040943, -1040943, -1040943, -1040943, -1040943, -1040455, -1040943, -1040943, -948508
        ];
        let max_result_range = [
            1040461, 1040461, 1040461, 516307, 1002046, 934404, 820563, 934404, 901330, 1040461,
            1002046, 829725, 934404, 568604, 1002046, 858526, 628573, -81359, 1002046, 1040461,
            821114, 1040461, 1040461, 1040461, 1040461, 1015531, 1002046, 1015531, 1040461, 1040461,
            934404,934404, 1002046, 1015531, 1002046, 1002046, 934404, 1040461, 1002046, 1002046, 1002046,
            773421, 1015531, 555978, 934404, 901330, 773421, 1015531, 900335, 1040461, 900335,
            900335, 934404, 992408, 992408, 1015531, 901330, 901330, 1040461, 1015531, 1002046,
            1040461, 1040461, 1040461, 1002046, 1040461, 1040461, 934404, 568604, 820563, 1015531,
            1015531, 901330, 1040461, 938657, 938657, 821114, 901330, 1015531, 1040461, 1040461,
            1015531, 1040461, 1002046, 1002046, 938657, 1040461, 938657, 1015531, 901330, 1015531,
            1015531, 1002046, 1002046, 938657, 948355, 1015531, 1002046, 938657, 901330
        ];
        let minadd_result = [
            -372519, -1013253, -1013253, -959888, -1013253, -950251, -618434, -950251, -1372495,
            -1118264, -1123680, 134582, -1566954, -1040455, -3181303, -561100, -1751403, -2659956,
            -1917952, -2659956, -2659956, -2659956, -3461389, -3461389, -2478913, -3900603,
            -1733490, -4546747, -4546747, -3388740, -3421941, -3421941, -2846240, -2872235,
            -3713762, -4420866, -3864888, -3260695, -2170918, -4169526, -2528583, -3271708,
            -5180852, -5180852, -5180852, -1864528, -83270, -6110432, -2286895, -2553518, -1434200,
            -1392891, -2654141, -2841214, -2841214, -4971519, -3524685, -1864528, -5417610,
            -1487066, 225325, -2383726, -1601791, -1601791, -2251718, -1788617, -2640680, 217282,
            -268409, -2266321, -4365811, -4365811, -704231, -4800593, -4876634, -3553728, -3458643,
            -2613793, -4995529, -5829470, -5465652, -4253796, -5085488, -1419485, 1121697,
            -5482727, -4960243, -2384654, -2882391, -2565942, -5695867, -5486873, 499423, -2121376,
            -5486873, -5016880, -2380858, -4276527, -5512724, -2359699,
        ];
        let maxadd_result = [
            1040461, 1246933, 1659673, 516307, 1659673, 1659673, 487999, 1405663, 1555579, 1568250,
            475283, 1555579, 1314240, 568604, 846284, -138463, -750311, -1792807, 145517, 1555579,
            -557770, -202884, 1555579, 1555579, 533762, 566198, 573210, -298861, 566198, -626003,
            566198, -801380, -303321, -2872235, 292659, -330338, -1393894, 225640, 225640, 225640,
            914954, -925111, 171829, -4327756, -181387, 2329000, 214337, -352569, 643644, 689655,
            353878, 799333, 721200, -34343, -1521367, -257386, 1750043, 441440, 2136623, -257386,
            2936622, 2936622, 3718557, 3718557, 3068630, 1897662, -1156700, 669021, -268409,
            -359889, 3945873, 5148983, 2531978, 2531978, -3600560, -2696991, -1303859, 2531978,
            -1944042, 3609070, 3972888, 670248, 4353052, 3272371, 4685057, -4740994, -644784,
            -2384654, -1486891, 2531978, -2724681, -2724681, 4062783, 3857921, -4965586, 625162,
            670248, 4259944, -4655281, 2531978,
        ];
        let sumset_result = [
            10670428, 6955718, 12565279, -5550874, -6242223, -1936460, -1662820, -1136326,
            -3309807, -6210422, -7482497, -863073, -1099478, -1111772, -27617356, -1214468,
            -1214468, -1821702, -27933, -25263635, -9108510, -24570100, 4155651, 5549149, 7120188,
            9146731, 591720, 4081499, -43882916, -23510201, -4550328, -4214912, -6581372, 30452,
            1315714, -25455744, 7227714, 81334, 7783692, 1997956, 6960789, -2851316, -12228994,
            -4644492, -3846758, 12485731, 1546842, -13277385, 1989666, 2611992, 3551544, 3220391,
            8998867, -10135972, -7963978, 2301835, -10845272, 481990, 27921089, 967549, 9599988,
            11501652, 35969010, 40458620, -13648467, 7872717, -11076819, -1299854, 30452, -3922079,
            -1982911, 12052233, 4181582, -28654282, -3064938, 1807138, -8495730, -5410392,
            -9202176, -29823681, 4420236, -7241055, 8798442, 1860840, 2254410, -1837329, 3405776,
            -884565, 1206640, 1813302, -1849772, -1238277, 7346150, -4506964, -611495, 7442531,
            139346, 5431244, 1031215, 6184086,
        ];

        let n = updates.len();
        assert_eq!(updates.len(), queries.len());
        assert_eq!(updates.len(), range_updates.len());

        {
            let mut sum_tree = SegmentTree::<Sum<i64>>::new(input.into(), Sum::default());
            let mut min_tree = SegmentTree::<Min<i64>>::new(input.into(), Min::default());
            let mut max_tree = SegmentTree::<Max<i64>>::new(input.into(), Max::default());

            for i in 0..n {
                let (p, v) = updates[i];
                sum_tree.set(p, v);
                min_tree.set(p, v);
                max_tree.set(p, v);

                let (l, r) = queries[i];
                assert_eq!(sum_tree.query(l..=r), sum_result[i]);
                assert_eq!(min_tree.query(l..=r), min_result[i]);
                assert_eq!(max_tree.query(l..=r), max_result[i]);
            }
        }

        {
            let mut min_tree = SegmentTree::<Min<i64>>::new(input.into(), Min::default());
            let mut max_tree = SegmentTree::<Max<i64>>::new(input.into(), Max::default());
            let mut argmin_tree = SegmentTree::<Argmin<i64>>::new(input.into(), Argmin::default());
            let mut argmax_tree = SegmentTree::<Argmax<i64>>::new(input.into(), Argmax::default());
            let mut minadd_tree = SegmentTree::<MinAdd<i64>>::new(input.into(), MinAdd::default());
            let mut maxadd_tree = SegmentTree::<MaxAdd<i64>>::new(input.into(), MaxAdd::default());
            let mut sumset_tree = SegmentTree::<SumSet>::new(input.into(), SumSet::default());

            for i in 0..n {
                let (l, r, v) = range_updates[i];
                min_tree.update(l..=r, v);
                max_tree.update(l..=r, v);
                minadd_tree.update(l..=r, v);
                maxadd_tree.update(l..=r, v);
                sumset_tree.update(l..=r, v);
                argmin_tree.update(l..=r, v);
                argmax_tree.update(l..=r, v);

                let (l, r) = queries[i];
                assert_eq!(min_tree.query(l..=r), min_result_range[i]);
                assert_eq!(min_tree.get(argmin_tree.query(l..=r)), min_result_range[i]);
                assert_eq!(max_tree.query(l..=r), max_result_range[i]);
                assert_eq!(max_tree.get(argmax_tree.query(l..=r)), max_result_range[i]);

                assert_eq!(minadd_tree.query(l..=r), minadd_result[i]);
                assert_eq!(maxadd_tree.query(l..=r), maxadd_result[i]);
                assert_eq!(sumset_tree.query(l..=r), sumset_result[i]);
            }
        }
    }
}
