// NAME graph
// START code
use std::{collections::HashSet, convert::TryInto, fmt::Debug};

/// Representation of a general unweighted graph, which can be either directed or undirected, with
/// optional extra data per vertex.
///
/// Currently supports the ability to add vertices and edges and basic dfs-based algorithms.
///
/// The ability to remove vertices and/or edges in a reasonable time complexity would require the
/// internal use of `HashMap`s instead of `Vec`s which have a higher constant factor.
/// TODO Make benchmarks to assess whether this is a worthwhile feature.

type Edges<I> = Vec<Vec<I>>;
type Component<I> = Vec<I>;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Graph<I, E = ()> {
    directed: bool,
    extra: Vec<E>,
    adj: Edges<I>,
    adj_rev: Edges<I>,
}

/// A data type representing indexes and distances in a graph.
///
/// It can also hold an invalid value, which enables space layout optimizations. `new` must never
/// create an invalid reference, panicking is allowed instead.
pub trait Idx: Copy + Eq {
    fn from(id: usize) -> Self;
    fn to_usize(self) -> usize;
    fn invalid() -> Self;
    fn is_valid(&self) -> bool;
    fn increment(&mut self) -> Self {
        let t = *self;
        *self = Self::from(t.to_usize() + 1);
        t
    }
}

impl Idx for u32 {
    fn from(id: usize) -> Self {
        let id = match id.try_into() {
            Ok(x) => x,
            Err(_) => panic!("vertex id {} exceeds implementation limit", id),
        };
        assert_ne!(
            id,
            u32::MAX,
            "vertex id {} exceeds implementation limit",
            id
        );
        id
    }

    fn to_usize(self) -> usize {
        self as usize
    }

    fn invalid() -> Self {
        u32::MAX
    }

    fn is_valid(&self) -> bool {
        *self != u32::MAX
    }
}

impl<I: Idx, E> Graph<I, E> {
    /// Creates a new empty graph.
    pub fn empty(directed: bool) -> Self {
        Graph {
            directed,
            extra: Vec::new(),
            adj: Vec::new(),
            adj_rev: Vec::new(),
        }
    }

    /// Verifies that `v` points to a valid vertex and returns it. Panics on error.
    #[track_caller]
    fn verify_vertex(&self, v: I) -> usize {
        assert!(v.is_valid(), "vertex is invalid");
        assert!(
            v.to_usize() < self.n(),
            "vertex {} does not exist",
            v.to_usize()
        );
        v.to_usize()
    }

    /// Returns an iterator over all vertices.
    pub fn vertices(
        &self,
    ) -> impl Iterator<Item = I> + DoubleEndedIterator + ExactSizeIterator + '_ {
        (0..self.n()).map(I::from)
    }

    /// Retrieves extra data associated with vertex `v`.
    pub fn get_data(&self, v: I) -> &E {
        let v = self.verify_vertex(v);
        &self.extra[v]
    }

    /// Adds a new vertex to the graph and returns a reference to it.
    pub fn add_vertex(&mut self, data: E) -> I {
        self.extra.push(data);
        self.adj.push(Vec::new());
        self.adj_rev.push(Vec::new());
        I::from(self.adj.len() - 1)
    }

    /// Adds a new edge between vertices `u` and `v` to the graph.
    ///
    /// The graph is stored as an adjacency list so there is no easy way to get a reference to an
    /// edge, hence this function returns nothing.
    #[track_caller]
    pub fn add_edge(&mut self, u: I, v: I) {
        let (uidx, vidx) = (self.verify_vertex(u), self.verify_vertex(v));
        if !self.directed {
            self.adj[vidx].push(u);
            self.adj_rev[uidx].push(v);
        }
        self.adj[uidx].push(v);
        self.adj_rev[vidx].push(u);
    }

    /// Returns an iterator over vertices adjacent to `u`.
    pub fn adjacent(
        &self,
        u: I,
    ) -> impl Iterator<Item = I> + DoubleEndedIterator + ExactSizeIterator + '_ {
        let u = self.verify_vertex(u);
        self.adj[u].iter().copied()
    }

    /// Returns the number of vertices.
    pub fn n(&self) -> usize {
        self.adj.len()
    }

    /// Runs a parametrizable depth-first search traversal of the graph.
    ///
    /// `transpose` determines whether we operate on the transpose of this graph.
    /// `order` determines the order that elements are considered as roots. It
    /// must only return indices for vertices that exist in this graph or the search
    /// will panic.
    /// `visitor` is a [DfsVisitor] which defines callbacks that are triggered on
    /// certain events.
    pub fn generic_dfs(
        &self,
        transpose: bool,
        order: impl IntoIterator<Item = I>,
        visitor: &mut impl DfsVisitor<I>,
    ) {
        #[derive(Debug, Clone, PartialEq, Eq, Copy)]
        enum VertexColor {
            Unvisited,
            Current,
            Finished,
        }

        let edges = if transpose { &self.adj_rev } else { &self.adj };
        let mut vertex_state = vec![VertexColor::Unvisited; self.n()];

        for source in order {
            let source_idx = self.verify_vertex(source);

            if vertex_state[source_idx] == VertexColor::Finished {
                continue;
            }

            let mut stack = Vec::new();
            visitor.new_root(source);
            vertex_state[source.to_usize()] = VertexColor::Current;
            stack.push((source, 0));
            visitor.visit_start(source);

            while let Some(&mut (cur, ref mut visited_child_cnt)) = stack.last_mut() {
                let cur_idx = cur.to_usize();

                if let Some(&child) = edges[cur_idx].get(*visited_child_cnt) {
                    let child_idx = child.to_usize();
                    *visited_child_cnt += 1;

                    match vertex_state[child_idx] {
                        VertexColor::Unvisited => {
                            vertex_state[child_idx] = VertexColor::Current;
                            stack.push((child, 0));
                            visitor.visit_start(child);
                        }
                        VertexColor::Current => (),
                        VertexColor::Finished => (),
                    }
                } else {
                    vertex_state[cur_idx] = VertexColor::Finished;
                    stack.pop();
                    visitor.visit_end(cur);
                }
            }
        }
    }

    /// Performs Kosaraju's algorithm to find the strongly components of this graph.
    /// Returns the SCC graph with each composite vertex storing a list of vertices of the original
    /// graph as extra data.
    /// [https://en.wikipedia.org/wiki/Kosaraju%27s_algorithm]
    ///
    /// The returned SCC graph never contains self-loops.
    pub fn scc(&self) -> Graph<I, Component<I>> {
        struct TopologicalSortVisitor<I> {
            order: Vec<I>,
        }
        impl<I> DfsVisitor<I> for TopologicalSortVisitor<I> {
            fn visit_end(&mut self, v: I) {
                self.order.push(v);
            }
        }

        let mut visitor = TopologicalSortVisitor { order: Vec::new() };
        self.generic_dfs(false, (0..self.n()).map(I::from), &mut visitor);

        struct ComponentVisitor<I> {
            current_root: I,
            component_of: Vec<I>,
            components: Vec<Component<I>>,
        }
        impl<I: Idx> DfsVisitor<I> for ComponentVisitor<I> {
            fn new_root(&mut self, root: I) {
                self.current_root = root;
                self.component_of[root.to_usize()] = I::from(self.components.len());
                self.components.push(vec![root]);
            }
            fn visit_start(&mut self, v: I) {
                if v != self.current_root {
                    let comp = self.component_of[self.current_root.to_usize()];
                    self.component_of[v.to_usize()] = comp;
                    self.components[comp.to_usize()].push(v);
                }
            }
        }

        let TopologicalSortVisitor { order } = visitor;
        let mut visitor = ComponentVisitor {
            current_root: I::invalid(),
            component_of: vec![I::invalid(); self.n()],
            components: Vec::new(),
        };
        self.generic_dfs(true, order.into_iter().rev(), &mut visitor);

        let ComponentVisitor {
            component_of,
            components,
            current_root: _,
        } = visitor;
        let n = components.len();
        let mut adj = vec![HashSet::new(); n];
        let mut adj_rev = vec![HashSet::new(); n];

        for u in 0..self.n() {
            for v in self.adj[u].iter() {
                let (u, v) = (
                    component_of[u].to_usize(),
                    component_of[v.to_usize()].to_usize(),
                );
                if u == v {
                    // Avoid introducing self-loops
                    continue;
                }
                adj[u].insert(v);
                adj_rev[v].insert(u);
            }
        }

        Graph {
            directed: true,
            extra: components,
            adj: adj
                .into_iter()
                .map(|x| x.into_iter().map(I::from).collect())
                .collect(),
            adj_rev: adj_rev
                .into_iter()
                .map(|x| x.into_iter().map(I::from).collect())
                .collect(),
        }
    }
}

impl<I: Idx> Graph<I, ()> {
    /// Creates a graph with `n` vertices and no edges, without any data
    /// attached to the vertices.
    pub fn with_vertices(n: usize, directed: bool) -> Self {
        Graph {
            directed,
            extra: vec![(); n],
            adj: vec![vec![]; n],
            adj_rev: vec![vec![]; n],
        }
    }
}

pub trait DfsVisitor<I> {
    // Suppress warning without having to rename the argument.
    fn new_root(&mut self, root: I) {
        let _ = root;
    }
    fn visit_start(&mut self, v: I) {
        let _ = v;
    }
    fn visit_end(&mut self, v: I) {
        let _ = v;
    }
}
// END code

/* Templates
// START imports
Graph
// END imports
*/

#[derive(Debug, Clone, Copy)]
pub enum StandardGraph {
    /// Vertices connected in a straight line
    Line,
    /// Single central vertex which all others connect to
    Star,
    /// No edges
    Empty,
    /// Like line but each vertex has an extra one attached
    ///
    /// `
    /// 0--2--4--6--
    /// |  |  |  |
    /// 1  3  5  7
    /// `
    Ladder,
    /// All vertices form a long cycle
    Cycle,
    /// Every edge exists
    Complete,
    /// Vertices form a binary tree
    BinaryTree,
}

impl StandardGraph {
    /// Instantiates this standard graph with size `n`.
    ///
    /// `directed` determines whether the graph should be directed.
    /// `reverse` determines whether the edge directions should be reversed (in directed graphs).
    ///
    /// The sum of the number of vertices and edges will be on the order of `n`, but might not be exactly equal.
    // TODO More options in struct, e.g. random edge directions or missed out/added edges.
    pub fn init(self, n: usize, directed: bool, reverse: bool) -> Graph<u32> {
        let mut graph = Graph::with_vertices(self.vertex_count(n), directed);
        for (u, v) in self.edges(n) {
            if reverse {
                graph.add_edge(v, u);
            } else {
                graph.add_edge(u, v);
            }
        }
        graph
    }

    /// Returns an iterator over all available standard graph types.
    pub fn iter() -> impl Iterator<Item = Self> {
        use StandardGraph::*;
        IntoIterator::into_iter([Line, Star, Empty, Ladder, Cycle, Complete, BinaryTree])
    }

    fn vertex_count(self, n: usize) -> usize {
        use StandardGraph::*;
        match self {
            Line => n,
            Star => n,
            Empty => n,
            Ladder => n + n % 2,
            Cycle => n,
            Complete => {
                assert!(n < 1 << 52, "Graph size exceeds supported limit");
                (n as f64).sqrt() as usize
            }
            BinaryTree => n.next_power_of_two() - 1,
        }
    }

    fn edges(self, n: usize) -> Vec<(u32, u32)> {
        let n: u32 = self
            .vertex_count(n)
            .try_into()
            .expect("Graph size exceeds supported limit");
        let mut edges = Vec::new();

        use StandardGraph::*;
        match self {
            Line => (0..n - 1).for_each(|i| edges.push((i, i + 1))),
            Star => (1..n).for_each(|i| edges.push((0, i))),
            Empty => (),
            Ladder => (0..n / 2).for_each(|i| {
                edges.push((2 * i, 2 * i + 1));
                if 2 * i + 2 < n {
                    edges.push((2 * i, 2 * i + 2));
                }
            }),
            Cycle => (0..n).for_each(|i| edges.push((i, (i + 1) % n))),
            Complete => (0..n).for_each(|i| (i + 1..n).for_each(|j| edges.push((i, j)))),
            BinaryTree => (0..n / 2).for_each(|i| {
                edges.push((i, 2 * i));
                edges.push((i, 2 * i + 1));
            }),
        }

        edges
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::panic::catch_unwind;

    #[test]
    fn add_edge() {
        let mut graph = Graph::with_vertices(3, false);
        graph.add_edge(0, 1);
        graph.add_edge(1, 2);
        graph.add_edge(2, 0);
        assert!(catch_unwind(move || graph.add_edge(0, 3)).is_err());

        let mut graph: Graph<u32, ()> = Graph::empty(true);
        assert!(catch_unwind(move || graph.add_edge(0, 0)).is_err());
    }

    #[test]
    fn adjacent() {
        let mut graph = Graph::empty(true);
        assert_eq!(graph.add_vertex(()), 0);
        assert_eq!(graph.add_vertex(()), 1);
        assert_eq!(graph.add_vertex(()), 2);
        assert_eq!(graph.add_vertex(()), 3);
        graph.add_edge(0, 1);
        graph.add_edge(1, 0);
        graph.add_edge(0, 2);
        graph.add_edge(2, 0);
        graph.add_edge(0, 3);
        graph.add_edge(3, 1);
        graph.add_edge(2, 3);
        graph.add_edge(3, 2);
        assert_eq!(graph.adjacent(0).collect::<Vec<u32>>(), vec![1, 2, 3]);
        assert_eq!(graph.adjacent(1).collect::<Vec<u32>>(), vec![0]);
        assert_eq!(graph.adjacent(2).collect::<Vec<u32>>(), vec![0, 3]);
        assert_eq!(graph.adjacent(3).collect::<Vec<u32>>(), vec![1, 2]);
    }

    fn create_graph(n: usize, directed: bool, edges: &[(u32, u32)]) -> Graph<u32> {
        let mut graph = Graph::with_vertices(n, directed);
        for &(u, v) in edges {
            graph.add_edge(u, v);
        }
        graph
    }

    fn create_graph_with_data<E>(
        data: Vec<E>,
        directed: bool,
        edges: &[(u32, u32)],
    ) -> Graph<u32, E> {
        let mut graph = Graph::empty(directed);
        for d in data {
            graph.add_vertex(d);
        }
        for &(u, v) in edges {
            graph.add_edge(u, v);
        }
        graph
    }

    #[track_caller]
    fn assert_graphs_eq<I, E>(g1: Graph<I, E>, g2: Graph<I, E>)
    where
        I: Idx + Debug + Ord,
        E: Debug + PartialOrd,
    {
        // Convert graphs to normalized form before comparing.
        let normalize = |g: Graph<I, E>| Graph::<I, E> {
            directed: g.directed,
            extra: g.extra,
            adj: g
                .adj
                .into_iter()
                .map(|mut x| {
                    x.sort();
                    x
                })
                .collect(),
            adj_rev: g
                .adj_rev
                .into_iter()
                .map(|mut x| {
                    x.sort();
                    x
                })
                .collect(),
        };
        assert_eq!(normalize(g1), normalize(g2));
    }

    const STANDARD_GRAPH_COUNT: i32 = 6;
    fn create_standard_graph(i: i32) -> Graph<u32> {
        match i {
            //   4   2
            //  /   /       5---6
            // 0---1---3
            1 => create_graph(7, false, &[(0, 1), (1, 2), (1, 3), (0, 4), (6, 5)]),

            //   0---1
            //  /     \
            // 5       2
            //  \     /
            //   4---3
            2 => create_graph(6, false, &[(0, 1), (3, 2), (2, 1), (4, 5), (3, 4), (0, 5)]),

            //         8
            //          \
            //          v
            //    ->1-->2-->5
            //   /      ^
            //  /       /
            // 0-->4-->3--->6
            //              ^
            //             /
            //            7
            3 => create_graph(
                9,
                true,
                &[
                    (8, 2),
                    (1, 2),
                    (0, 1),
                    (4, 3),
                    (2, 5),
                    (0, 4),
                    (3, 6),
                    (7, 6),
                    (3, 2),
                ],
            ),

            // 0-->1   2
            // ^   v
            // \---/
            4 => create_graph(3, true, &[(0, 1), (1, 0)]),

            // 3-->1-->5-->2
            //     |      /
            //     v     v
            //     0---->4
            //     ^     v
            //     \-----/
            5 => create_graph(
                6,
                true,
                &[(3, 1), (1, 5), (5, 2), (1, 0), (2, 4), (0, 4), (4, 0)],
            ),

            // 0-->2<-->1
            6 => create_graph(3, true, &[(0, 2), (2, 1), (1, 2)]),

            _ => panic!("no standard graph with index {}", i),
        }
    }

    #[derive(Debug, Clone, PartialEq, Eq)]
    enum CallbackResult<I> {
        Root(I),
        VisitStart(I),
        VisitEnd(I),
    }

    fn full_dfs<I: Idx, E>(graph: Graph<I, E>) -> Vec<CallbackResult<I>>
    {
        use CallbackResult::*;
        struct EventCollector<I>(Vec<CallbackResult<I>>);
        impl<I> DfsVisitor<I> for EventCollector<I> {
            fn new_root(&mut self, root: I) {
                self.0.push(Root(root));
            }
            fn visit_start(&mut self, v: I) {
                self.0.push(VisitStart(v));
            }
            fn visit_end(&mut self, v: I) {
                self.0.push(VisitEnd(v));
            }
        }

        let mut visitor = EventCollector(Vec::new());
        graph.generic_dfs(false, (0..graph.n()).map(I::from), &mut visitor);
        visitor.0
    }

    #[test]
    #[rustfmt::skip]
    fn dfs() {
        let r = CallbackResult::Root;
        let s = CallbackResult::VisitStart;
        let e = CallbackResult::VisitEnd;

        assert_eq!(
            full_dfs(create_standard_graph(1)),
            vec![
                r(0), s(0), s(1), s(2), e(2), s(3), e(3), e(1), s(4), e(4), e(0),
                r(5), s(5), s(6), e(6), e(5)
            ],
        );

        assert_eq!(
            full_dfs(create_standard_graph(2)),
            vec![
                r(0), s(0), s(1), s(2), s(3), s(4), s(5), e(5), e(4), e(3), e(2),
                e(1), e(0)
            ],
        );

        assert_eq!(
            full_dfs(create_standard_graph(3)),
            vec![
                r(0), s(0), s(1), s(2), s(5), e(5), e(2), e(1), s(4), s(3), s(6),
                e(6), e(3), e(4), e(0), r(7), s(7), e(7), r(8), s(8), e(8)
            ],
        );

        assert_eq!(
            full_dfs(create_standard_graph(4)),
            vec![
                r(0), s(0), s(1), e(1), e(0), r(2), s(2), e(2),
            ],
        );

        assert_eq!(
            full_dfs(create_standard_graph(5)),
            vec![
                r(0), s(0), s(4), e(4), e(0), r(1), s(1), s(5), s(2), e(2), e(5),
                e(1), r(3), s(3), e(3),
            ],
        );

        assert_eq!(
            full_dfs(create_standard_graph(6)),
            vec![
                r(0), s(0), s(2), s(1), e(1), e(2), e(0)
            ],
        );

        assert_eq!(STANDARD_GRAPH_COUNT, 6);
    }

    #[test]
    fn scc() {
        assert_graphs_eq(
            create_standard_graph(1).scc(),
            create_graph_with_data(vec![vec![5, 6], vec![0, 1, 2, 3, 4]], true, &[]),
        );

        assert_graphs_eq(
            create_standard_graph(2).scc(),
            create_graph_with_data(vec![vec![0, 1, 2, 3, 4, 5]], true, &[]),
        );

        assert_graphs_eq(
            create_standard_graph(3).scc(),
            create_graph_with_data(
                vec![
                    vec![8],
                    vec![7],
                    vec![0],
                    vec![4],
                    vec![3],
                    vec![6],
                    vec![1],
                    vec![2],
                    vec![5],
                ],
                true,
                &[
                    (2, 6),
                    (6, 7),
                    (7, 8),
                    (0, 7),
                    (2, 3),
                    (3, 4),
                    (4, 7),
                    (4, 5),
                    (1, 5),
                ],
            ),
        );

        assert_graphs_eq(
            create_standard_graph(4).scc(),
            create_graph_with_data(vec![vec![2], vec![0, 1]], true, &[]),
        );

        assert_graphs_eq(
            create_standard_graph(5).scc(),
            create_graph_with_data(
                vec![vec![3], vec![1], vec![5], vec![2], vec![0, 4]],
                true,
                &[(0, 1), (1, 2), (2, 3), (1, 4), (3, 4)],
            ),
        );

        assert_graphs_eq(
            create_standard_graph(6).scc(),
            create_graph_with_data(vec![vec![0], vec![2, 1]], true, &[(0, 1)]),
        );

        assert_eq!(STANDARD_GRAPH_COUNT, 6);
    }

    #[test]
    #[should_panic(expected = "no standard graph")]
    fn all_standard_graphs_covered() {
        // Whenever a new standard graph is added, this test will automatically
        // fail. This serves as a reminder to update STANDARD_GRAPH_COUNT, which
        // in turn reminds us to update all tests which test properties of
        // standard graphs.
        create_standard_graph(STANDARD_GRAPH_COUNT + 1);
    }

    // TODO Test for correct behavior with self-loops
}
