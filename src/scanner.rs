// NAME scanner
// START code
use std::fmt::Debug;
use std::io::{stdin, BufReader, Bytes, Error as IoError, Read, Stdin};
use std::str::{self, FromStr, Utf8Error};

#[derive(Debug)]
pub struct Scanner<R: Read> {
    bytes: Bytes<BufReader<R>>,
    buf: Vec<u8>,
}

/// A description of how to read `Target` using a `Scanner`.
///
/// The advantage of having a custom trait rather than just using `FromStr` is that we can have
/// multiple different input specifications for the same type.
pub trait Instructions {
    type Target;
    type Error;
    fn read_from<R: Read>(scanner: &mut Scanner<R>) -> Result<Self::Target, Self::Error>;
}

impl<T: FromStr> Instructions for T {
    type Target = T;
    type Error = ScanError<T>;
    fn read_from<R: Read>(scanner: &mut Scanner<R>) -> Result<Self::Target, Self::Error> {
        scanner.read_token()
    }
}

#[derive(Debug)]
pub enum ScanError<T: FromStr> {
    Io(IoError),
    Parse(T::Err, String),
    NonUtf8(Utf8Error, Vec<u8>),
}

impl Scanner<Stdin> {
    pub fn stdin() -> Self {
        Self::new(stdin())
    }
}

impl<R: Read> Scanner<R> {
    pub fn new(reader: R) -> Self {
        Self {
            bytes: BufReader::new(reader).bytes(),
            buf: Vec::new(),
        }
    }

    pub fn read_token<T: FromStr>(&mut self) -> Result<T, ScanError<T>> {
        debug_assert!(self.buf.is_empty());

        for b in &mut self.bytes {
            let b = b.map_err(ScanError::Io)?;
            if b.is_ascii_whitespace() {
                // Skip leading whitespace
                if self.buf.is_empty() {
                    continue;
                } else {
                    break;
                }
            } else {
                self.buf.push(b);
            }
        }

        // Making sure self.buf is cleared in every codepath
        match str::from_utf8(&self.buf) {
            Err(err) => Err(ScanError::NonUtf8(err, std::mem::take(&mut self.buf))),
            Ok(s) => {
                let ret = s.parse().map_err(|err| ScanError::Parse(err, s.to_owned()));
                self.buf.clear();
                ret
            }
        }
    }

    pub fn read<T: Instructions>(&mut self) -> Result<T::Target, T::Error> {
        T::read_from(self)
    }
}

#[macro_use]
pub mod macros {
    use super::*;
    use std::fmt::{self, Debug};
    use std::io::Read;
    use std::marker::PhantomData;
    use std::num::NonZeroUsize;
    use std::str::FromStr;

    /// Helper for scanning usize and subtracting 1.
    ///
    /// Can be used for easily converting 1-based indexing to 0-based indexing.
    pub struct UsizeM1(());
    impl Instructions for UsizeM1 {
        type Target = usize;
        type Error = ScanError<NonZeroUsize>;
        fn read_from<R: Read>(scanner: &mut Scanner<R>) -> Result<Self::Target, Self::Error> {
            Ok(scanner.read_token::<NonZeroUsize>()?.get() - 1)
        }
    }

    /// Helper for scanning an array prefixed by its length.
    pub struct LenArray<T>(PhantomData<T>);

    impl<T: FromStr> Instructions for LenArray<T> {
        type Target = Vec<T>;
        type Error = LenArrayScanError<T>;
        fn read_from<R: Read>(scanner: &mut Scanner<R>) -> Result<Self::Target, Self::Error> {
            let len = scanner
                .read_token::<usize>()
                .map_err(LenArrayScanError::Len)?;
            let mut arr = Vec::with_capacity(len);
            for i in 0..len {
                let elem = scanner
                    .read_token::<T>()
                    .map_err(|e| LenArrayScanError::Data(i, e))?;
                arr.push(elem);
            }
            Ok(arr)
        }
    }

    pub enum LenArrayScanError<T: FromStr> {
        Len(ScanError<usize>),
        Data(usize, ScanError<T>),
    }

    impl<T> Debug for LenArrayScanError<T>
    where
        T: Debug + FromStr,
        T::Err: Debug,
    {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            use self::LenArrayScanError::*;
            match self {
                Len(e) => f.debug_tuple("Len").field(e).finish(),
                Data(i, e) => f.debug_tuple("Data").field(i).field(e).finish(),
            }
        }
    }

    #[macro_export]
    macro_rules! scan {
    (@scan 1 $scan:expr, $type:ty) => {
        $scan
            .read::<$type>()
            .expect(concat!("failed to read token of type ", stringify!($type)))
    };
    (@scan 2 $scan:expr, $type:ty, $n:expr) => {
        (0..$n)
            .map(|_| scan!(@scan 1 $scan, $type))
            .collect::<Vec<_>>()
    };

    (@accum ($scan:expr) () -> ($($body:tt)*)) => {
        ($($body)*)
    };
    (@accum ($scan:expr) ($type:ty) -> ($($body:tt)*)) => {
        scan!(@accum ($scan) () -> ($($body)* scan!(@scan 1 $scan, $type)))
    };
    (@accum ($scan:expr) ($type:ty, $($next:tt)*) -> ($($body:tt)*)) => {
        scan!(@accum ($scan) ($($next)*) -> ($($body)* scan!(@scan 1 $scan, $type),))
    };
    (@accum ($scan:expr) ($n:tt * $type:ty) -> ($($body:tt)*)) => {
        scan!(@accum ($scan) () -> ($($body)* scan!(@scan 2 $scan, $type, $n)))
    };
    (@accum ($scan:expr) ($n:tt * $type:ty, $($next:tt)*) -> ($($body:tt)*)) => {
        scan!(@accum ($scan) ($($next)*) -> ($($body)* scan!(@scan 2 $scan, $type, $n),))
    };

    ($scan:expr, $($arg:tt)+) => {
        scan!(@accum ($scan) ($($arg)+) -> ())
    };
}
}
// END code

/* Templates
// START imports
{Scanner, macros::{UsizeM1, LenArray}}
// END imports
// START include_main
let mut sc = Scanner::stdin();
// END include_main
*/

#[cfg(test)]
mod tests {
    use super::{macros::*, *};
    use std::io::ErrorKind;
    use std::panic::catch_unwind;

    #[test]
    fn scanner() {
        let input = b"\n1 2\t\t3\n \x88 -3 asdf false";
        let mut scanner = Scanner::new(&input[..]);
        assert!(matches!(scanner.read::<i32>(), Ok(1)));
        assert!(matches!(scanner.read::<i32>(), Ok(2)));
        assert!(matches!(scanner.read::<i32>(), Ok(3)));
        assert!(
            matches!(scanner.read::<i32>(), Err(ScanError::NonUtf8(_, part)) if part == [0x88])
        );
        assert!(matches!(scanner.read::<u32>(), Err(ScanError::Parse(_, part)) if part == "-3"));
        assert!(matches!(scanner.read::<String>(), Ok(out) if out == "asdf"));
        assert!(matches!(scanner.read::<bool>(), Ok(false)));
    }

    #[test]
    fn scanner_io_error() {
        #[derive(Debug)]
        struct BadReader {
            buf: Vec<u8>,
        }
        impl BadReader {
            fn new(buf: &'static [u8]) -> BadReader {
                BadReader { buf: buf.to_vec() }
            }
        }
        impl Read for BadReader {
            fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
                if self.buf.len() == 0 {
                    Err(IoError::new(ErrorKind::PermissionDenied, "Read failed"))
                } else {
                    let to_read = usize::min(self.buf.len(), buf.len());
                    (&mut buf[..to_read]).copy_from_slice(&self.buf[..to_read]);
                    self.buf.splice(..to_read, std::iter::empty());
                    Ok(to_read)
                }
            }
        }

        let input = BadReader::new(b"5\tabc ");
        let mut scanner = Scanner::new(input);
        assert!(matches!(scanner.read::<u32>(), Ok(5)));
        assert!(matches!(scanner.read::<String>(), Ok(x) if x == "abc"));
        assert!(matches!(scanner.read::<String>(), Err(ScanError::Io(_))));
    }

    #[test]
    fn scan_macro() {
        let input = b"4 5\n0 1 2 3\n100 101 102 103 104\ntrue false true oops true";
        let mut scanner = Scanner::new(&input[..]);
        let (x, y) = scan!(scanner, i32, i32);
        assert!(x == 4 && y == 5);
        assert_eq!(scan!(scanner, x * i32), [0, 1, 2, 3]);
        assert_eq!(scan!(scanner, y * i32), [100, 101, 102, 103, 104]);
        assert!(catch_unwind(move || scan!(scanner, bool, bool, bool, bool, bool)).is_err());

        let mut sc = Scanner::new(&b"6\nfalse 8 5 2 1 2 3 23"[..]);
        let n = scan!(sc, usize);
        assert_eq!(n, 6);
        let a = scan!(sc, bool, n * i64, String);
        assert_eq!(a, (false, vec![8, 5, 2, 1, 2, 3], String::from("23")));

        let mut sc = Scanner::new(&b"5\nc d e b g\n1 3 4\n"[..]);
        assert_eq!(
            scan!(sc, LenArray<String>),
            vec![
                "c".to_string(),
                "d".to_string(),
                "e".to_string(),
                "b".to_string(),
                "g".to_string()
            ]
        );
        assert_eq!(scan!(sc, 3 * UsizeM1), [0, 2, 3]);
    }

    #[test]
    // This test should always succeed, but will discover memory leaks in unsafe scan! code
    // when run under miri.
    // n.b. This code is not strictly needed anymore since scan! does not use any unsafe code.
    fn macro_memory_leak() {
        #[derive(Debug)]
        struct SpecificString(String);
        impl FromStr for SpecificString {
            type Err = ();
            fn from_str(s: &str) -> Result<SpecificString, ()> {
                if s.len() == 4 {
                    Ok(SpecificString(s.to_owned()))
                } else {
                    Err(())
                }
            }
        }

        let input = b"good good bad good terrible";
        let mut scanner = Scanner::new(&input[..]);
        assert!(catch_unwind(move || scan!(scanner, 4 * SpecificString)).is_err());
    }
}
