// NAME avl_tree
// START code
use std::{
    borrow::Borrow,
    cmp::{self, Ordering},
    mem,
};

/// An AVL tree storing an ordered multiset of elements of type `T`. If multiple equal elements
/// are in the set, only one representative is stored, with a count representing the rest.
///
/// Time complexity of all operations, assuming `T::cmp` is `O(1)`, is `O(log n)`.
#[derive(Debug, Clone)]
pub struct AvlTree<T> {
    root: PotentialNode<T>,
}

#[derive(Debug, Clone)]
/// A single node of an AVL tree.
struct Node<T> {
    value: T,
    count: usize,

    height: usize,
    size: usize,

    left: PotentialNode<T>,
    right: PotentialNode<T>,
}

impl<T> AvlTree<T>
where
    T: Ord,
{
    /// Creates an empty AVL tree.
    pub fn new() -> Self {
        AvlTree {
            root: PotentialNode::empty(),
        }
    }

    /// Inserts `val` into the tree.
    pub fn insert(&mut self, val: T) {
        self.root.insert(val)
    }

    /// Removes `val` from the tree if it exists.
    pub fn remove<K>(&mut self, val: &K)
    where
        K: Ord + ?Sized,
        T: Borrow<K>,
    {
        self.root.remove(val)
    }

    /// Traverses the tree to an element equal to `val` and returns a reference to it.
    pub fn find<K>(&self, val: &K) -> Option<&T>
    where
        K: Ord + ?Sized,
        T: Borrow<K>,
    {
        self.root.find(val).map(|n| &n.value)
    }

    /// Returns the number of elements equal to `val`.
    pub fn count<K>(&self, val: &K) -> usize
    where
        K: Ord + ?Sized,
        T: Borrow<K>,
    {
        self.root.find(val).map(|n| n.count).unwrap_or(0)
    }

    /// Returns the number of elements strictly less than `val`.
    pub fn count_lt<K>(&self, val: &K) -> usize
    where
        K: Ord + ?Sized,
        T: Borrow<K>,
    {
        self.root.count_less(val, false)
    }

    /// Returns the number of elements less than or equal to `val`.
    pub fn count_le<K>(&self, val: &K) -> usize
    where
        K: Ord + ?Sized,
        T: Borrow<K>,
    {
        self.root.count_less(val, true)
    }

    /// Returns the number of elements strictly greater than `val`.
    pub fn count_gt<K>(&self, val: &K) -> usize
    where
        K: Ord + ?Sized,
        T: Borrow<K>,
    {
        self.root.size() - self.root.count_less(val, true)
    }

    /// Returns the number of elements greater than or equal to `val`.
    pub fn count_ge<K>(&self, val: &K) -> usize
    where
        K: Ord + ?Sized,
        T: Borrow<K>,
    {
        self.root.size() - self.root.count_less(val, false)
    }

    /// Returns the index `val` would have in a sorted list of all elements in this tree. This is
    /// equivalent to the number of elements less than `val`.
    pub fn rank<K>(&self, val: &K) -> usize
    where
        K: Ord + ?Sized,
        T: Borrow<K>,
    {
        self.root.count_less(val, false)
    }

    /// Returns a reference to the `k`th smallest element in the tree if the tree has enough
    /// elements.
    pub fn select(&self, k: usize) -> Option<&T> {
        self.root.select(k).map(|n| &n.value)
    }

    /// Returns the number of elements in the tree.
    pub fn len(&self) -> usize {
        self.root.size()
    }

    /// Returns `true` if the tree contains no elements.
    pub fn is_empty(&self) -> bool {
        self.root.size() == 0
    }
}

impl<T> Default for AvlTree<T>
where
    T: Ord,
{
    fn default() -> Self {
        Self::new()
    }
}

/// A potential node (an existing node in the tree or not) which enables a clean implementation of
/// tree functions.
#[derive(Debug, Clone)]
struct PotentialNode<T>(Option<Box<Node<T>>>);

impl<T> PotentialNode<T> {
    /// Creates an empty node.
    fn empty() -> Self {
        PotentialNode(None)
    }

    /// Creates a new node.
    fn new(x: Box<Node<T>>) -> Self {
        PotentialNode(Some(x))
    }

    /// Returns `true` if this node actually exists.
    fn exists(&self) -> bool {
        self.0.is_some()
    }

    /// Returns the size of this node.
    fn size(&self) -> usize {
        self.0.as_ref().map(|x| x.size).unwrap_or(0)
    }

    /// Returns the height of this node.
    fn height(&self) -> usize {
        self.0.as_ref().map(|x| x.height).unwrap_or(0)
    }

    /// Calls `.take()` on the underyling `Option`.
    fn take(&mut self) -> Self {
        PotentialNode(self.0.take())
    }
    /// Calls `.unwrap()` on the underyling `Option`.
    fn unwrap(self) -> Box<Node<T>> {
        self.0.unwrap()
    }
}

impl<T> PotentialNode<T>
where
    T: Ord,
{
    /// Inserts `val` into the tree.
    fn insert(&mut self, val: T) {
        if let Some(node) = &mut self.0 {
            match val.cmp(&node.value) {
                Ordering::Less => node.left.insert(val),
                Ordering::Equal => node.count += 1,
                Ordering::Greater => node.right.insert(val),
            }
            node.restore_invariant();
        } else {
            self.0 = Some(Box::new(Node::leaf(val)));
        }
    }

    /// Removes 'val' from the tree, if it exists.
    fn remove<K>(&mut self, val: &K)
    where
        K: Ord + ?Sized,
        T: Borrow<K>,
    {
        if let Some(node) = &mut self.0 {
            match val.cmp(node.value.borrow()) {
                Ordering::Less => node.left.remove(val),
                Ordering::Greater => node.right.remove(val),
                Ordering::Equal => {
                    if node.count == 1 {
                        match (node.left.take(), node.right.take()) {
                            (mut x @ PotentialNode(Some(_)), y @ PotentialNode(Some(_))) => {
                                *node = x.take_largest().unwrap();
                                node.left = x;
                                node.right = y;
                                node.restore_invariant();
                            }

                            (x @ PotentialNode(Some(_)), PotentialNode(None))
                            | (PotentialNode(None), x @ PotentialNode(Some(_))) => {
                                *self = x;
                            }

                            (PotentialNode(None), PotentialNode(None)) => {
                                *self = PotentialNode::empty();
                            }
                        }
                        return;
                    } else {
                        node.count -= 1;
                    }
                }
            }
            node.restore_invariant()
        }
    }

    /// Takes and returns the largest node in this subtree, replacing it with its left child.
    fn take_largest(&mut self) -> PotentialNode<T> {
        if let Some(node) = &mut self.0 {
            if node.right.exists() {
                node.right.take_largest()
            } else {
                let tmp = node.left.take();
                mem::replace(self, tmp)
            }
        } else {
            PotentialNode::empty()
        }
    }

    /// Attempts to find a key equal to `val` in the tree.
    fn find<K>(&self, val: &K) -> Option<&Node<T>>
    where
        K: Ord + ?Sized,
        T: Borrow<K>,
    {
        if let Some(node) = &self.0 {
            match val.cmp(node.value.borrow()) {
                Ordering::Less => node.left.find(val),
                Ordering::Greater => node.right.find(val),
                Ordering::Equal => Some(node),
            }
        } else {
            None
        }
    }

    /// Counts the number of elements less than `val`, optionally including equal elements.
    fn count_less<K>(&self, val: &K, include_equal: bool) -> usize
    where
        K: Ord + ?Sized,
        T: Borrow<K>,
    {
        if let Some(node) = &self.0 {
            match val.cmp(node.value.borrow()) {
                Ordering::Less => node.left.count_less(val, include_equal),
                Ordering::Equal => node.left.size() + if include_equal { node.count } else { 0 },
                Ordering::Greater => {
                    node.left.size() + node.count + node.right.count_less(val, include_equal)
                }
            }
        } else {
            0
        }
    }

    /// Returns the `k`th smallest element in the tree, if it exists.
    fn select(&self, k: usize) -> Option<&Node<T>> {
        if let Some(node) = &self.0 {
            let left_size = node.left.size();
            if k < left_size {
                node.left.select(k)
            } else if k < left_size + node.count {
                Some(node)
            } else {
                node.right.select(k - left_size - node.count)
            }
        } else {
            None
        }
    }
}

impl<T> Node<T> {
    /// Returns a new leaf node containing `val`.
    fn leaf(val: T) -> Self {
        Node {
            value: val,
            count: 1,
            height: 1,
            size: 1,
            left: PotentialNode::empty(),
            right: PotentialNode::empty(),
        }
    }

    /// Returns the difference between the height of the right and left child. A positive value
    /// shows that the height of the right child is greater.
    fn balance(&self) -> isize {
        self.right.height() as isize - self.left.height() as isize
    }

    /// Performs a left rotation while maintaining this node as the root.
    /// The right child must be non-null.
    fn rotate_left(&mut self) {
        let alpha = self.left.take();
        let mut y = self.right.take().unwrap();
        let beta = y.left.take();
        let gamma = y.right.take();

        mem::swap(self, &mut y);
        y.left = alpha;
        y.right = beta;
        y.recalculate_data();
        self.left = PotentialNode::new(y);
        self.right = gamma;
        self.recalculate_data();
    }

    /// Performs a right rotation while maintaining this node as the root.
    /// The left child must be non-null.
    fn rotate_right(&mut self) {
        let gamma = self.right.take();
        let mut y = self.left.take().unwrap();
        let beta = y.right.take();
        let alpha = y.left.take();

        mem::swap(self, &mut y);
        y.right = gamma;
        y.left = beta;
        y.recalculate_data();
        self.right = PotentialNode::new(y);
        self.left = alpha;
        self.recalculate_data();
    }

    /// Recalculates the data in the node based on the values of its children.
    fn recalculate_data(&mut self) {
        self.height = cmp::max(self.left.height(), self.right.height()) + 1;
        self.size = self.left.size() + self.right.size() + self.count;
    }

    /// Restores the AVL tree invariant for this node assuming both of its children already satisfy it.
    fn restore_invariant(&mut self) {
        self.recalculate_data();
        let balance = self.balance();

        // We bypass the implementation of PotentialNode here but this avoids a lot of boilerplate
        // that would be needed for as_mut() otherwise so just leave it.
        if balance == 2 {
            let right = self.right.0.as_mut().unwrap();
            let right_balance = right.balance();

            if right_balance == -1 {
                right.rotate_right();
            }

            self.rotate_left();
        } else if balance == -2 {
            let left = self.left.0.as_mut().unwrap();
            let left_balance = left.balance();

            if left_balance == 1 {
                left.rotate_left();
            }

            self.rotate_right();
        }

        debug_assert!(self.balance().abs() <= 1);
    }
}
// END code

/* Templates
// START imports
AvlTree
// END imports
*/

#[cfg(test)]
mod tests {
    use super::*;
    use std::fmt::Debug;

    #[track_caller]
    fn assert_node_eq<T: Debug + Eq>(a: &Node<T>, b: &Node<T>) {
        assert_eq!(a.value, b.value);
        assert_eq!(a.height, b.height);
        match (&a.left.0, &b.left.0) {
            (Some(x), Some(y)) => assert_node_eq(x, y),
            (None, None) => (),
            _ => panic!("unequal left child in a={:?} b={:?}", a, b),
        }
        match (&a.right.0, &b.right.0) {
            (Some(x), Some(y)) => assert_node_eq(x, y),
            (None, None) => (),
            _ => panic!("unequal right child in a={:?} b={:?}", a, b),
        }
    }

    fn empty<T>() -> PotentialNode<T> {
        PotentialNode::empty()
    }

    fn make_node<T>(
        value: T,
        count: usize,
        left: PotentialNode<T>,
        right: PotentialNode<T>,
    ) -> PotentialNode<T> {
        PotentialNode::new(Box::new(Node {
            value,
            count,
            height: std::cmp::max(left.height(), right.height()) + 1,
            size: left.size() + right.size() + count,
            left,
            right,
        }))
    }

    fn make_node1<T>(
        value: T,
        left: PotentialNode<T>,
        right: PotentialNode<T>,
    ) -> PotentialNode<T> {
        make_node(value, 1, left, right)
    }

    fn make_tree<T: Ord + Debug>(values: Vec<T>) -> AvlTree<T> {
        let mut tree = AvlTree::new();
        for val in values {
            tree.insert(val);
        }
        tree
    }

    #[test]
    fn insert() {
        let mut tree = AvlTree::<i32>::new();
        for x in [-4, -2, 0, 2, 4, -1, 3, 7] {
            tree.insert(x);
            dbg!(&tree);
        }
        assert_node_eq(
            &*tree.root.0.unwrap(),
            &make_node(
                0,
                1,
                make_node(
                    -2,
                    1,
                    make_node(-4, 1, empty(), empty()),
                    make_node(-1, 1, empty(), empty()),
                ),
                make_node(
                    3,
                    1,
                    make_node(2, 1, empty(), empty()),
                    make_node(4, 1, empty(), make_node(7, 1, empty(), empty())),
                ),
            )
            .0
            .unwrap(),
        );

        let mut tree = AvlTree::<String>::new();
        for x in ["abc", "bccc", "bcc", "abd", "bcca"] {
            tree.insert(x.to_string());
        }
        assert_node_eq(
            &*tree.root.0.unwrap(),
            &make_node(
                String::from("bcc"),
                1,
                make_node(
                    String::from("abc"),
                    1,
                    empty(),
                    make_node(String::from("abd"), 1, empty(), empty()),
                ),
                make_node(
                    String::from("bccc"),
                    1,
                    make_node(String::from("bcca"), 1, empty(), empty()),
                    empty(),
                ),
            )
            .0
            .unwrap(),
        );

        let mut tree = AvlTree::<i64>::new();
        for x in [1, 2, 1, -1, 2, 1, -1, 0, 3, 2, 1, 5] {
            tree.insert(x);
        }

        assert_node_eq(
            &*tree.root.0.unwrap(),
            &make_node(
                1,
                4,
                make_node(-1, 2, empty(), make_node(0, 1, empty(), empty())),
                make_node(
                    3,
                    1,
                    make_node(2, 3, empty(), empty()),
                    make_node(5, 1, empty(), empty()),
                ),
            )
            .0
            .unwrap(),
        );
    }

    #[test]
    fn remove() {
        let mut tree = AvlTree::<i64>::new();
        for x in 1..=10 {
            tree.insert(x);
        }
        assert_node_eq(
            &*tree.root.0.as_ref().unwrap(),
            &make_node1(
                4,
                make_node1(
                    2,
                    make_node1(1, empty(), empty()),
                    make_node1(3, empty(), empty()),
                ),
                make_node1(
                    8,
                    make_node1(
                        6,
                        make_node1(5, empty(), empty()),
                        make_node1(7, empty(), empty()),
                    ),
                    make_node1(9, empty(), make_node1(10, empty(), empty())),
                ),
            )
            .0
            .unwrap(),
        );

        for x in 1..=5 {
            tree.remove(&x);
        }
        assert_node_eq(
            &*tree.root.0.as_ref().unwrap(),
            &make_node1(
                8,
                make_node1(6, empty(), make_node1(7, empty(), empty())),
                make_node1(9, empty(), make_node1(10, empty(), empty())),
            )
            .0
            .unwrap(),
        );

        let mut tree = AvlTree::<i32>::new();
        tree.insert(1);
        tree.insert(2);
        tree.insert(3);
        tree.remove(&2);
        assert_node_eq(
            &*tree.root.0.as_ref().unwrap(),
            &make_node1(1, empty(), make_node1(3, empty(), empty()))
                .0
                .unwrap(),
        );
    }

    #[test]
    fn find() {
        let tree = make_tree::<String>(vec![
            "abc".into(),
            "abb".into(),
            "abba".into(),
            "bba".into(),
            "bcb".into(),
            "bcad".into(),
        ]);
        assert!(tree.find("abb").is_some());
        assert!(tree.find("abba").is_some());
        assert!(tree.find("abbb").is_none());
        assert!(tree.find("bb").is_none());
        assert!(tree.find("bba").is_some());
        assert!(tree.find("bcb").is_some());

        #[derive(Debug)]
        struct ByLen(String);
        impl PartialEq for ByLen {
            fn eq(&self, other: &Self) -> bool {
                self.cmp(other) == Ordering::Equal
            }
        }
        impl Eq for ByLen {}
        impl PartialOrd for ByLen {
            fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
                Some(self.cmp(other))
            }
        }
        impl Ord for ByLen {
            fn cmp(&self, other: &Self) -> Ordering {
                self.0.len().cmp(&other.0.len())
            }
        }

        let tree = make_tree::<ByLen>(vec![
            ByLen("abc".into()),
            ByLen("dddd".into()),
            ByLen("x".into()),
            ByLen("hello".into()),
            ByLen("unknown".into()),
            ByLen("couldn't see me".into()),
        ]);
        assert_eq!(tree.find(&ByLen("a".into())).unwrap().0, "x");
        assert!(tree.find(&ByLen("bb".into())).is_none());
        assert_eq!(tree.find(&ByLen("aaa".into())).unwrap().0, "abc");
        assert_eq!(tree.find(&ByLen("temp".into())).unwrap().0, "dddd");
        assert_eq!(tree.find(&ByLen("world".into())).unwrap().0, "hello");
        assert!(tree.find(&ByLen("123456".into())).is_none());
        assert_eq!(tree.find(&ByLen("???????".into())).unwrap().0, "unknown");
        assert!(tree.find(&ByLen("welcome home".into())).is_none());
        assert_eq!(
            tree.find(&ByLen("could see me !!".into())).unwrap().0,
            "couldn't see me"
        );
    }

    #[test]
    fn count() {
        let tree = make_tree::<i32>((0..100).collect());
        assert_eq!(tree.count_lt(&-1), 0);
        assert_eq!(tree.count_le(&-1), 0);
        assert_eq!(tree.count_gt(&-1), 100);
        assert_eq!(tree.count_ge(&-1), 100);
        assert_eq!(tree.count_lt(&0), 0);
        assert_eq!(tree.count_le(&0), 1);
        assert_eq!(tree.count_gt(&0), 99);
        assert_eq!(tree.count_ge(&0), 100);
        assert_eq!(tree.count_lt(&17), 17);
        assert_eq!(tree.count_le(&17), 18);
        assert_eq!(tree.count_gt(&17), 82);
        assert_eq!(tree.count_ge(&17), 83);
        assert_eq!(tree.count_lt(&69), 69);
        assert_eq!(tree.count_le(&69), 70);
        assert_eq!(tree.count_gt(&69), 30);
        assert_eq!(tree.count_ge(&69), 31);
        assert_eq!(tree.count_lt(&110), 100);
        assert_eq!(tree.count_le(&110), 100);
        assert_eq!(tree.count_gt(&110), 0);
        assert_eq!(tree.count_ge(&110), 0);

        let tree = make_tree::<i32>(vec![1, 2, 1, 3, 1, 3, 4, 1, 2, 0, 6]);
        assert_eq!(tree.count_lt(&1), 1);
        assert_eq!(tree.count_le(&1), 5);
        assert_eq!(tree.count_gt(&3), 2);
        assert_eq!(tree.count_ge(&3), 4);
        assert_eq!(tree.count_gt(&7), 0);
        assert_eq!(tree.count_lt(&5), 10);
        assert_eq!(tree.count_le(&5), 10);

        assert_eq!(tree.count(&-1), 0);
        assert_eq!(tree.count(&0), 1);
        assert_eq!(tree.count(&1), 4);
        assert_eq!(tree.count(&2), 2);
        assert_eq!(tree.count(&3), 2);
        assert_eq!(tree.count(&4), 1);
        assert_eq!(tree.count(&5), 0);
        assert_eq!(tree.count(&6), 1);
        assert_eq!(tree.count(&7), 0);
    }

    #[test]
    fn select() {
        let tree = make_tree::<i32>(vec![6, 3, 2, 1, 6, 4, 2, 3, 3, 3, 0, 7, 8, 10]);
        for (i, &x) in [0, 1, 2, 2, 3, 3, 3, 3, 4, 6, 6, 7, 8, 10]
            .iter()
            .enumerate()
        {
            assert_eq!(tree.select(i), Some(&x));
        }
        assert_eq!(tree.select(14), None);
        assert_eq!(tree.select(15), None);
        assert_eq!(tree.select(usize::MAX), None);
    }

    #[test]
    fn composite_random() {
        let mut tree = AvlTree::<u64>::new();
        for i in 0u64..3000 {
            tree.insert(i.wrapping_mul(970875144426250549));
        }
        for i in 0u64..1000 {
            tree.remove(&(i * 3).wrapping_mul(970875144426250549));
        }

        let mut res = 0usize;
        for i in 0u64..2000 {
            res += tree.count_lt(&i.wrapping_mul(4584530927321024899));
        }
        assert_eq!(res, 1903116);
        let mut res = 0u64;
        for i in 0usize..2000 {
            res = res.wrapping_add(
                *tree
                    .select(i.wrapping_mul(2793196198016246683) % 2000)
                    .unwrap(),
            );
        }
        assert_eq!(res, 8741491219489754287);
    }
}
