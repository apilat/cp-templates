// NAME writer
// START code
use std::fmt::Display;
use std::io::{stdout, BufWriter, Error as IoError, Stdout, Write};

#[derive(Debug)]
pub struct Writer<W: Write> {
    writer: BufWriter<W>,
}

#[derive(Debug, Clone)]
pub struct Format<'a> {
    // Separator for multiple elements
    separator: &'a str,
    // End statement with newline
    ending: &'a str,
}

impl<'a> Format<'a> {
    pub fn separator(self, separator: &'a str) -> Self {
        Self { separator, ..self }
    }

    pub fn ending(self, ending: &'a str) -> Self {
        Self { ending, ..self }
    }
}

impl Default for Format<'_> {
    fn default() -> Self {
        Format {
            separator: " ",
            ending: "\n",
        }
    }
}

impl Writer<Stdout> {
    pub fn stdout() -> Self {
        Self::new(stdout())
    }
}

impl<W: Write> Writer<W> {
    pub fn new(writer: W) -> Self {
        Self {
            writer: BufWriter::new(writer),
        }
    }

    pub fn write<M, T: Writable<M>>(&mut self, val: T) -> Result<(), IoError> {
        self.write_with(val, &Format::default())
    }

    pub fn write_with<M, T: Writable<M>>(&mut self, val: T, fmt: &Format) -> Result<(), IoError> {
        val.write_to(&mut self.writer, fmt)
    }

    pub fn flush(&mut self) -> Result<(), IoError> {
        self.writer.flush()
    }
}

pub trait Writable<Mode> {
    fn write_to<W: Write>(self, w: &mut W, fmt: &Format) -> Result<(), IoError>;
}

#[non_exhaustive]
pub struct Single;
impl<T: Display> Writable<Single> for T {
    fn write_to<W: Write>(self, w: &mut W, fmt: &Format) -> Result<(), IoError> {
        write!(w, "{}{}", self, fmt.ending)
    }
}

#[non_exhaustive]
pub struct Many;
impl<I> Writable<Many> for I
where
    I: Iterator,
    I::Item: Display,
{
    fn write_to<W: Write>(mut self, w: &mut W, fmt: &Format) -> Result<(), IoError> {
        if let Some(x) = self.next() {
            write!(w, "{}", x)?;
        } else {
            // Don't output anything if empty
            return Ok(());
        }

        for x in self {
            write!(w, "{}{}", fmt.separator, x)?;
        }
        write!(w, "{}", fmt.ending)
    }
}

#[non_exhaustive]
pub struct Slice;
impl<T: Display> Writable<Slice> for &[T] {
    fn write_to<W: Write>(self, w: &mut W, fmt: &Format) -> Result<(), IoError> {
        self.iter().write_to(w, fmt)
    }
}

#[macro_export]
// The reason for this weird name is to make sure it does not alias with write! from the standard
// library.
macro_rules! out {
    ($writer:expr, $val:expr) => {
        $writer.write($val).expect("failed to write token");
    };

    ($writer:expr, $val:expr, $($builder_name:ident = $value:expr),+) => {
        let mut fmt = Format::default();
        $(
            fmt = fmt.$builder_name($value);
        )+
        $writer.write_with($val, &fmt).expect("failed to write token");
    };
}
// END code

/* Templates
// START imports
{Writer, Format}
// END imports
// START include_main
let mut wr = Writer::stdout();
// END include_main
*/

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::ErrorKind;

    #[test]
    fn writer() {
        let mut output = Vec::new();
        {
            let mut writer = Writer::new(&mut output);
            writer.write(1).unwrap();
            writer.write("abc").unwrap();
            writer.write(&[3.3, 1e3, 2001.0][..]).unwrap();
            writer
                .write_with("HELLO", &Format::default().ending(""))
                .unwrap();
        }
        assert_eq!(output, b"1\nabc\n3.3 1000 2001\nHELLO");
    }

    #[test]
    fn writer_io_error() {
        #[derive(Debug)]
        struct BadWriter {
            buf: Vec<u8>,
            size: usize,
        }
        impl BadWriter {
            fn new(size: usize) -> BadWriter {
                BadWriter {
                    size,
                    buf: Vec::new(),
                }
            }
        }
        impl Write for BadWriter {
            fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
                if self.buf.len() == self.size {
                    Err(IoError::new(ErrorKind::PermissionDenied, "Write failed"))
                } else {
                    let to_write = usize::min(self.size - self.buf.len(), buf.len());
                    self.buf.extend_from_slice(&buf[..to_write]);
                    Ok(to_write)
                }
            }
            fn flush(&mut self) -> std::io::Result<()> {
                Ok(())
            }
        }

        let mut output = BadWriter::new(10);
        {
            let mut writer = Writer::new(&mut output);
            writer.write(98).unwrap();
            writer.write("hello").unwrap();
            writer.flush().unwrap();
        }

        let mut output = BadWriter::new(10);
        {
            let mut writer = Writer::new(&mut output);
            writer.write(98).unwrap();
            writer.write("hello").unwrap();
            // This does not immediately return an error since the output to the underlying
            // BadWriter is buffered
            writer.write("yes").unwrap();
            writer.flush().unwrap_err();
        }
    }

    #[test]
    fn write_array() {
        let mut output = Vec::new();
        {
            let mut writer = Writer::new(&mut output);
            writer
                .write(&[&123u32 as &dyn Display, &"abc", &-3isize][..])
                .unwrap();
            writer
                .write(&[&"xyz" as &dyn Display, &'x', &'y', &'z'][..])
                .unwrap();
            writer
                .write_with(
                    &[123, 34, 54, 91][..],
                    &Format::default().separator(", ").ending(""),
                )
                .unwrap();
        }
        assert_eq!(output, b"123 abc -3\nxyz x y z\n123, 34, 54, 91");
    }

    #[test]
    fn write_many() {
        use std::iter::{empty, once, repeat};
        let mut output = Vec::new();
        {
            let mut writer = Writer::new(&mut output);
            writer.write(repeat(1).take(5)).unwrap();
            writer.write(["abc", "x", "z"].iter()).unwrap();
            writer.write(empty::<f64>()).unwrap();
            writer.write(once(1.30)).unwrap();
            writer
                .write_with(
                    [3, 2, 1].iter(),
                    &Format::default().separator(":").ending(","),
                )
                .unwrap();
            writer
                .write_with((-3..).take(3), &Format::default().separator(","))
                .unwrap();
        }
        assert_eq!(
            std::str::from_utf8(&output).unwrap(),
            "1 1 1 1 1\nabc x z\n1.3\n3:2:1,-3,-2,-1\n"
        );
    }

    #[test]
    fn out_macro() {
        let mut output = Vec::new();
        {
            let mut writer = Writer::new(&mut output);
            out!(writer, &[1, 2, 3][..]);
            out!(writer, "abc");
            out!(writer, 2.0 / 4.0, ending = "\t");
            out!(
                writer,
                vec![3, 4, 5, 6].iter(),
                separator = ",",
                ending = ""
            );
        }
        assert_eq!(output, b"1 2 3\nabc\n0.5\t3,4,5,6");
    }
}
