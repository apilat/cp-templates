// NAME combinatorics
// REQUIREMENT num
// REQUIREMENT number_theory
// START code
use crate::{num::Integer, number_theory::mod_mulinv};

/// Transforms `perm` to the lexicographically next permutation, wrapping around if it is already
/// the greatest permutation. Returns `true` if the new permutation is greater than the old one,
/// or `false` if it has wrapped around.
pub fn next_permutation<T>(perm: &mut [T]) -> bool
where
    T: Ord,
{
    if perm.is_empty() {
        return false;
    }

    // Last index where perm[i] < perm[i+1].
    let i = match Iterator::zip(perm.iter(), perm[1..].iter()).rposition(|(x, y)| x < y) {
        None => {
            // Reverse order, so this is the last permutation.
            perm.reverse();
            return false;
        }
        Some(x) => x,
    };

    // Last index where perm[i] < perm[j], which is the next largest value after perm[i] in perm[i..].
    let j = perm
        .iter()
        .rposition(|x| &perm[i] < x)
        .expect("inconsistent Ord implementation");

    perm.swap(i, j);
    perm[i + 1..].reverse();
    true
}

/// A data store for precomputed factorials which allows you to easily compute combinatoric functions
/// modulo a constant.
///
/// The time complexities of operations are:
/// - `new: O(n + log m)`
#[derive(Debug, Clone)]
// See note on number_theory::PrimeSieve for why this doesn't implement Eq.
pub struct Factorials<N> {
    m: N,
    fact: Vec<N>,
    fact_inv: Vec<N>,
}

impl<N> Factorials<N>
where
    N: Integer + Clone,
{
    #[inline]
    fn as_n(x: usize) -> N {
        N::from_usize(x).expect("inconsistent Integer implementation")
    }
    #[inline]
    fn as_usize(x: &N) -> usize {
        N::as_usize(x).expect("inconsistent Integer implementation")
    }

    /// Computes the factorials up to `n` inclusive, modulo `m`.
    ///
    /// Panics if the n is negative, required capacity overflows `usize` or `n!` is not invertible modulo `m`.
    /// If an intermediate result overflows the target type, the results are unspecified.
    #[track_caller]
    pub fn new(n: N, m: N) -> Self {
        let cap = n
            .as_usize()
            .and_then(|x| x.checked_add(1))
            .expect("required capacity overflowed usize");
        assert!(cap > 0, "n must be non-negative");

        let mut fact = Vec::with_capacity(cap);
        fact.push(N::one());
        let mut cur = N::one();
        for i in 1..cap {
            cur = (cur * Self::as_n(i)).imod(&m);
            fact.push(cur.clone());
        }

        // Build the inverses from the back to reduce number of calls to mulinv.
        let mut fact_inv = Vec::with_capacity(cap);
        let mut cur = mod_mulinv(fact[cap - 1].clone(), m.clone());
        for i in (0..cap).rev() {
            fact_inv.push(cur.clone());
            cur = (cur * Self::as_n(i)).imod(&m);
        }
        fact_inv.reverse();

        debug_assert_eq!(fact.len(), cap);
        debug_assert_eq!(fact_inv.len(), cap);
        Factorials { m, fact, fact_inv }
    }

    /// Returns the length of the precomputation (1 more than the highest factorial available).
    pub fn len(&self) -> usize {
        self.fact.len()
    }

    /// Returns `true` if the precomputation is empty. This never happens, so this function always
    /// returns `false`.
    pub fn is_empty(&self) -> bool {
        false
    }

    /// Returns the largest number whose factorial has been computed.
    pub fn n(&self) -> N {
        Self::as_n(self.fact.len() - 1)
    }

    /// Verifies that `n` is in the precomputed range and returns it. Panics otherwise.
    #[track_caller]
    fn verify_n<'a>(&self, n: &'a N) -> &'a N {
        assert!(
            N::zero() <= *n && *n <= self.n(),
            "n is outside precomputed range"
        );
        n
    }

    /// Returns `n! (mod m)`.
    ///
    /// Panics if `n` is outside the precomputed range.
    #[track_caller]
    pub fn factorial(&self, n: &N) -> N {
        let n = self.verify_n(n);
        self.fact[Self::as_usize(n)].clone()
    }

    /// Returns `1/n! (mod m)`.
    ///
    /// Panics if `n` is outside the precomputed range.
    #[track_caller]
    pub fn factorial_inverse(&self, n: &N) -> N {
        let n = self.verify_n(n);
        self.fact_inv[Self::as_usize(n)].clone()
    }

    /// Returns the binomial coefficient `n` choose `k` modulo `m`.
    ///
    /// Panics if `n` is outside the precomputed range or `k` is negative.
    #[track_caller]
    pub fn choose(&self, n: N, k: N) -> N {
        self.verify_n(&n);
        assert!(k >= N::zero(), "k is negative");
        if k <= n {
            // n! / k! / (n-k)!
            ((self.fact[Self::as_usize(&n)].clone() * self.fact_inv[Self::as_usize(&k)].clone())
                .imod(&self.m)
                * self.fact_inv[Self::as_usize(&(n - k))].clone())
            .imod(&self.m)
        } else {
            N::zero()
        }
    }
}
// END code

/* Templates
// START imports
{Factorials, next_permutation}
// END imports
*/

#[cfg(test)]
mod tests {
    use super::*;
    use std::fmt::Debug;
    use std::panic::catch_unwind;

    #[track_caller]
    fn check_permutations<T: Ord + Clone + Debug>(mut x: Vec<T>, expected_count: u32) {
        let lowest = {
            let mut a = x.clone();
            a.sort();
            a
        };

        let mut count = 0;
        loop {
            let prev = x.clone();
            count += 1;
            if next_permutation(&mut x) {
                assert!(x > prev);
            } else {
                break;
            }
        }

        assert_eq!(x, lowest);
        assert_eq!(count, expected_count);
    }

    #[test]
    fn next_permutation_normal() {
        check_permutations(Vec::<i8>::new(), 1);
        check_permutations(vec![u32::MAX], 1);
        check_permutations(vec![0, 1], 2);
        check_permutations(vec![1, 0], 1);
        check_permutations((0..6).collect(), 720);
        check_permutations(vec!["a", "b", "bc", "c", "e"], 120);
    }

    #[test]
    fn next_permutation_equal() {
        check_permutations(vec!["a", "a", "b", "a"], 3);
        check_permutations(vec![1, 2, 3, 3, 4, 5], 360);
        check_permutations(vec![0, 0, 0, 0], 1);
        check_permutations(vec![0, 1, 0, 1], 5);
    }

    #[test]
    fn factorials() {
        let factorials = Factorials::<i64>::new(40, 1_000_000_007);
        assert_eq!(
            factorials.fact,
            [
                1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800, 39916800, 479001600,
                227020758, 178290591, 674358851, 789741546, 425606191, 660911389, 557316307,
                146326063, 72847302, 602640637, 860734560, 657629300, 440732388, 459042011,
                394134213, 35757887, 36978716, 109361473, 390205642, 486580460, 57155068,
                943272305, 14530444, 523095984, 354551275, 472948359, 444985875, 799434881
            ]
        );
        assert_eq!(
            factorials.fact_inv,
            [
                1, 1, 500000004, 166666668, 41666667, 808333339, 301388891, 900198419, 487524805,
                831947206, 283194722, 571199524, 380933296, 490841026, 320774361, 821384963,
                738836565, 514049213, 639669405, 402087866, 120104394, 862862120, 130130097,
                179570875, 799148792, 791965957, 761229465, 917082579, 282752951, 699405279,
                123313510, 649139150, 957785605, 604781386, 958964165, 170256120, 671396008,
                261389083, 243720767, 211377457, 230284438
            ]
        );

        assert!(catch_unwind(|| factorials.factorial(&-1)).is_err());
        assert_eq!(factorials.factorial(&0), 1);
        assert_eq!(factorials.factorial(&1), 1);
        assert_eq!(factorials.factorial(&5), 120);
        assert_eq!(factorials.factorial(&40), 799434881);
        assert!(catch_unwind(|| factorials.factorial(&41)).is_err());

        assert!(catch_unwind(|| factorials.factorial_inverse(&-1)).is_err());
        assert_eq!(factorials.factorial_inverse(&0), 1);
        assert_eq!(factorials.factorial_inverse(&1), 1);
        assert_eq!(factorials.factorial_inverse(&5), 808333339);
        assert_eq!(factorials.factorial_inverse(&40), 230284438);
        assert!(catch_unwind(|| factorials.factorial_inverse(&41)).is_err());

        assert!(catch_unwind(|| factorials.choose(-1, 1)).is_err());
        assert!(catch_unwind(|| factorials.choose(1, -1)).is_err());
        assert_eq!(factorials.choose(0, 0), 1);
        assert_eq!(factorials.choose(1, 0), 1);
        assert_eq!(factorials.choose(1, 1), 1);
        assert_eq!(factorials.choose(0, 1), 0);
        assert!(catch_unwind(|| factorials.choose(40, -1)).is_err());
        assert_eq!(factorials.choose(40, 0), 1);
        assert_eq!(factorials.choose(40, 17), 732378184);
        assert_eq!(factorials.choose(40, 27), 33222796);
        assert_eq!(factorials.choose(40, 40), 1);
        assert_eq!(factorials.choose(40, 41), 0);
        assert!(catch_unwind(|| factorials.choose(41, 10)).is_err());
    }

    #[test]
    fn factorials_random() {
        let factorials = Factorials::<i64>::new(5000, 1684207661);
        let mut res = 0;
        for i in 0u64..3000 {
            res ^= factorials.factorial(&((i.wrapping_mul(13382058426586094383) % 5000) as i64));
            res ^= factorials
                .factorial_inverse(&((i.wrapping_mul(7720031967033978577) % 5000) as i64));
            res ^= factorials.choose(
                (i.wrapping_mul(5204712958343805163) % 5000) as i64,
                (i.wrapping_mul(4935223006647559) % 5000) as i64,
            );
        }
        assert_eq!(res, 881539165);
    }
}
