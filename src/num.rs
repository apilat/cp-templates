// NAME num
// START code
use std::convert::{TryFrom, TryInto};
use std::ops::{Add, Div, Mul, Rem, Sub};

/// Trait for generalizing numbers. They must form a total order and support the common operations
/// of `add`, `sub`, `mul`, `div`.
pub trait Num:
    Ord
    + Add<Output = Self>
    + Sub<Output = Self>
    + Mul<Output = Self>
    + Div<Output = Self>
    + Rem<Output = Self>
    + FromPrimitive
    + IntoPrimitive
    + Sized
{
    /// Returns `0` which is the additive identity.
    fn zero() -> Self;
    /// Returns `1` which is the multiplicative identity.
    fn one() -> Self;
}

pub trait FromPrimitive: Sized {
    /// Returns `x` as this type if it is representable.
    fn from_u64(x: u64) -> Option<Self>;
    /// Returns `x` as this type if it is representable.
    fn from_i64(x: i64) -> Option<Self>;

    /// Returns `x` as this type if it is representable.
    fn from_usize(x: usize) -> Option<Self> {
        Self::from_u64(x as u64)
    }
    /// Returns `x` as this type if it is representable.
    fn from_isize(x: isize) -> Option<Self> {
        Self::from_i64(x as i64)
    }
}

pub trait IntoPrimitive {
    /// Returns `self` as `u64` if it is representable.
    fn as_u64(&self) -> Option<u64>;
    /// Returns `self` as `i64` if it is representable.
    fn as_i64(&self) -> Option<i64>;

    /// Returns `self` as `usize` if it is representable.
    fn as_usize(&self) -> Option<usize> {
        self.as_u64().and_then(|x| x.try_into().ok())
    }
    /// Returns `self` as `isize` if it is representable.
    fn as_isize(&self) -> Option<isize> {
        self.as_i64().and_then(|x| x.try_into().ok())
    }
}

/// Trait for generalizing integers. In addition to being numbers, they must support integer
/// division and remainder.
///
/// `(x idiv y) * y + x imod y` must equal `x` for any `x,y`.
/// `0 <= x imod y < y` for any `x,y`.
pub trait Integer: Num {
    /// Returns the integer part of `self / x`.
    fn idiv(&self, x: &Self) -> Self;

    /// Returns the modulus of `self` divided by `x`.
    fn imod(&self, x: &Self) -> Self;

    /// Returns the absolute value of `self`.
    fn iabs(&self) -> Self;
}

macro_rules! generate_integer_impl_for_primitive {
    ($( ($ty:ty, $unsigned:tt) ),+) => {
        $(
            impl FromPrimitive for $ty {
                #[inline(always)]
                fn from_u64(x: u64) -> Option<Self> {
                    <$ty>::try_from(x).ok()
                }
                #[inline(always)]
                fn from_i64(x: i64) -> Option<Self> {
                    <$ty>::try_from(x).ok()
                }
            }

            impl IntoPrimitive for $ty {
                #[inline(always)]
                fn as_u64(&self) -> Option<u64> {
                    u64::try_from(*self).ok()
                }
                #[inline(always)]
                fn as_i64(&self) -> Option<i64> {
                    i64::try_from(*self).ok()
                }
            }

            impl Num for $ty {
                #[inline(always)]
                fn zero() -> Self {
                    0
                }
                #[inline(always)]
                fn one() -> Self {
                    1
                }
            }

            impl Integer for $ty {
                #[inline(always)]
                fn idiv(&self, x: &Self) -> Self {
                    self.div_euclid(*x)
                }
                #[inline(always)]
                fn imod(&self, x: &Self) -> Self {
                    self.rem_euclid(*x)
                }
                generate_iabs_impl!($unsigned);
            }
        )+
    };
}

macro_rules! generate_iabs_impl {
    (true) => {
        #[inline(always)]
        fn iabs(&self) -> Self {
            *self
        }
    };
    (false) => {
        #[inline(always)]
        fn iabs(&self) -> Self {
            self.abs()
        }
    };
}

generate_integer_impl_for_primitive!(
    (u8, true),
    (u16, true),
    (u32, true),
    (u64, true),
    (u128, true),
    (usize, true),
    (i8, false),
    (i16, false),
    (i32, false),
    (i64, false),
    (i128, false),
    (isize, false)
);
// END code
