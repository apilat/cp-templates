// NAME number_theory
// REQUIREMENT num
// START code
use crate::num::Integer;
use std::mem;

// TODO Implement Modular<const M> type which encapsulates integers modulo M however this is not
// particularly useful for CP right now since const generics were only stabilized in 1.51 which is
// newer than many platforms support.

// TODO Remove unncessary clones where a reference would be sufficient (this would improve
// performance for possible future bigint but might hurt normal ergonomics). This will likely need
// Num to include XxAssign operations as well as operations using references.

/// Returns the greatest common divisor of `a` and `b`.
///
/// The returned divisor is guaranteed to be non-negative. The function might panic or return
/// non-sense if this is not representable as the target type.
pub fn gcd<N: Integer>(mut a: N, mut b: N) -> N {
    while b != N::zero() {
        mem::swap(&mut a, &mut b);
        b = b.imod(&a);
    }
    a.iabs()
}

/// Returns `(g, s, t)`, where `g` is the greatest common divisor of `a` and `b`, and `s` and `t`
/// are the Bézout coefficients such that `a*s + b*t = g`.
///
/// The returned divisor follows the the same convention as `gcd`.
#[allow(clippy::many_single_char_names)]
pub fn egcd<N: Integer + Clone>(a: N, b: N) -> (N, N, N) {
    if b == N::zero() {
        (
            a.iabs(),
            if a >= N::zero() {
                N::one()
            } else {
                N::zero() - N::one()
            },
            N::zero(),
        )
    } else {
        let (g, s, t) = egcd(b.clone(), a.imod(&b));
        (g, t.clone(), s - a.idiv(&b) * t)
    }
}

/// Returns `(a ^ b) % m` computed efficiently. The return value is guaranteed to be in `[0, m)`.
///
/// Panics if `m` is not positive, `b` is `i64::MIN` (it cannot be negated) or `b` is negative and
/// `a` does not have a multiplicative inverse modulo `m`. If intermediate computations overflow,
/// the behavior in unspecified.
#[track_caller]
pub fn mod_exp<N: Integer + Clone>(a: N, mut b: N, m: N) -> N {
    let zero = N::zero();
    let one = N::one();
    let two = N::one() + N::one();

    assert!(m > zero, "modulus must be positive");
    if b < zero {
        return mod_exp(mod_mulinv(a, m.clone()), zero - b, m);
    }

    let mut x = N::one();
    let mut pwr = a;
    while b > N::zero() {
        if b.imod(&two) == one {
            x = (x * pwr.clone()).imod(&m);
        }
        pwr = (pwr.clone() * pwr).imod(&m);
        b = b.idiv(&two);
    }
    x
}

/// Returns the multiplicative inverse of `a` modulo `m`. The return value is guaranteed to be in
/// `[0, m)`.
///
/// Panics if an inverse does not exist or `m` is not positive.
#[track_caller]
pub fn mod_mulinv<N: Integer + Clone>(a: N, m: N) -> N {
    let zero = N::zero();
    let one = N::one();

    assert!(m > zero, "modulus must be positive");
    let (g, s, _) = egcd(a, m.clone());
    assert!(g == one, "multiplicative inverse does not exist");
    s.imod(&m)
}

/// Prime sieve which allows you to efficiently find all primes below a given bounds and factorize
/// all composite numbers.
///
/// Time complexity of operations:
/// - `new(n): O(n)`
/// - `factorize(x):` `O(log x)` if `x <= n`, `O(sqrt x / log x)` otherwise
/// - `is_prime(x), lowest_factor(x):` `O(1)` if `x <= n`, `O(sqrt x / log x)` otherwise
#[derive(Debug, Clone)]
// This could implement PartialEq,Eq by simply checking self.len() but the usefulness is debatable
// and it could mess with tests which expect assert_eq!() to actually check equality.
pub struct PrimeSieve<N> {
    lowest_factor: Vec<N>,
    primes: Vec<N>,
}

impl<N> PrimeSieve<N>
where
    N: Integer + Clone,
{
    #[inline]
    fn as_n(x: usize) -> N {
        N::from_usize(x).expect("inconsistent Integer implementation")
    }
    #[inline]
    fn as_usize(x: &N) -> usize {
        N::as_usize(x).expect("inconsistent Integer implementation")
    }

    /// Computes a prime sieve up to `n` (inclusive).
    ///
    /// Panics if `n` is negative or `n+1` overflows `usize`.
    #[track_caller]
    pub fn new(n: N) -> Self {
        let cap = n
            .as_usize()
            .and_then(|x| x.checked_add(1))
            .expect("capacity overflowed usize");
        assert!(cap > 0, "n must be non-negative");

        let mut lowest_factor = vec![N::zero(); cap];
        let mut primes = vec![];

        for i in 2..cap {
            if lowest_factor[i] == N::zero() {
                lowest_factor[i] = Self::as_n(i);
                primes.push(Self::as_n(i));
            }

            // Need to clone here since compiler cannot prove lowest_factor is not accessed mutably
            // at the same time.
            let lowest_factor_i = lowest_factor[i].clone();
            for (p, p_usize) in primes
                .iter()
                .map(|p| (p, Self::as_usize(p)))
                .take_while(|(p, p_usize)| i * p_usize < cap && p <= &&lowest_factor_i)
            {
                lowest_factor[i * p_usize] = p.clone();
            }
        }

        PrimeSieve {
            lowest_factor,
            primes,
        }
    }

    /// Returns the length of the prime sieve (1 more than the highest number available).
    pub fn len(&self) -> usize {
        self.lowest_factor.len()
    }

    /// Returns `true` if the prime sieve is empty. This never happens, so this function always
    /// returns `false`.
    pub fn is_empty(&self) -> bool {
        false
    }

    /// Returns the largest number that is included in the prime sieve.
    pub fn n(&self) -> N {
        Self::as_n(self.lowest_factor.len() - 1)
    }

    /// Returns `true` if `x` is prime.
    ///
    /// Panics if `x` is negative or the prime sieve does not have enough elements to
    /// determine if `x` is prime, i.e. if `x > n^2`.
    #[track_caller]
    pub fn is_prime(&self, x: &N) -> bool {
        assert!(*x >= N::zero(), "x must be non-negative");
        let n = self.n();
        if *x <= n {
            *x > N::zero() && self.lowest_factor[Self::as_usize(x)] == *x
        } else if *x <= n.clone() * n {
            for p in self.primes.iter() {
                if x.imod(p) == N::zero() {
                    return false;
                }
            }
            true
        } else {
            panic!("unable to determine primality: x > n^2");
        }
    }

    /// Returns the lowest factor of `x` that is greater than `1`.
    /// If `x` is `0` or `1`, no such factor exists and `0` is returned instead.
    ///
    /// Panics if `x` is negative the prime sieve does not have enough elements
    /// to determine this, i.e. if `x > n^2`.
    #[track_caller]
    pub fn lowest_factor(&self, x: &N) -> N {
        assert!(*x >= N::zero(), "x must be non-negative");
        let n = self.n();
        if *x <= n {
            self.lowest_factor[Self::as_usize(x)].clone()
        } else if *x <= n.clone() * n {
            for p in self
                .primes
                .iter()
                .take_while(|&p| p.clone() * p.clone() <= *x)
            {
                if x.imod(p) == N::zero() {
                    return p.clone();
                }
            }
            x.clone()
        } else {
            panic!("unable to determine lowest factor: x > n^2",)
        }
    }

    /// Returns a vector of prime factors of `x`. Note that it is not guaranteed to be sorted.
    ///
    /// Panics if `x <= 0` or the sieve does not have enough elements to determine this, i.e. if `x >
    /// n^2`.
    #[track_caller]
    pub fn factorize(&self, mut x: N) -> Vec<N> {
        let n = self.n();
        assert!(x > N::zero(), "cannot factorize non-positive numbers");
        assert!(x <= n.clone() * n.clone(), "unable to factorize: x > n^2",);
        let mut factors = Vec::new();
        let x2 = x.clone();
        let mut primes = self
            .primes
            .iter()
            .take_while(|&p| p.clone() * p.clone() <= x2);
        while x > N::one() {
            if x <= n {
                let f = &self.lowest_factor[Self::as_usize(&x)];
                factors.push(f.clone());
                x = x.idiv(f);
            } else if let Some(p) = primes.next() {
                while x.imod(p) == N::zero() {
                    factors.push(p.clone());
                    x = x.idiv(p);
                }
            } else {
                // If we have consumed all primes up to n and x > 1 it must be a prime larger than n.
                factors.push(x);
                break;
            }
        }
        factors
    }
}
// END code

/* Templates
// START imports
{gcd, egcd, mod_exp, mod_mulinv, PrimeSieve}
// END imports
*/

#[cfg(test)]
mod tests {
    use super::*;
    use std::convert::TryFrom;
    use std::fmt::Debug;
    use std::panic::catch_unwind;

    #[test]
    fn gcd_() {
        assert_eq!(gcd::<i8>(1, 0), 1);
        assert_eq!(gcd::<u16>(3, 0), 3);
        assert_eq!(gcd::<i64>(i64::MAX, i64::MAX), i64::MAX);
        assert_eq!(gcd::<i64>(1 << 63, 1 << 62), 1 << 62);
        assert_eq!(
            gcd::<u64>(473248674540728559, 470559049380524060),
            603983017
        );
        assert_eq!(
            gcd::<u64>(101841997870789268, 152721654225124688),
            1129008044
        );

        assert_eq!(gcd::<i64>(i64::MAX, i64::MIN), 1);
        assert_eq!(gcd::<i64>(-1, 9), 1);
        assert_eq!(gcd::<i64>(-6, -8), 2);
        assert_eq!(
            gcd::<i64>(189223002891731781, -570620135966569416),
            765027711
        );
        assert_eq!(
            gcd::<i64>(-56587095219349200, -111048485285026696),
            950151752
        );

        // This is no longer defined
        //assert_eq!(gcd::<i64>(i64::MIN, 0), i64::MIN);
        //assert_eq!(gcd::<i64>(i64::MIN, i64::MIN), i64::MIN);
    }

    #[test]
    fn egcd_() {
        #[track_caller]
        fn test(a: i64, b: i64) {
            let (g, s, t) = egcd(a, b);
            assert_eq!(g, gcd(a, b), "incorrect gcd");
            assert_eq!(
                i64::try_from(a as i128 * s as i128 + b as i128 * t as i128).unwrap(),
                g,
                "incorrect bezout coefficients"
            );
        }

        test(1, 0);
        test(0, -4);
        test(i64::MAX, i64::MAX);
        test(i64::MAX, i64::MIN);
        test(i64::MAX, 0);
        test(332871506478498782, 332871506478498782);
        test(-339302781836039591, 657241338739516269);
        test(-157085568652559310, -192806349195580413);

        // No longer defined
        //test(i64::MIN, 0);
        //test(i64::MIN, i64::MIN);
    }

    #[test]
    fn mod_exp_() {
        assert_eq!(mod_exp(1, 0, 1013), 1);
        assert_eq!(mod_exp(5, 2, 1013), 25);
        assert_eq!(mod_exp(0, 3, 1013), 0);
        assert_eq!(mod_exp(-3, 3, 1013), 986);
        assert_eq!(mod_exp(3, 10, 1013), 295);
        assert_eq!(mod_exp(3, -10, 1013), 182);

        // Cannot rely on a panic in release mode
        //assert!(catch_unwind(|| mod_exp(1, i64::MIN, 2)).is_err());
        assert!(catch_unwind(|| mod_exp(1, 3, 0)).is_err());
        assert!(catch_unwind(|| mod_exp(1, 3, -10)).is_err());

        assert_eq!(mod_exp(631913225, 224590222, 1000000007), 558902119i64);
        assert_eq!(mod_exp(287660029, 498173971, 536819534), 171934115i64);
        assert_eq!(mod_exp(377928231, 406575040, 393798675), 212205051i64);
    }

    #[test]
    fn mod_mulinv_() {
        assert_eq!(mod_mulinv(1, 2), 1);
        assert_eq!(mod_mulinv(100, 1013), 233);
        assert_eq!(mod_mulinv(9, 7), 4);
        assert_eq!(mod_mulinv(125, 8), 5);

        assert!(catch_unwind(|| mod_mulinv(1, -1)).is_err());
        assert!(catch_unwind(|| mod_mulinv(1, 0)).is_err());
        assert!(catch_unwind(|| mod_mulinv(0, 3)).is_err());
        assert!(catch_unwind(|| mod_mulinv(2, 4)).is_err());
        assert!(catch_unwind(|| mod_mulinv(377928230, 406575040)).is_err());

        assert_eq!(mod_mulinv(419284005, 1000000007), 259975247);
        assert_eq!(mod_mulinv(189649391, 359503237), 89809209);
        assert_eq!(mod_mulinv(888828557, 29588503), 24772035);
    }

    #[test]
    fn prime_sieve() {
        #[track_caller]
        fn verify<N>(sieve: &PrimeSieve<N>, x: N, factorization: &[N])
        where
            N: Integer + Clone + Debug,
        {
            assert_eq!(sieve.is_prime(&x), factorization.len() == 1);
            assert_eq!(
                sieve.lowest_factor(&x),
                factorization.get(0).unwrap_or(&N::zero()).clone()
            );
            assert_eq!(sieve.factorize(x), factorization);
        }

        let sieve = PrimeSieve::<i16>::new(50);
        assert_eq!(
            sieve.lowest_factor,
            [
                0, 0, 2, 3, 2, 5, 2, 7, 2, 3, 2, 11, 2, 13, 2, 3, 2, 17, 2, 19, 2, 3, 2, 23, 2, 5,
                2, 3, 2, 29, 2, 31, 2, 3, 2, 5, 2, 37, 2, 3, 2, 41, 2, 43, 2, 3, 2, 47, 2, 7, 2
            ]
        );
        assert_eq!(
            sieve.primes,
            [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47]
        );

        assert!(catch_unwind(|| sieve.is_prime(&-1)).is_err());
        assert_eq!(sieve.is_prime(&0), false);
        assert_eq!(sieve.is_prime(&1), false);
        assert!(catch_unwind(|| sieve.lowest_factor(&-1)).is_err());
        assert_eq!(sieve.lowest_factor(&0), 0);
        assert_eq!(sieve.lowest_factor(&1), 0);
        assert!(catch_unwind(|| sieve.factorize(-1)).is_err());
        assert!(catch_unwind(|| sieve.factorize(0)).is_err());
        verify(&sieve, 1, &[]);
        verify(&sieve, 2, &[2]);
        verify(&sieve, 5, &[5]);
        verify(&sieve, 36, &[2, 2, 3, 3]);
        verify(&sieve, 1245, &[3, 5, 83]);
        verify(&sieve, 2500, &[2, 2, 5, 5, 5, 5]);
        assert!(catch_unwind(|| sieve.is_prime(&2501)).is_err());
        assert!(catch_unwind(|| sieve.lowest_factor(&2501)).is_err());
        assert!(catch_unwind(|| sieve.factorize(2501)).is_err());

        let sieve = PrimeSieve::<i64>::new(999983);
        assert_eq!(sieve.primes.len(), 78498);
        assert_eq!(
            sieve.primes[sieve.primes.len() - 20..],
            [
                999671, 999683, 999721, 999727, 999749, 999763, 999769, 999773, 999809, 999853,
                999863, 999883, 999907, 999917, 999931, 999953, 999959, 999961, 999979, 999983
            ]
        );

        verify(&sieve, 470301, &[3, 13, 31, 389]);
        verify(&sieve, 470303, &[470303]);
        verify(&sieve, 470315, &[5, 94063]);
        verify(&sieve, 470317, &[470317]);
        verify(&sieve, 8459050933, &[19, 293, 1519499]);
        verify(&sieve, 8459050949, &[8459050949]);
        verify(&sieve, 8459050951, &[13, 650696227]);
        verify(&sieve, 999966000289, &[999983, 999983]);
        assert!(catch_unwind(|| sieve.is_prime(&999966000290)).is_err());
        assert!(catch_unwind(|| sieve.lowest_factor(&999966000290)).is_err());
        assert!(catch_unwind(|| sieve.factorize(999966000290)).is_err());

        assert!(catch_unwind(|| PrimeSieve::new(usize::MAX as u64)).is_err());
    }

    #[test]
    fn prime_sieve_random() {
        const R: u64 = 2167048859;
        const P: u64 = 78341;
        let sieve = PrimeSieve::new(P);
        let mut res = 0;
        for i in 0..2000 {
            let x = (R * (i + 1)) % if i < 1000 { P } else { P * P };
            res += sieve.factorize(x).into_iter().sum::<u64>();
        }
        assert_eq!(res, 213028942576);
    }
}
