// NAME sparse_table
// START code
use std::{
    fmt::Debug,
    ops::{Bound, Range, RangeBounds},
};

/// Specification of an operation used in a sparse table.
///
/// Must be associative, i.e. `op(x, op(y, z)) = op(op(x, y), z)` for any values `x,y,z`.
/// If `IDEMPOTENT` is set to true, must be idempotent, i.e. `op(x, x) = x` for any `x`.
pub trait Spec {
    const IDEMPOTENT: bool;
    type Data: Clone;
    fn op(&self, a: &Self::Data, b: &Self::Data) -> Self::Data;
}

/// Sparse table data structure which supports range queries for arbitrary associative operations.
///
/// Time complexity, assuming `Spec::op` is `O(1)`:
/// - `new: O(n log n)`
/// - `query:` `O(1)` if the operation is idempotent, `O(log n)` otherwise
#[derive(Debug, Clone)]
pub struct SparseTable<S: Spec> {
    spec: S,
    n: usize,
    data: Vec<S::Data>,
}

impl<S: Spec> SparseTable<S> {
    /// Creates a new sparse table over the elements in `vals`.
    #[track_caller]
    pub fn new(vals: Vec<S::Data>, spec: S) -> Self {
        let n = vals.len();
        let d = (0usize.leading_zeros() - n.leading_zeros()) as usize;
        let size = n
            .checked_add(1)
            .and_then(|x| x.checked_mul(d))
            .and_then(|x| x.checked_add(1))
            .expect("size needed for table overflowed")
            - (1 << d);

        let mut data = vals;
        data.reserve(size);
        for i in 1..d {
            let len = 1 << i;
            let half_len = 1 << (i - 1);
            let prev_offset = Self::offset_to_depth(n, i - 1);
            for j in 0..=n - len {
                data.push(spec.op(&data[prev_offset + j], &data[prev_offset + j + half_len]));
            }
        }

        debug_assert_eq!(data.len(), size);
        Self { spec, n, data }
    }

    /// Offset to depth `d` in sparse table, assuming len `n`.
    fn offset_to_depth(n: usize, d: usize) -> usize {
        d * (n + 1) + 1 - (1 << d)
    }

    /// Returns the length of the underlying array.
    pub fn len(&self) -> usize {
        self.n
    }

    /// Returns `true` if the underlying array is empty.
    pub fn is_empty(&self) -> bool {
        self.n == 0
    }

    /// Returns the depth of the table.
    pub fn depth(&self) -> usize {
        (0usize.leading_zeros() - self.n.leading_zeros()) as usize
    }

    /// Verifies the range bounds are valid and returns the range `[l, r)` they represent.
    /// Panics on error
    #[track_caller]
    fn verify_range_bounds(&self, range: impl RangeBounds<usize>) -> Range<usize> {
        let left = match range.start_bound() {
            Bound::Included(&x) => x,
            Bound::Excluded(&x) => x + 1,
            Bound::Unbounded => 0,
        };
        let right = match range.end_bound() {
            Bound::Included(&x) => x + 1,
            Bound::Excluded(&x) => x,
            Bound::Unbounded => self.len(),
        };
        assert!(
            left < right,
            "range inverted: left >= right ({} >= {})",
            left,
            right
        );
        assert!(
            right <= self.len(),
            "range out of bounds: right > n ({} > {})",
            right,
            self.len()
        );
        Range {
            start: left,
            end: right,
        }
    }

    /// Returns the aggregate of `op` applied for all elements in `range`.
    #[track_caller]
    pub fn query(&self, range: impl RangeBounds<usize>) -> S::Data {
        let Range {
            start: left,
            end: right,
        } = self.verify_range_bounds(range);
        let len = right - left;
        let depth = (0usize.leading_zeros() - 1 - len.leading_zeros()) as usize;
        let offset = Self::offset_to_depth(self.n, depth);

        if len == 1 << depth {
            self.data[offset + left].clone()
        } else if S::IDEMPOTENT {
            self.spec.op(
                &self.data[offset + left],
                &self.data[offset + right - (1 << depth)],
            )
        } else {
            let mut val = self.data[offset + left].clone();
            let (mut pos, mut len) = (left + (1 << depth), len - (1 << depth));
            for i in (0..depth).rev() {
                if len & (1 << i) > 0 {
                    // We might be able to update the offset more efficiently when iterating but
                    // the performance improvement is likely negligible.
                    val = self
                        .spec
                        .op(&val, &self.data[Self::offset_to_depth(self.n, i) + pos]);
                    len -= 1 << i;
                    pos += 1 << i;
                }
            }
            val
        }
    }
}

pub mod ops {
    use super::Spec;
    use std::{marker::PhantomData, ops::Add};

    /// Sparse table specification for calculating the sum of a range.
    #[derive(Debug, Clone, Default)]
    pub struct Sum<T>(PhantomData<T>);
    impl<T> Spec for Sum<T>
    where
        T: Clone + Add<Output = T>,
    {
        type Data = T;
        const IDEMPOTENT: bool = false;
        #[inline(always)]
        fn op(&self, a: &Self::Data, b: &Self::Data) -> Self::Data {
            a.clone() + b.clone()
        }
    }

    /// Sparse table specification for calculating the minimum of a range.
    #[derive(Debug, Clone, Default)]
    pub struct Min<T>(PhantomData<T>);
    impl<T> Spec for Min<T>
    where
        T: Clone + Ord,
    {
        type Data = T;
        const IDEMPOTENT: bool = true;
        #[inline(always)]
        fn op(&self, a: &Self::Data, b: &Self::Data) -> Self::Data {
            Ord::min(a, b).clone()
        }
    }

    /// Sparse table specification for calculating the maximum of a range.
    #[derive(Debug, Clone, Default)]
    pub struct Max<T>(PhantomData<T>);
    impl<T> Spec for Max<T>
    where
        T: Clone + Ord,
    {
        type Data = T;
        const IDEMPOTENT: bool = true;
        #[inline(always)]
        fn op(&self, a: &Self::Data, b: &Self::Data) -> Self::Data {
            Ord::max(a, b).clone()
        }
    }

    // TODO Implement Argmin, Argmax (which would also work in O(1)), but this requires
    // input/output (similar to segtree) since we need to store (min, arg) tuple.
}
// END code

/* Templates
// START imports
{SparseTable, Spec}
// END imports
*/

#[cfg(test)]
mod tests {
    use super::{ops::*, *};
    use std::panic::catch_unwind;

    struct Empty;
    impl Spec for Empty {
        type Data = ();
        const IDEMPOTENT: bool = true;
        fn op(&self, _a: &Self::Data, _b: &Self::Data) -> Self::Data {
            ()
        }
    }

    struct Panic;
    impl Spec for Panic {
        type Data = i64;
        const IDEMPOTENT: bool = true;
        fn op(&self, _a: &Self::Data, _b: &Self::Data) -> Self::Data {
            panic!()
        }
    }

    struct Concat;
    impl Spec for Concat {
        type Data = String;
        const IDEMPOTENT: bool = false;
        fn op(&self, a: &Self::Data, b: &Self::Data) -> Self::Data {
            a.clone() + b
        }
    }

    #[test]
    fn new() {
        assert_eq!(SparseTable::new(vec![], Empty).data, vec![]);
        assert_eq!(
            SparseTable::new(vec![1], Sum::<i8>::default()).data,
            vec![1]
        );

        // Test boundary conditions around 2^n
        assert_eq!(
            SparseTable::new(vec![6, 9, 1, 0, 5, 5, 6], Min::<u8>::default()).data,
            vec![6, 9, 1, 0, 5, 5, 6, 6, 1, 0, 0, 5, 5, 0, 0, 0, 0]
        );
        assert_eq!(
            SparseTable::new(vec![5, 6, 7, 2, 1, 4, 5, 6], Min::<u8>::default()).data,
            vec![5, 6, 7, 2, 1, 4, 5, 6, 5, 6, 2, 1, 1, 4, 5, 2, 1, 1, 1, 1, 1]
        );
        assert_eq!(
            SparseTable::new(vec![9, 4, 2, 7, 9, 2, 3, 1, 8, 6], Min::<u8>::default()).data,
            vec![
                9, 4, 2, 7, 9, 2, 3, 1, 8, 6, 4, 2, 2, 7, 2, 2, 1, 1, 6, 2, 2, 2, 2, 1, 1, 1, 1, 1,
                1
            ]
        );

        assert_eq!(SparseTable::new(vec![1], Panic).data, vec![1]);
        assert_eq!(
            SparseTable::new(vec![4, 8, 10, 2, 18, 4, 5, 6, -3, 1], Sum::<i64>::default()).data,
            vec![
                4, 8, 10, 2, 18, 4, 5, 6, -3, 1, 12, 18, 12, 20, 22, 9, 11, 3, -2, 24, 38, 34, 29,
                33, 12, 9, 57, 50, 43
            ]
        );
        assert_eq!(
            SparseTable::new(vec![-2, 4, -5, 1, 3, 7, -10, 12, 0], Max::<i32>::default()).data,
            vec![
                -2, 4, -5, 1, 3, 7, -10, 12, 0, 4, 4, 1, 3, 7, 7, 12, 12, 4, 4, 7, 7, 12, 12, 12,
                12
            ]
        );
    }

    #[test]
    fn query() {
        let tree = SparseTable::new(vec![5, 4, 1, 2, 4, 5, 0, 2, 8, 3], Min::<i64>::default());
        assert_eq!(tree.query(..), 0);
        assert_eq!(tree.query(4..), 0);
        assert_eq!(tree.query(..2), 4);
        assert_eq!(tree.query(..=2), 1);
        assert_eq!(tree.query((Bound::Excluded(2), Bound::Included(3))), 2);

        assert_eq!(tree.query(3..=5), 2);
        assert_eq!(tree.query(2..=5), 1);
        assert_eq!(tree.query(2..=6), 0);

        assert!(catch_unwind(|| tree.query(1..=1)).is_ok());
        assert!(catch_unwind(|| tree.query(1..1)).is_err());
        assert!(catch_unwind(|| tree.query(0..=9)).is_ok());
        assert!(catch_unwind(|| tree.query(0..11)).is_err());

        let tree = SparseTable::new(
            vec![3, 4, -1, 0, -2, 3, 3, 6, 2, 4, 5],
            Sum::<i64>::default(),
        );
        assert_eq!(tree.query(..), 27);
        assert_eq!(tree.query(2..9), 11);
        assert_eq!(tree.query(5..=7), 12);

        assert_eq!(tree.query(4..=6), 4);
        assert_eq!(tree.query(4..=7), 10);
        assert_eq!(tree.query(4..=8), 12);
    }

    #[test]
    fn query_non_commutative() {
        let tree = SparseTable::new(
            ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]
                .iter()
                .map(|x| x.to_string())
                .collect(),
            Concat,
        );
        let sum = "abcdefghij";
        for i in 0..sum.len() {
            for j in i..sum.len() {
                assert_eq!(tree.query(i..=j), sum[i..=j]);
            }
        }
    }

    #[test]
    fn query_random() {
        let tree = SparseTable::new(
            (0u64..5000)
                .map(|x| x.wrapping_mul(8444581530163263827) >> 32)
                .collect(),
            Sum::<u64>::default(),
        );
        let mut res = 0;
        for i in 0u64..5000 {
            let a = (i.wrapping_mul(5704048548839871799) % 5000) as usize;
            let b = (i.wrapping_mul(1965665916967740679) % 5000) as usize;
            res += tree.query(a.min(b)..=a.max(b));
        }
        assert_eq!(res, 17838014936058117);
    }
}
